import React, {Component} from 'react'
import cms from "../cms.json"
import { NavLink } from "react-router-dom"
import {getPaths, showSidebar, hideSidebar, toggleSubnav} from '../functions'

class Sidebar extends Component {
  constructor(props) {
    super(props)
  }
    
  render() {
    let linkAtts = {
      onClick: this.hide,
      className: "relative flex content-between items-center max-w-full pl-3 my-2 ml-2 border-2 border-r-0 border-amber-600 border-t-0 border-b-0 text-4xl text-amber-50 hover:text-amber-200",
      onMouseEnter: showSidebar,
      activeClassName: 'text-poppy-900 border-poppy-900 pointer-events-none'
    }

    let user = JSON.parse(document.head.querySelector("[name=user]").content)
    let user_level_id = 'Tenant'
    if( user.user_level_id === 1 ) {
      user_level_id = 'Administrator'
    }else if( user.user_level_id === 2 ) {
      user_level_id = 'Manager'
    }
    let paths = getPaths()
    let is_admin = paths.filter(p => p === 'admin')
    is_admin = is_admin.length ? true : false
    return (
      <aside data-sidebar className="h-screen w-1/5 flex flex-col items-start content-start bg-amber-600 text-amber-50 transition-all ease-in-out duration-500" onMouseLeave={hideSidebar}>
        <div className="flex w-full p-8 border-b-1">
          <NavLink to="/" className="flex" title={`Jump to the ${is_admin ? 'touchscreen' : 'content managment system Dashboard'}`}>
            <svg className="icon">
              <use xlinkHref="#logo__land_baron"/>
            </svg>
            <span className="ml-1 text-orange-50 uppercase">Land Baron</span>
          </NavLink>
        </div>
        <figure className="flex flex-col w-full border border-orange-700 border-l-0 border border-r-0 text-sm">
          <div className="flex items-center w-full px-6 pt-6">
            <div className="rounded-full w-8 h-8" style={{backgroundImage:`url(${user.photo ? user.photo : '//via.placeholder.com/48x48'})`}}>
              <img src={`${user.photo ? user.photo : '//via.placeholder.com/48x48'}`} className="hidden"/>
            </div>
            <figcaption className="ml-1 flex flex-col items-start content-center cursor-pointer" onClick={toggleSubnav}>
              <span className="ml-1 text-light">{user.first_name}&nbsp;{user.last_name}</span>
              <span className="ml-1 text-light text-xs text-amber-50">{user_level_id}</span>
            </figcaption>
          </div>
          <nav data-subnav className="relative flex flex-col items-start content-center w-full text-amber-50 h-0 transition duration-300 overflow-hidden mt-4 mb-0">
            <NavLink activeClassName="text-amber-300" key={'subnav__profile'} to={`/profile`} className={cms.sidebar.subnav.style}>
              <span className="text-xs">Profile</span>
            </NavLink>
            <NavLink activeClassName="text-amber-300" key={'subnav__income'} to={`/profile#rent`} className={cms.sidebar.subnav.style}>
              <span className="text-xs">Income</span>
              <span className="flex flex-col content-center items-center w-4 h-4 ml-2 rounded-full bg-amber-400 text-center text-amber-600 text-xs font-bold">{user.rent}</span>
            </NavLink>
          </nav>
        </figure>
        <nav className="w-full pb-4 pt-4">
        { this.props.links.map((link,l) => {
          return (
          <NavLink key={`navigation__${l}`} to={`/${link.slug}`}{...linkAtts}>
            <svg className="icon">
              <use xlinkHref={`#${link.icon ? link.icon : 'icon__stage'}`}></use>
            </svg>
            <span className="ml-1 text-sm w-full">{link.label}</span>
            { link === cms.rent &&
            <span className={cms.sidebar.nav.rent.style}>{this.props.rent.length}</span>
            }
          </NavLink>
          )
        })}
        </nav>
      </aside>
    )
  }
}

Sidebar.defaultProps = {
  links: [cms.properties, cms.tenant, cms.barons, cms.settings]
}

export default Sidebar