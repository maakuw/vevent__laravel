import React, {Component} from 'react'
import cms from "../cms.json"
import { toggleRegistration } from '../functions'

class Register extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    return  (
      <>
        <div className="flex items-stretch content-between space-x-4">
          <fieldset>
            <label htmlFor="first_name">First Name</label>
            <input 
              type="text" 
              placeholder="Someone"
              className="rounded border border-gray-300 text-sm p-2 w-full shadow-sm focus:border-amber-400" 
              name="first_name" 
              required 
            />
          </fieldset>
          <fieldset>
            <label htmlFor="last_name">Last Name</label>
            <input 
              type="text" 
              placeholder="Persona"
              className="rounded border border-gray-300 text-sm p-2 w-full shadow-sm focus:border-amber-400" 
              name="last_name" 
              required 
            />
          </fieldset>
        </div>
        <div className="flex items-stretch content-between space-x-4">
          <fieldset className="w-full">
            <label htmlFor="email">Email</label>
            <input 
              type="email" 
              placeholder="name@email.com"
              className="rounded border border-gray-300 text-sm p-2 w-full shadow-sm focus:border-amber-400" 
              name="email" 
              required 
            />
          </fieldset>
        </div>
        <div className="flex items-stretch content-between space-x-4">
          <fieldset>
            <label htmlFor="password">Password</label>
            <input 
              type="password" 
              placeholder="your_password"
              className="rounded border border-gray-300 text-sm p-2 w-full shadow-sm focus:border-amber-400 small" 
              name="password" 
              required 
            />
          </fieldset>
          <fieldset>
            <label htmlFor="password">Confirm Password</label>
            <input 
              type="password" 
              placeholder="confirm"
              className="rounded border border-gray-300 text-sm p-2 w-full shadow-sm focus:border-amber-400 small" 
              name="confirm" 
              required 
            />
          </fieldset>
        </div>
        <div className="flex items-stretch content-between space-x-4">
          <fieldset className="w-full">
            <label htmlFor="title">Title</label>
            <input 
              type="text" 
              placeholder="optional"
              className="rounded border border-gray-300 text-sm p-2 w-full shadow-sm focus:border-amber-400 small" 
              name="title" 
              required 
            />
          </fieldset>
          <fieldset>
            <label htmlFor="company">Company</label>
            <input 
              type="text" 
              placeholder="optional"
              className="rounded border border-gray-300 text-sm p-2 w-full shadow-sm focus:border-amber-400 small" 
              name="company" 
              required 
            />
          </fieldset>
        </div>
        <nav className="flex flex-col content-stretch items-stretch mt-4 space-x-8">
          <button type="submit" className="bg-amber-500 hover:bg-amber-400 text-gray-50 rounded w-full p-2 transition">Register</button>
          <a className="mt-4 text-center text-sm text-gray-900" href="/login" onClick={toggleRegistration}>{cms.login.existing} <span className="text-sky-600 pointer-events-none">Log in</span></a>
        </nav>
      </>
    )
  }
}

export default Register