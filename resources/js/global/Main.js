import api from '../api.js'
import React, {Component} from "react"
import {BrowserRouter, Route, Switch} from "react-router-dom"

//Global Setup File
import { randomID, timedAlert, toggleSidenav, updateBodyStyle } from '../functions.js'
import cms from "../cms.json"

//Global Components
import Sidebar from "./Sidebar"
import Feedback from '../partials/Feedback'

//Pages
import Bios from "../pages/Bios"
import Bio from "../pages/Bio"
import Index from "../pages/Index"
import Profile from "../pages/Profile"
import Portfolio from "../pages/Portfolio"
import Property from "../pages/Property"
import Properties from "../pages/Properties"
import Settings from "../pages/Settings"
//import Talk from "../pages/Talk"
import Tenant from "../pages/Tenant"
import Tenants from "../pages/Tenants"
import WalkTheFloor from "../pages/WalkTheFloor"
import Attribute from '../pages/Attribute.js'
import Area from '../pages/Area.js'

class Main extends Component {
  constructor(props) {
    super(props)

    /*------
    * Global
    ------*/

    //Methods
    this.sortWeek = this.sortWeek.bind(this)
    this.resetAlert = this.resetAlert.bind(this)

    //Variables
    this.active = ''
    this.activeFilters = cms.activeFilters
    this.property_timer = false
    this.watchTimer = false


    //Modal Methods
    this.resetModal       = this.resetModal.bind(this)
    this.setModal         = this.setModal.bind(this)
    this.editModal        = this.editModal.bind(this)
    this.createModal      = this.createModal.bind(this)
    this.deleteModal      = this.deleteModal.bind(this)

    this.changeFloor = this.changeFloor.bind(this)
    
    this.buildPortfolioCategories = this.buildPortfolioCategories.bind(this)
    this.buildUserCategories = this.buildUserCategories.bind(this)

    this.getPortfolios = this.getPortfolios.bind(this)
    this.getProperties = this.getProperties.bind(this)
    this.getViewing = this.getViewing.bind(this)
    this.getBarons  = this.getBarons.bind(this)
    this.getAttributes = this.getAttributes.bind(this)
    this.getAreas = this.getAreas.bind(this)
    this.getFilters = this.getFilters.bind(this)
    this.getFloors = this.getFloors.bind(this)
    this.getTenants  = this.getTenants.bind(this)

    /*--------------
    * Property
    --------------*/
    //Methods
    this.checkVideo = this.checkVideo.bind(this)
    this.clearVideo = this.clearVideo.bind(this)
    this.gotoProperty = this.gotoProperty.bind(this)
    this.clearProperty = this.clearProperty.bind(this)

    this.makeBadge = this.makeBadge.bind(this)

    /*--------------
    * Chat
    ---------------*/
    this.getRoomId = this.getRoomId.bind(this)

    /*--------------
    * Active State
    These are elements that are ever-changing, based on user interaction
    ---------------*/
    this.state = {
      activeFilters: this.activeFilters, 
      properties: false,
      categories: false, 
      portfolios: false, 
      filters: false, 
      floors: false, 
      settings: [],
      barons: false,
      tenants: false,
      areas: false, 
      watching: false, 
      schedule: false, 
      user: false, 
      feedback: {
        msg: false,
        stlye: ''
      }
    }
  }

  //Set the alert window to its original state
  resetAlert(msg=false,style=''){
    timedAlert(() => {
      this.setState({
        loading: false,
        feedback: {
          msg: msg,
          style: style
        }
      })
    })
  }

  makeBadge(text,icon,color="primary") {
    return (
      <svg key={`badge_svg--${randomID()}`} className="img-thumbnail col my-2 my-md-3 lg:my-4"
        xmlns="http://www.w3.org/2000/svg" 
        width="70" viewBox="0 0 70.402 63">
        <g transform="translate(1.341 30.33)" opacity="0.1">
          <path d="M949.1,502.048H882.68c-.524,0-.874-.263-.874-.657V477.18c0-.394.349-.657.874-.657H949.1c.525,0,.874.263.874.657v24.146A.787.787,0,0,1,949.1,502.048Z"
            transform="translate(-881.806 -476.523)" 
            className={color}/>
        </g>
        <g>
          <g transform="translate(0 29.661)">
            <g transform="translate(0.004)">
              <path d="M951.3,503.227h-68.59c-.541,0-.9-.275-.9-.687V477.21c0-.412.361-.687.9-.687H951.3c.542,0,.9.275.9.687v25.262A.816.816,0,0,1,951.3,503.227Zm-67.687-1.374H950.4V477.9H883.611Z"
                transform="translate(-881.806 -476.523)" 
                className={color}/>
            </g>
            <g transform="translate(0 25.318)">
              <path d="M888.118,560.394a.824.824,0,0,1-.632-.268l-5.415-5.355a.808.808,0,0,1-.18-.982.867.867,0,0,1,.812-.535h5.415a.848.848,0,0,1,.9.893V559.5a.78.78,0,0,1-.542.8C888.389,560.3,888.208,560.394,888.118,560.394Zm-3.249-5.355,2.347,2.321v-2.321Z"
                transform="translate(-881.797 -553.254)" 
                className={color}/>
            </g>
            <g transform="translate(63.178 25.318)">
              <path d="M1028.364,560.394a.547.547,0,0,1-.361-.089.857.857,0,0,1-.541-.8v-5.355a.848.848,0,0,1,.9-.893h5.415a.789.789,0,0,1,.812.535.808.808,0,0,1-.18.982L1029,560.127A.824.824,0,0,1,1028.364,560.394Zm.9-5.355v2.321l2.346-2.321Z"
                transform="translate(-1027.462 -553.254)" 
                className={color}/>
            </g>
          </g>
          <g transform="translate(5.418)">
            <path d="M952.952,443.826h-57.76a.848.848,0,0,1-.9-.893V427.1a3.636,3.636,0,0,1,1.9-3.213l25.992-10.532a3.884,3.884,0,0,1,3.7,0l26.082,10.621a3.636,3.636,0,0,1,1.9,3.213v15.834A.83.83,0,0,1,952.952,443.826ZM896.1,442.04H952.05V427.1a1.911,1.911,0,0,0-.993-1.7l-25.992-10.532a1.81,1.81,0,0,0-1.9,0L897.088,425.4a1.91,1.91,0,0,0-.993,1.7Z"
              transform="translate(-894.29 -412.885)" 
              className={color}/>
          </g>
        </g>
        {text}
        {icon}
      </svg>
    )
  }

  checkVideo(duration) {
    this.watchTimer = setTimeout(function(){
        window.location.href = '/go-live'
    }, duration)
  }

  clearVideo() {
    clearTimeout(this.watchTimer)
  }

  getRoomId(){
    let hash = window.location.hash
    if( hash ){
      hash = hash.substr(1)
    }else{
      hash = false
    }
    return hash
  }

  //An Interface for the setModal that will slightly realign the display
  editModal(title, copy, inputs, on_submit) {
    this.setModal("edit",title,copy, [
      {
        label: 'Update',
        icon: false
      }
    ], inputs, on_submit)
  }
  
  //An Interface for the setModal that will slightly realign the display
  createModal(title, copy, inputs, on_submit) {
    this.setModal("create",title,copy, [
      {
        label: 'Save',
        icon: false
      }
    ], inputs, on_submit)
  }
  
  //An Interface for the setModal that will slightly realign the display
  deleteModal(title, copy, inputs, on_submit) {
    this.setModal("delete",title,copy, [
      {
        label: 'Confirm',
        style: 'danger',
        icon: 'math--multiply',
        callback: on_submit
      }
    ], inputs)
  }

  //Set the modal in the state object from this or a child component (modal is reusable)
  setModal(type='edit', headline='', copy='', ctas=false, inputs=[], on_submit=false, image='', caption=false){
    if(type === 'preview') {
      $('#main__modal_window').modal('show')
    }

    this.setState({
      modal: {
        type: type,
        headline: headline,
        copy: copy,
        ctas: ctas,
        image: image, 
        caption: caption, 
        inputs: inputs,
        nStyle: type === 'delete' ? 'justify-content-center' : false,
        on_submit: on_submit
      }
    })
  }

  //Clear out the modal in the state object for this or a child component
  resetModal(){
    $('#main__modal_window').modal('hide')
    setTimeout(() => this.setState({modal: {}}), 300)
  }

  closeModal(){
    let modal = document.querySelector('.modal-backdrop')
    modal.remove()
    document.querySelector('body').classList.remove('modal-open')
  }
  
  //Switch floors on the Walk the Floor page
  changeFloor(event){
    let target = event.target
    let floors = document.querySelector('[data-floorplan]')

    if( target ) {
      let pics = floors.querySelectorAll('[data-floor]')
      pics.forEach(pic => {
        pic.classList.remove('active')
      })
      let floor = document.querySelector(`[data-floor="${target.value}"]`)
      if( floor ) floor.classList.add('active')
    }
  }

  //Return the list of barons from the Filters in the DB
  getFilters(callback=false) {
    this.setState({loading:true})
    api.get(`/cms/filters`)
    .then((response) => {
      this.setState({ filters: response.data})
      if(callback) callback()
    })
    .catch((error) => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading Filters! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  //Return the list of barons from the Users in the DB
  getFloors(callback=false) {
    this.setState({loading:true})
    api.get(`/cms/floors`)
    .then(response => {
      this.setState({ floors: JSON.parse(response.data) })
      console.log('floors', JSON.parse(response.data))
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading Floors! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  //Return the list of attendees from the Users in the DB
  getTenants(callback=false) {
    this.setState({loading:true})
    api.get(`/cms/tenants`)
    .then(response => {
      let user = JSON.parse(document.head.querySelector("[name=user]").content)
      user = response.data.filter(tenant => tenant.id = user.id)
      this.setState({ 
        tenants: response.data,
        user: user[0]
      })
      
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading Tenants! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  //Return the list of barons from the Users in the DB
  getBarons(callback=false) {
    this.setState({loading:true})
    api.get(`/cms/barons`)
    .then(response => {
      this.setState({ barons: response.data })
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading Barons! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  //Return the list of barons from the Users in the DB
  getAttributes(callback=false) {
    this.setState({loading:true})
    api.get(`/cms/attributes`)
    .then(response => {
      this.setState({ attributes: response.data })
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading Attributes! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  //Return the list of barons from the Users in the DB
  getAreas(callback=false) {
    this.setState({loading:true})
    api.get(`/cms/areas`)
    .then(response => {
      this.setState({ areas: response.data })
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading Areas! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  //Return the list of barons from the Users in the DB
  getViewing(callback=false) {
    this.setState({loading:true})
    api.get(`/cms/watching`)
    .then(response => {
      this.setState({ watching: response.data })
      if(callback) callback()
      return response.data
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `An error occurred when checking who's watching from the Cache! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  //Global Settings
  getPortfolios(callback=false) {
    this.setState({loading:true})
    api.get('/cms/portfolios')
    .then(response => {
      let event = response.data[0]
      let properties = event.properties
      //console.log(properties)
      this.resetAlert()
      this.setState({
        title: event.title,
        description: event.description,
        portfolios: response.data,
        feedback: {
          msg: 'Portfolios successfully loaded!',
          style: 'success'
        },
      })
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading the Portfolios! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  gotoProperty(playlist) {
    const target = this
    if( playlist ) {
      this.clearProperty()
//      console.log('gotoProperty', playlist[0])
      this.property_timer = setInterval(function(){    
          if( playlist.length > 0 ) {
            target.clearProperty()
            window.location.href = `/go-live/${playlist[0].id}`
          }
      }, 10000)
    }
  }

  clearProperty() {
    clearInterval(this.property_timer)
  }
  
  //Return the list of properties found in the backend, then set them to our state
  getProperties(callback=false) {
    this.setState({loading:true})
    api.get('/cms/properties')
    .then(response => {
      console.log(JSON.parse(response.data))
      this.resetAlert()
      this.setState({
        properties: JSON.parse(response.data), 
        feedback: {
          msg: 'Properties successfully loaded!',
          style: 'success'
        },
      })
      if(callback) callback()
    })
    .catch(error => {
      this.setState({
        feedback: {
          msg: `And error occurred when loading the Properties! ${error.data}`,
          style: 'danger'
        }
      })
    })
  }

  /*--------------
  * Portfolio Methods
  ---------------*/

  //Sorting All days in a week list
  sortWeek(filter, portfolios) {
    console.log(filter, portfolios)
    if( filter && portfolios ) {
      let index = this.activeFilters.indexOf(filter)
      let mon =[]
      let tue = []
      let wed = []
      let thu = []
      let fri = []
      let sat = []
      let sun = []
      let dem = []

      if( index > -1 ) {
          this.activeFilters.splice(index,1)
      }else{
          this.activeFilters.push(filter)
      }

      if( portfolios.mon ) {
        portfolios.mon.forEach(ev => {
            this.activeFilters.forEach(act => {
                if( ev.filters.indexOf(act) !== -1 ) {
                    if( mon.indexOf(ev) === -1 ) {
                        mon.push(ev)
                    }
                }
            })
        })
      }

      if( portfolios.tue ) {
        portfolios.tue.forEach(ev => {
            this.activeFilters.forEach(act => {
                if( ev.filters.indexOf(act) !== -1 ) {
                    if( tue.indexOf(ev) === -1 ) {
                        tue.push(ev)
                    }
                }
            })
        })
      }

      if( portfolios.wed ) {
        portfolios.wed.forEach(ev => {
            this.activeFilters.forEach(act => {
                if( ev.filters.indexOf(act) !== -1 ) {
                    if( wed.indexOf(ev) === -1 ) {
                        wed.push(ev)
                    }
                }
            })
        })
      }

      if( portfolios.thu ) {
        portfolios.thu.forEach(ev => {
            this.activeFilters.forEach(act => {
                if( ev.filters.indexOf(act) !== -1 ) {
                    if( thu.indexOf(ev) === -1 ) {
                        thu.push(ev)
                    }
                }
            })
        })
      }

      if( portfolios.fri ) {
        portfolios.fri.forEach(ev => {
            this.activeFilters.forEach(act => {
                if( ev.filters.indexOf(act) !== -1 ) {
                    if( fri.indexOf(ev) === -1 ) {
                        fri.push(ev)
                    }
                }
            })
        })
      }

      if( portfolios.sat ) {
        portfolios.sat.forEach(ev => {
            this.activeFilters.forEach(act => {
                if( ev.filters.indexOf(act) !== -1 ) {
                    if( sat.indexOf(ev) === -1 ) {
                        sat.push(ev)
                    }
                }
            })
        })
      }

      if( portfolios.sun ) {
        portfolios.sun.forEach(ev => {
            this.activeFilters.forEach(act => {
                if( ev.filters.indexOf(act) !== -1 ) {
                    if( sun.indexOf(ev) === -1 ) {
                        sun.push(ev)
                    }
                }
            })
        })
      }

      if( portfolios.dem ) {
        portfolios.dem.forEach(ev => {
            this.activeFilters.forEach(act => {
                if( ev.filters.indexOf(act) !== -1 ) {
                    if( dem.indexOf(ev) === -1 ) {
                        dem.push(ev)
                    }
                }
            })
        })
      }

      return {
          mon: mon ? mon : false,
          tue: tue ? tue : false,
          wed: wed ? wed : false,
          thu: thu ? thu : false,
          fri: fri ? fri : false,
          sat: sat ? sat : false,
          sun: sun ? sun : false,
          dem: dem ? dem : false
      }
      
    }else{
      console.error(`Either the filter (${filter}) or the portfolios aren't defined/is misdefined`, portfolios)
      return false
    }
  }

  buildUserCategories(bio, slug='bio'){
    if( !bio ) return false
    
    let info = false
    let tenants = false
    let schedule = false
    let properties = false

    //Do we have Information tab content?
    if( ( bio.tenants || bio.properties || bio.portfolios ) && ( bio.description || bio.interests || bio.title || bio.address || bio.company ) ) {
        info = {
            slug: `${slug}__information`,
            href: `#tab--${slug}__information`,
            label: `Information`,
            submenu: []
        }

        if( bio.tenants || bio.badges || bio.games ) {
          let scoreboard_pushed = false

          //About tab and About synopsis (default tab)?
          if( bio.description || bio.interests || bio.title || bio.address || bio.company ) {
              info.submenu.push({
                  label: 'About',
                  slug: `${slug}__information--about`,
                  href: `#anchor--${slug}__information--about`
              })
          }
          if( bio.badges ) {
            if( bio.badges.length > 6 ) {
              info.submenu.push({
                  label: 'Badges',
                  slug: `${slug}__information--badges`,
                  href: `#anchor--${slug}__information--badges`
              })
            }
          }
          // Tenants are a little more complex. This will require interfacing with a hook and tracking with this the Notifications component
          if( bio.tenants ) {
            if( bio.tenants.length > 6 ) {
              info.submenu.push({
                  label: 'Scoreboard',
                  slug: `${slug}__information--scoreboard`,
                  href: `#anchor--${slug}__information--scoreboard`
              })
              scoreboard_pushed = true
            }
          }else if( bio.badges && !scoreboard_pushed ) {
            if( bio.badges.length > 6 ) {
              info.submenu.push({
                  label: 'Scoreboard',
                  slug: `${slug}__information--scoreboard`,
                  href: `#anchor--${slug}__information--scoreboard`
              })
              scoreboard_pushed = true
            }
          }else if( bio.games && !scoreboard_pushed ) {
            if( bio.games > 6 ) {
              info.submenu.push({
                  label: 'Scoreboard',
                  slug: `${slug}__information--scoreboard`,
                  href: `#anchor--${slug}__information--scoreboard`
              })
              scoreboard_pushed = true
            }
          }
        }else{
          info.submenu = false
        }
    }

    //Do we have Tenants to show? (not on Bio. Later, we can merge this and Profile)
    if( bio.tenants ) {
      if( bio.tenants.attendees || bio.tenants.areas  || bio.tenants.barons ) {
        tenants = {
            slug: `${slug}__tenants`,
            href: `#tab--${slug}__tenants`,
            label: `Tenants`,
            submenu: []
        }

        //Are there attendees?
        if( bio.tenants.attendees ) {
            tenants.submenu.push({
                label: 'Attendees',
                slug: `${slug}__schedule--attendees`,
                href: `#tab--${slug}__schedule--attendees`,
            })
        }

        //Are there areas?
        if( bio.tenants.areas ) {
            tenants.submenu.push({
                label: 'Areas',
                slug: `${slug}__schedule--areas`,
                href: `#tab--${slug}__schedule--areas`
            })
        }

        //Are there barons?
        if( bio.tenants.barons ) {
            tenants.submenu.push({
                label: 'Barons',
                slug: `${slug}__schedule--barons`,
                href: `#tab--${slug}__schedule--barons`
            })
        }

        if( !tenants.submenu.length > 0 ) tenants.submenu = false
      }
    }

    //Do we have Portfolios to show?
    if( bio.portfolios ) {
        schedule = {
            slug: `${slug}__schedule`,
            href: `#tab--${slug}__schedule`,
            label: `Portfolio`,
            submenu: []
        }
        for (const [day, value] of Object.entries(bio.portfolios)) {
          if( value ) {
            let label
            switch(day){
              case 'mon': label = 'Monday'; break
              case 'tue': label = 'Tuesday'; break
              case 'wed': label = 'Wednesday'; break
              case 'thu': label = 'Thursday'; break
              case 'fri': label = 'Friday'; break
              case 'sat': label = 'Saturday'; break
              case 'sun': label = 'Sunday'; break
              case 'dem': label = 'On Demand (Replay)'; break
              default: label = false; break
            }

            schedule.submenu.push({
                label: label,
                slug: `${slug}__schedule--${day}`,
                href: `#tab--${slug}__schedule--${day}`,
                count: this.parseCount(value.length)
            })
          }
        }

       if( !schedule.submenu.length > 0 ) schedule.submenu = false
    }

    //Do we have Tenants to show? (not on Bio. Later, we can merge this and Profile)
    if( bio.properties ) {
      if( bio.properties.attributes || bio.properties.areas ) {
        properties = {
            slug: `${slug}__properties`,
            href: `#tab--${slug}__properties`,
            label: `Properties`,
            submenu: []
        }

        //Are there attributes?
        if( bio.properties.attributes ) {
            properties.submenu.push({
                label: 'Attributes',
                slug: `${slug}__properties--attributes`,
                href: `#tab--${slug}__properties--attributes`
            })
        }

        //Are there areas?
        if( bio.properties.areas ) {
            properties.submenu.push({
                label: 'Areas',
                slug: `${slug}__properties--areas`,
                href: `#tab--${slug}__properties--areas`
            })
        }

        if( !properties.submenu.length > 0 ) properties.submenu = false
      }
    }

    return ( info || tenants || schedule || properties ) ? [info,tenants,schedule,properties] : false
  }

  buildPortfolioCategories(event){
    let schedule = false

    //Do we have Portfolios to show?
    if( event ) {
        schedule = {
            slug: `event__schedule`,
            href: `#tab--event__schedule`,
            label: `Portfolio`,
            submenu: []
        }

        //Is there a list of Monday portfolios?
        if( event.mon ) {
            schedule.submenu.push({
                label: 'Monday',
                slug: 'event__schedule--mon',
                href: '#tab--event__schedule--mon',
                count: this.parseCount(event.mon.length)
            })
        }
        //Is there a list of Tuesday portfolios?
        if( event.tue ) {
            schedule.submenu.push({
                label: 'Tuesday',
                slug: 'event__schedule--tue',
                href: '#tab--event__schedule--tue',
                count: this.parseCount(event.tue.length)
            })
        }
        //Is there a list of Wednesay portfolios?
        if( event.wed ) {
            schedule.submenu.push({
                label: 'Wednesday',
                slug: 'event__schedule--wed',
                href: '#tab--event__schedule--wed',
                count: this.parseCount(event.wed.length)
            })
        }
        //Dev Note: let's add in the remaining days, so in the future, when they ask, we are ready!
        //Is there a list of Thursday portfolios?
        if( event.thu ) {
            schedule.submenu.push({
                label: 'Thursday',
                slug: 'event__schedule--thu',
                href: '#tab--event__schedule--thu',
                count: this.parseCount(event.thu.length)
            })
        }
        //Is there a list of Friday portfolios?
        if( event.fri ) {
            schedule.submenu.push({
                label: 'Friday',
                slug: 'event__schedule--fri',
                href: '#tab--event__schedule--fri',
                count: this.parseCount(event.fri.length)
            })
        }
        //Is there a list of Saturday portfolios?
        if( event.sat ) {
            schedule.submenu.push({
                label: 'Saturday',
                slug: 'event__schedule--sat',
                href: '#tab--event__schedule--sat',
                count: this.parseCount(event.sat.length)
            })
        }
        //Is there a list of Sunday portfolios?
        if( event.sun ) {
            schedule.submenu.push({
                label: 'Sunday',
                slug: 'event__schedule--sun',
                href: '#tab--event__schedule--sun',
                count: this.parseCount(event.sun.length)
            })
        }
        //Is there a list of On-Demand portfolios?
        if( event.dem ) {
            schedule.submenu.push({
                label: 'On-Demand',
                slug: 'event__schedule--dem',
                href: '#tab--event__schedule--dem',
                count: '(Replay)'
            })
        }
    }

    return [schedule]
  }

  componentDidMount() {
    window.addEventListener('resize', () => {
      let sidebar = document.querySelector('[data-sidebar]')
      if( sidebar ) sidebar.classList.remove('-ml-14')
    })
    let main = document.querySelector('[data-main]')
    let toggler = document.querySelector('header [data-toggler]')

    if( toggler ){
      toggler.addEventListener('click', toggleSidenav)
    }
    
    //Then, setup the body style so things hide/show/animate in time
    //updateBodyStyle()

    if( main && cms.background ) {
      main.style.backgroundImage = cms.background.backgroundImage
      main.style.backgroundSize = cms.background.backgroundSize
      main.style.backgroundRepeat = cms.background.backgroundRepeat
    }

    //First, get the most important chunk of information
    this.getPortfolios()
    this.getTenants()
    this.getBarons()
    this.getProperties()
    this.getAttributes()
    this.getAreas()
    this.getFilters()
    this.getFloors()
  }

  render(){
    let globalVars = {
      has_tags: cms.has_tags, 
      has_search: cms.has_search, 
      use_rent: cms.use_rent, 
      has_filters: this.state.filters ? true : false,
      bgStyle: cms.settings.bgStyle, 
      maiStyle: cms.maiStyle, 
      activeFilters: this.state.activeFilters, 
      properties: this.state.properties, 
      filters: this.state.filters, 
      floors: this.state.floors, 
      attributes: this.state.attributes, 
      areas: this.state.areas, 
      categories: this.state.categories,
      tenants: this.state.tenants,
      portfolios: this.state.portfolios,
      social: cms.social,
      barons: this.state.barons,
      user: this.state.user, 
      tenants: this.state.tenants
    }

    let globalMethods = {
      checkVideo: this.checkVideo, 
      clearProperty: this.clearProperty, 
      buildUserCategories: this.buildUserCategories, 
      buildPortfolioCategories: this.buildPortfolioCategories, 
      sortWeek: this.sortWeek, 
      closeModal: this.closeModal,
      makeBadge: this.makeBadge,
      editModal: this.editModal,
      createModal: this.createModal,
      deleteModal: this.deleteModal
    }

    return (
    <BrowserRouter>
      <Sidebar active={this.state.active} {...globalVars}/>
      <Feedback feedback={this.state.feedback}/>
      <Switch>
        <Route exact path="/" render={() => (
          <Index {...globalVars} {...globalMethods}
           headline={cms.index.headline} 
           summary={cms.index.summary} 
           hero={cms.index.feature}
           watching={this.state.watching}
           getViewing={this.getViewing}/>
          )} />
        <Route exact path={`/tenants`} render={() => (
          <Tenants {...globalVars} {...globalMethods}
            headline={cms.tenant.headline} 
            summary={cms.tenant.summary}
            hero={cms.tenant.feature}/>
        )} />
        <Route path={`/tenants/:id`} render={(params) => (
          <Tenant {...globalVars} {...globalMethods}
            id={parseFloat(params.match.params.id)}
            panStyle={cms.bio.panStyle} 
            heaStyle={cms.bio.heaStyle} 
            headline={cms.tenant.headline} />
        )} />
        <Route exact path={`/properties`} render={() => (
          <Properties {...globalVars} {...globalMethods}
           headline={cms.properties.headline} 
           summary={cms.properties.summary}
           hero={cms.properties.hero}/>
        )} />
        <Route exact path={`/properties/:id`} render={(params) => (
          <Property {...globalVars} {...globalMethods}
           id={parseFloat(params.match.params.id)}
           headline={cms.properties.headline} 
           summary={cms.properties.summary}
           hero={cms.properties.hero}/>
        )} />
        <Route path={`/properties/attribute/:id`} render={(params) => {
          let attribute = this.state.attributes ? this.state.attributes.filter(attribute => attribute.id === parseFloat(params.match.params.id)) : false
          let property = ( attribute && this.state.properties ) ? this.state.properties.filter(property => property.id === attribute[0].property_id) : false
          return (
          <Attribute {...globalVars} {...globalMethods}
            property={property[0]} 
            attribute={attribute[0]} 
            panStyle={cms.attribute.panStyle} 
            heaStyle={cms.attribute.heaStyle} 
            headline={cms.attribute.headline} />
          )
        }} />
        <Route path={`/properties/area/:id`} render={(params) => {
          let area = this.state.areas ? this.state.areas.filter(area => area.id === parseFloat(params.match.params.id)) : false
          let property = ( area && this.state.properties ) ? this.state.properties.filter(property => property.id === area[0].property_id) : false
          return (
          <Area {...globalVars} {...globalMethods}
            property={property[0]} 
            area={area[0]} 
            panStyle={cms.area.panStyle} 
            heaStyle={cms.area.heaStyle} 
            headline={cms.area.headline} />
          )
        }} />
        <Route path={`/properties/walk-the-floor`} render={() => (
          <WalkTheFloor {...globalVars} {...globalMethods}
           headline={cms.walk_the_floor.headline}
           hero={cms.walk_the_floor.feature}
           changeFloor={this.changeFloor}/>
        )} />
        <Route path="/profile" render={() => (
          <Profile {...globalVars} {...globalMethods}
           feature={cms.profile.feature}
           headline={cms.profile.headline}/>
        )} />
        <Route exact path={`/portfolio`} render={() => (
          <Portfolio {...globalVars} {...globalMethods}
           headline={cms.schedule.headline} 
           summary={cms.schedule.summary}
           getViewing={this.getViewing} 
           watching={this.state.watching}/>
        )} />
        <Route exact path={`/barons`} render={() => (
          <Bios {...globalVars} {...globalMethods}
            headline={cms.barons.headline} 
            summary={cms.barons.summary}/>
        )} />
        <Route path={`/barons/:id`} render={(params) => (
          <Bio {...globalVars} {...globalMethods}
            id={parseFloat(params.match.params.id)}
            panStyle={cms.bio.panStyle} 
            heaStyle={cms.bio.heaStyle} 
            headline={cms.bio.headline} />
        )} />
        <Route path={`/portfolio/properties/:id`} render={(params) => (
          <Property {...globalVars} {...globalMethods}
            id={parseFloat(params.match.params.id)}
            headline={cms.property.headline} 
            copy={cms.property.copy} 
            watching={this.state.watching} 
            checkVideo={this.checkVideo}
            gotoProperty={this.gotoProperty}
            clearVideo={this.clearVideo}
            getViewing={this.getViewing}/>
        )} />
        <Route exact path="/settings" render={() => (
          <Settings {...globalVars} {...globalMethods}/>
        )} />
      </Switch>
    </BrowserRouter>
    )
  }
}

export default Main