import React, {Component} from 'react'
import cms from "../cms.json"
import { toggleRegistration } from '../functions'

class Login extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount(){
    let hash = window.location.hash
    if(hash === '#register') toggleRegistration()
  }

  render() {
    return  (
      <>
        <div className="flex flex-col items-stretch content-center">
          <fieldset className="w-full mb-6">
            <label htmlFor="signup_email">{cms.login.email.label}</label>
            <input className="rounded border border-gray-300 text-sm p-2 w-full shadow-sm focus:border-amber-400" type="email" placeholder={cms.login.email.placeholder} name={cms.login.email.slug} required />
          </fieldset>
          <fieldset className="w-full mb-6">
            <div className="flex items-center">
              <label htmlFor="signup_password">{cms.login.password.label}</label>
              <a href="forgot-password" className="text-xs text-sky-600 mr-0 ml-auto">{cms.login.password.forgot}</a>
            </div>
            <input className="rounded border border-gray-300 text-sm p-2 w-full shadow-sm rounded border border-gray-300 text-sm p-2 w-full shadow-sm focus:border-amber-600 focus:ring-amber-600 hover:border-amber-400" type="password" placeholder={cms.login.password.placeholder} name={cms.login.password.slug} required/>
          </fieldset>
        </div>
        <nav className="flex flex-col content-stretch items-stretch mt-4">
          <button type="submit" className="bg-amber-500 hover:bg-amber-400 text-gray-50 rounded w-full p-2 transition">{cms.login.submit}</button>
          <a className="mt-4 text-center text-sm text-gray-900" href="#register" onClick={toggleRegistration}>{cms.login.register} <span className="text-sky-600 pointer-events-none">Register</span></a>
        </nav>
      </>
    )
  }
}

export default Login