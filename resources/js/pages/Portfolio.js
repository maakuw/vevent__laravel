import React, {Component} from 'react'
import Barons from '../components/Barons'
import Categories from '../components/Categories'
import Properties from '../components/Properties'
import Headline from '../components/Headline'
import Filters from '../partials/Filters'
import {jumpToTop, updateBodyStyle} from '../functions'

class Schedule extends Component {
  constructor(props) {
    super(props)

    this.portfolio = false
    this.properties = false
    this.categories = false
    this.filters = []
    this.activeFilters = []

    this.state = {
      has_properties: false, 
      properties: this.properties
    }

    this.sortList = this.sortList.bind(this)
  }
  
  sortList(event) {
    if( event ) {
      let target = event.target
      let filter = target.parentNode.getAttribute('data-filter')

      this.setState({
        properties: {},
        has_properties: true
      })
    }else{
      this.setState({
        properties: this.properties,
        has_properties: true
      })
    }
  }

  parseProps(){
    if( this.props.categories ) this.categories = this.props.categories
    if( this.props.filters ) this.filters = this.props.filters
    if( this.props.portfolios && !this.state.has_properties) {
      this.portfolio = this.props.portfolios[0]
      this.properties = this.portfolio.properties
      this.categories = this.props.buildPortfolioCategories(this.portfolio)
      this.sortList()
    }
  }

  componentDidMount() {
    jumpToTop()
    updateBodyStyle()
    this.parseProps()
  }

  componentDidUpdate(){
    if(this.props && !this.state.has_properties) this.parseProps()
  }
    
  render() {
    let barons = false
    let tabStyle = ' w-full'

    if(this.portfolio){
      if( this.portfolio && this.props.barons ) {
        //Dev Note: this is a half-assed effort until we have properties setup
        barons = this.props.barons.filter(baron => baron.speeches)
      }
      
      if( this.categories ) {
        if( this.categories[0] ) {
          if( this.categories[0].submenu.length > 1 ) tabStyle = ' w-full md:w-2/3 lg:3/4 lg:pl-4 lg:pr-0'
        }
      }
    }

    console.log(this.state)

    return (
      <div className="container">
        { this.props.headline &&
        <Headline key="portfolio__headline" copy={`${this.props.headline}${this.portfolio ? this.portfolio.properties.length : 'No Properties in this Portfolio'}`}/>
        }
        { ( barons && this.state.properties ) &&
        <Barons key="portfolio__barons"
          headline="Property Barons"
          barons={[this.props.barons[0]]}/>
        }
        <section className="flex content-between">
          { this.categories &&
          <Categories key="portfolio__categories"
            searchLabel="Search Properties" 
            has_search={this.props.has_search}
            categories={this.categories}/>
          }
          { this.props.properties &&
          <div className={`tab-content${tabStyle}`}>
            { this.props.filters &&
            <Filters key="portfolio__filters"
              sortList={this.sortList}
              filter={this.props.filters}
              activeFilters={this.props.activeFilters}/>
            }
            { this.state.properties &&
            <div data-schedule
              className={`tab-pane fade active show`} 
              role="tabpanel" id="tab--event__schedule" 
              aria-labelledby="portfolio__mon">
              <Properties key="portfolio__properties" 
                has_tags={this.props.has_tags}
                divStyle="w-full"
                properties={this.state.properties}
                barons={this.state.properties.barons}/>
            </div>
            }
          </div>
          }
        </section>
      </div>
    )
  }
}

export default Schedule