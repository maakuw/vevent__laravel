import React, {Component} from 'react'
import { updateBodyStyle } from '../functions'
import { initMap } from '../map_functions'

class Index extends Component {
  componentDidMount(){
    updateBodyStyle('dashboard')
    //Trigger an event and load the map that way...
    // initMap() 
  }

  render(){
    let map = document.querySelector('[data-map]')
    return (
      <div className="w-4/5 relative container h-screen">
        { this.props.headline &&
        <header className="absolute top-0 left-0 w-full z-50 flex flex-col items-center content-center px-2 pb-4 md:pb-6 lg:pb-8 transition duration-600" style={{"background":"linear-gradient(180deg, #000000 55.42%, rgba(0, 0, 0, 0) 100%)"}}>
          {this.props.headline}
        </header>
        }
      </div>
    )
  }
}

export default Index

Index.defaultProps = {
  initMap : () => {}
}