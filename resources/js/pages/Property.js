import React, {Component} from 'react'
import { formatPhone } from '../functions'
import Categories from '../components/Categories'
import Contacts from '../components/Tenants'
import ReactMarkdown from 'react-markdown'
import {updateBodyStyle, jumpToTop} from '../functions'

class Session extends Component {
  constructor(props) {
    super(props)

    this.divStyle = "container pt-0 lg:pt-4 relative"
    this.panel = {
      style: "bg-tertiary p-4 mb-4"
    }
  }

  parseSession(response, sessions) {
    let categories = this.categories
    let poster = false
    let src = response.src
    let loop = false
    let autoplay = false

    if (response.stream_type === 1) {
      autoplay = true;
      loop = true;
    }

    this.setState({
      id: response.id,
      title: response.title,
      description: response.description, 
      categories: categories,
      address: response.address,
      phone: response.phone, 
      bg: response.bg,
      src: src,
      poster: poster,
      loop: loop,
      autoplay: autoplay, 
      vidStyle: this.vidStyle
    })
  }

  componentDidMount() {
    updateBodyStyle()
  }

  render(){
    return (
      <div className={this.divStyle}>
        <section className="flex content-between mt-4">
          { this.props.categories.length > 1 &&
          <Categories key="session__categories" 
            has_search={this.props.has_search}
            searchLabel="Search Information"  
            categories={this.props.categories}/>
          }
          <div className={`tab-content w-full${this.props.categories.length > 1 ? ' md:w-2/3 lg:w-3/4 lg:pl-4 lg:pr-0' : ' px-3 lg:px-0'}`}>
            <div data-panels
              className="tab-pane fade active show" 
              role="tabpanel" id="tab--session__information" 
              aria-labelledby="session__information">
              <article data-panel className={this.panel.style}>
                <div className="p-4">
                  { ( this.props.title || this.props.description ) &&
                  <h2 className="text-sm mb-4 font-bold text-primary uppercase">About</h2>
                  }
                  { this.props.title &&
                  <h3 className="h4">{this.props.title}</h3>
                  }
                  <ReactMarkdown children={this.props.description} skipHtml={false}/>
                </div>
              </article>
            { (this.props.address || this.props.phone  || this.props.email ) &&
              <article data-panel className={this.panel.style}>
                <div className="p-4">
                  <h2 className="text-sm font-bold text-primary uppercase">Contact</h2>
                  { this.props.address &&
                  <address className="mb-0 font-light">
                    <ReactMarkdown children={this.props.address} skipHtml={false} />
                  </address>
                  }
                  { this.props.phone &&
                  <a className="block mb-1 text-battleship font-light" href={`tel:+1${this.props.phone}`}>{formatPhone(this.props.phone)}</a>
                  }
                  { this.props.email &&
                  <a className="block mb-1 text-battleship font-light" href={`mailto:${this.props.email}`}>{this.props.email}</a>
                  }
                </div>
              </article>
            }
            </div>
            <div data-my_barons
              className="tab-pane fade"
              role="tabpanel" id="tab--session__roster"
              aria-labelledby="session__roster">
              <Contacts key="session__roster" roster={this.props.roster}/>
            </div>
          </div>
        </section>
      </div>
    )    
  }
}

Session.defaultProps = {
  title: '',
  description: '',
  vidStyle: {
    top: false,
    left: false,
    minWidth: false,
    width: false,
    minHeight: false,
    height: false
  },
  bg: false,
  categories: [{
    slug: `session__information`,
    href: `#tab--session__information`,
    label: `About`
  }], 
  address: false,
  phone: false,
  src: false,
  poster: false,
  duration: false
}

export default Session 