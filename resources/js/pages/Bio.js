import React, {Component} from 'react'
import Cards from '../partials/Contacts'
import Categories from '../components/Categories'
import Properties from '../components/Properties'
import Headline from '../components/Headline'
import User from '../components/User'
//import Tag from '../partials/Tag'
import Filters from '../partials/Filters'
import ReactMarkdown from 'react-markdown'
import { updateBodyStyle, jumpToTop } from '../functions'
import cms from '../cms.json'
import Badges from '../partials/Badges'

class Bio extends Component {
    constructor(props) {
        super(props)

        this.state = {
          portfolio: false, 
          baron: false
        }
        
        this.categories = false
        this.portfolio     = false
        this.baron    = false
        this.panStyle   = ''
        this.heaStyle   = ''
        this.tabStyle   = 'w-full'

        this.sortList   = this.sortList.bind(this)
        this.parseProps = this.parseProps.bind(this)
    }
  
  sortList(event) {
    if( event ) {
      let target = event.target
      let filter = target.parentNode.getAttribute('data-filter')
      let week  = this.props.sortWeek(filter, this.props.portfolio)

      this.setState({ portfolio: week })
    }else{
      this.setState({ portfolio: this.props.portfolio })
    }
  }

  parseProps(){
    //Global
    if( this.props.heaStyle ) this.heaStyle =  this.props.heaStyle
    if( this.props.panStyle ) this.panStyle =  this.props.panStyle
    if( this.props.tabStyle ) this.tabStyle =  this.props.tabStyle
    if( this.props.bgStyle ) this.bgStyle = this.props.bgStyle
    
    if( this.props.barons ) {
      let user = this.props.barons.filter(baron => baron.id === this.props.id)
      if(user){
        user = user[0]
        this.setState({baron:user})

        this.baron = user
        this.id = this.props.id
        
        //Page Props
        if( this.props.activeFilters ) this.activeFilters = this.props.activeFilters
        if( this.categories ) this.categories = false
        if( this.props.buildUserCategories ){
          this.props.buildUserCategories(user,'profile')
          this.sortList()
        }
      }
    }
  }
  
  componentDidMount() {
    jumpToTop()
    updateBodyStyle()

    if(this.props.barons) this.parseProps()
  }

  componentDidUpdate(){
    updateBodyStyle()
    if( this.props.barons && !this.baron || this.id !== this.props.id) {
      this.parseProps()
    }
  }

  render(){
//    console.log(this.panStyle, this.props)
    if( this.props.tabStyle ) {
      this.tabStyle = this.props.tabStyle
    }else if( this.categories ) {
  //    console.log(this.categories)
      this.tabStyle = 'w-full md:w-2/3 lg:3/4 lg:pl-4 lg:pr-0'
    }

    return (
      <div className="container h-screen p-4">
        { this.props.headline &&
        <Headline key="bio__headline" copy={this.props.headline}/>
        }
        { this.state.baron &&
        <>
        <User key="bio_user"
          id={this.state.baron.id}
          photo={this.state.baron.photo}
          feature={cms.bio.feature}
          first_name={this.state.baron.first_name} 
          last_name={this.state.baron.last_name} 
          social={this.state.social}
          rent={this.state.baron.rent}/>
        <section className="flex content-between">
        { this.categories &&
          <Categories 
            key="bio__categories" 
            has_search={this.props.has_search}
            searchLabel="Search Schedule" 
            use_portfolios={this.state.portfolio && true} 
            categories={this.categories}/>
        }
          <div className={`tab-content ${!this.categories ? ' px-0' : ''} ${this.tabStyle}`}>
            { this.props.has_filters &&
            <Filters key="bio__filters"
              sortList={this.sortList}
              filter={this.props.filters}
              activeFilters={this.props.activeFilters}/>
            }
            { this.state.baron.title &&
            <div data-information
              className="tab-pane fade active show" 
              role="tabpanel" id="tab--bio__information" 
              aria-labelledby="bio__information">
              <article data-panel
                className={this.panStyle}>
                <div className="p-3 md:p-4 col flex-col">
                { (this.state.baron.title && this.state.baron.company) &&
                  <h2 className="font-light">{`${this.state.baron.title}, ${this.state.baron.company}`}</h2>
                }
                { (this.state.baron.title && !this.state.baron.company) &&
                  <h2 className="font-light">{this.state.baron.title}</h2>
                }
                { this.state.baron.address &&
                  <address>{this.state.baron.address}</address>
                }
                { this.state.baron.bio &&
                  <h3 className={this.heaStyle}>Bio</h3>
                }
                { this.state.baron.bio &&
                  <ReactMarkdown source={this.state.baron.bio} 
                    escapeHtml={false}
                    key={`bio__bio--${this.props.id}`}/>
                }
                </div>
              </article>
              { this.state.baron.badges &&
              <Badges panStyle={this.panStyle} badges={this.state.baron.badges} makeBadge={this.props.makeBadge}/>
              }
              { ( this.state.baron.contacts || this.state.baron.badges || this.state.baron.games ) &&
              <Cards panStyle={this.panStyle} 
              contacts={this.state.baron.contacts} badges={this.state.baron.badges} games={this.state.baron.games}/>
              }
            </div>
            }
            { this.state.baron.properties &&
            <div data-schedule
              className="tab-pane fade" 
              role="tabpanel" id="tab--bio__schedule" 
              aria-labelledby="bio__schedule">
              <div className="tab-content">
                <div data-schedule--day
                  className={`tab-pane fade${e === 0 && " active show"}`} 
                  role="tabpanel" id={`tab--bio__schedule--${this.state.portfolio.id}`} 
                  aria-labelledby={`bio__schedule--${this.state.portfolio.id}`}>
                  <Properties 
                    key={`bio__schedule--portfolios--${this.state.portfolio.id}`} 
                    has_tags={this.props.has_tags}
                    divStyle="w-full" 
                    list={this.state.properties}
                    barons={this.props.barons}/>
                </div>
              </div>
            </div>
            }
          </div>
        </section>
          </>
          }
      </div>
    )
  }
}

export default Bio