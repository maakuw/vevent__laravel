import React, {Component} from 'react'
import Password from '../components/Password'
import {jumpToTop, updateBodyStyle} from '../functions'

class Forgot extends Component {
    componentDidMount(){
        if(this.props) {
            //Global Functions
            jumpToTop()
            updateBodyStyle()
        }
    }

    render() {
        return  <Password key="signup__login" 
                     event={this.props.event ? this.props.event.title : this.defaults.event.title}
                     unsetUser={this.props.unsetUser}
                     forgot_headline={this.props.forgot_headline}
                     forgot_copy={this.props.forgot_copy}
                     confirm_headline={this.props.confirm_headline}
                     confirm_copy={this.props.confirm_copy}
                     errors={this.props.errors}
                     is_submitted={this.props.is_submitted}
                     password={this.props.password}/>
    }
}

export default Forgot