import React, {Component} from 'react'
import Button from '../components/Button'
import Headline from '../components/Headline'
import Filters from '../partials/Filters'
import Search from '../partials/Search'
import Summary from '../components/Summary'
import { Table } from 'antd'
import { updateBodyStyle, jumpToTop } from '../functions'
import cms from '../cms.json'
import "../../../node_modules/antd/dist/antd.min.css"

class Tenant extends Component {
  constructor(props){
    super(props)

    this.headline = false
    this.summary = {}
    this.tenants = false

    this.state = {
      tenants: this.tenants,
      selectedTenants: []
    }

    this.sortList = this.sortList.bind(this)
    this.parseProps = this.parseProps.bind(this)
  }
  
  sortList(event) {
    if( event ) {
      let target = event.target
      let filter = target.parentNode.getAttribute('data-filter')

      if( this.props.activeFilters ) {
        let index = this.props.activeFilters.indexOf(filter)

        if( index > -1 ) {
          this.props.activeFilters.splice(index,1)
        }else{
          this.props.activeFilters.push(filter)
        }
                
        let tenantSorted = []

        if( tenant.filters ) {
            if( tenants.filters.indexOf(act) !== -1 ) {
            if( tenantSorted.indexOf(this.tenant.id) === -1 ) {
                tenantSorted.push(this.tenant.id)
            }
            }
        }

        if( tenantsSorted.length > 0 ){
          this.setState({ tenants: tenantsSorted })
        }else{
          console.error(`There were no sorted results returned for the filter "${filter}"`)
        }
      }else{
        console.error('There are no active filters!')
      }
    }else{
      this.setState({ tenants: this.tenants })
    }
  }

  parseProps(){
    if( this.props.divStyle ) this.divStyle = this.props.divStyle
    if( this.props.headline ) this.headline = this.props.headline
    if( this.props.summary ) this.summary = this.props.summary
    if( this.props.bgStyle ) this.bgStyle = this.props.bgStyle
      
    if( this.props.tenants ) {
      if( this.props.tenants !== this.tenants ) {
        this.tenants = this.props.tenants
        this.sortList()
      }
    }
  }

  componentDidUpdate(){
    jumpToTop()
    updateBodyStyle()
    if(this.props) this.parseProps()
  }

  componentDidMount(){
    updateBodyStyle()
    if( this.props ) this.parseProps()
  }

  render() {
    return (
      <div className={cms.wrapStyle}>
        { this.props.headline &&
          <Headline key="tenants__headline" copy={this.props.headline}/>
        }
        <Summary key="tenants__summary" 
         headline={this.props.headline}
         copy={this.props.summary.copy} 
         logo={this.props.summary.logo}/>
        { this.props.has_search  ?
        <div className={cms.barons.subnav.divStyle}>
          <div className={this.props.has_filters ? ' md:w-1/4' : '' + cms.barons.subnav.search.divStyle}>
            <Search key="tenants__search" 
             filterText={this.props.filterText}
             target={this} 
             use_tenant={true}/>
          </div>
          { this.props.has_filters &&
          <div className="w-full md:w-3/4 px-0 md:px-2">
            <Filters key="tenants__filters"
             sortList={this.sortList}
             filters={this.props.filters}
             activeFilters={this.props.activeFilters}/>
          </div>
          }
        </div>
        :
        <Filters key="tenants__filters"
         sortList={this.sortList}
         filters={this.props.filters}
         activeFilters={this.props.activeFilters}/>
        }
        <Table key="tenants__user" dataSource={this.state.tenants} className="w-full"
          loading={ this.state.tenants.length ? false : true}
          rowKey={(record) => {
            return `tenants__table__row--${record.id}`
          }}
          rowSelection={{
            onChange: rows => {
              this.setState({ selectedTenants: rows })
            }
          }}
          rowClassName={(record) => {
            return "ant-table-clickable"
          }}
          columns={[
            {
              title: 'Id',
              dataIndex: 'id',
              key: 'id',
              align: 'center', 
              width: '1rem', 
              defaultSortOrder: 'descend',
              sorter: (a,b) => a.id - b.id
            },{
              title: 'First Name',
              dataIndex: 'first_name',
              key: 'first_name', 
              sorter: (a,b) => a.first_name - b.first_name
            },{
              title: 'Last Name',
              dataIndex: 'last_name',
              key: 'last_name', 
              sorter: (a,b) => a.last_name - b.last_name
            },{
              title: 'Address',
              dataIndex: 'address',
              key: 'address', 
              sorter: (a,b) => a.address - b.address
            },{
              title: 'Rent',
              dataIndex: 'rent',
              key: 'rent', 
              sorter: (a,b) => a.rent - b.rent
            },{
              title: 'Action',
              dataIndex: 'Action',
              key: 'Action',
              align: 'center',
              width: '2rem',
              render: (text, record, index) => {
                return (
                <Button key={"edit_button--"+index} type="dropdown" is_enabled={true} 
                 ctas={[
                  {
                    label: 'Edit', 
                    target: '#main__modal_window', 
                    callback: () => {
                      this.props.editModal(`Edit Tenant #${record.id}`, '',[
                      {
                        label: 'id',
                        id: 'id',
                        type: 'hidden',
                        value: record.id
                      },{
                        label: 'First Name',
                        id: 'name',
                        type: 'text', 
                        required: true, 
                        style: 'w-full', 
                        value: record.first_name
                      },{
                        label: 'Last Name',
                        id: 'name',
                        type: 'text', 
                        required: true, 
                        style: 'w-full', 
                        value: record.last_name
                      },{
                        label: 'Address',
                        id: 'address',
                        type: 'text', 
                        required: true, 
                        style: 'mt-2 md:w-1/2', 
                        value: record.address
                      },{
                        label: 'Rent',
                        id: 'rent',
                        type: 'text', 
                        required: true, 
                        style: 'mt-2 md:w-1/2', 
                        value: record.rent
                      }
                    ],(obj) => {
                      closeModal(this.props.resetModal)
                      this.setState({loading: true})
                      this.props.editTenant(obj, (pr) => {
                        this.setState({programs: false})
                        let np = this.state.programs.map(p => {
                          if(p.id !== pr.id){
                            return p
                          }else{
                            return pr
                          }
                        })
                        this.setState({programs: np, loading: false})
                        })
                      })
                    }
                  },{
                    label: 'Delete',
                    target: '#main__modal_window', 
                    style: 'text-danger',
                    callback: () => {
                      this.props.deleteModal(`Delete "${record.name}"`, '',[
                        {
                          label: 'id',
                          id: 'id',
                          type: 'hidden',
                          value: record.id
                        }
                      ],() => {
                        closeModal(this.props.resetModal)
                        setTimeout(() => {this.props.deleteTenant(record.id)}, 3600)
                      })
                    }
                  }]}/>
                )
              }
            }
          ]}
          pagination={'bottom'}/>
      </div>
    )
  }
}

export default Tenant