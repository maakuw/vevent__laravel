import React, {Component} from 'react'
import ReactMarkdown from 'react-markdown'
import Stage from '../components/Stage'
import Categories from '../components/Categories'
import Contacts from '../components/Tenants'
import Infobar from '../components/Infobar'
import { jumpToTop, updateBodyStyle } from '../functions'

class Area extends Component {
    constructor(props) {
        super(props)
        
        this.divStyle = "container pt-0 lg:pt-4 relative"
        this.categories = [{
            slug: `session__information`,
            href: `#tab--session__information`,
            label: `About`
        }]
        this.id = false
        this.events = false
        this.featuredSrc = false
        this.firstName = ``
        this.headline = false
        this.lastName = ``
        this.username = ``
        this.userTitle = ``
        this.userSubtitle = ``
        this.userPhoto = ``
        this.bgStyle = {}
        this.maiStyle = this.props.maiStyle
        this.panStyle = "bg-tertiary p-4 mb-4"
        this.navStyle = "flex display-4 my-auto text-secondary"
        this.tabStyle = ''
        this.nextProperty = false
        this.prevProperty = false
        this.bg = false
        this.vidStyle = {
            top: false,
            left: false,
            minWidth: false,
            width: false,
            minHeight: false,
            height: false
        }
        this.src = false
        this.poster = false
        this.user = false
        this.state = {
            events: this.events,
            activePanelHref: '#tab--session__information'
        }
        this.copy = ''
        this.contactSlug = `contact`
        this.heaStyle = "text-lg font-bold text-primary uppercase"
        this.links = false
        this.roster = [] 
        this.visiting = {}
        this.title = ''
        this.address = false
        this.phone = false

        this.sortList = this.sortList.bind(this)
    }

  sortList(event) {
    let property = this.props.property ? this.props.property.events : false
    if( event ) {
      let target = event.target
      let filter = target.parentNode.getAttribute('data-filter')
      let week = this.props.sortWeek(filter, property)

      this.setState({events: week})
    }else{
      this.setState({events: property})
    }
  }

  parseProps(){
    if( this.props.headline ) this.headline = this.props.headline
    if( this.props.copy ) this.copy = this.props.copy
    if( this.props.bgStyle ) this.bgStyle = this.props.bgStyle
    if( this.props.navStyle ) this.navStyle = this.props.navStyle

    if( this.props.properties ) {
    /*
    if( this.props.moderator ) {
        this.moderator = this.props.moderator
        if( this.moderator.surname ) {
            this.userTitle = <h5 key="property_user__header">{`${this.moderator.firstName} ${this.moderator.surname}`}</h5>
        }else{
            this.userTitle = <h5 key="property_user__header">{`${this.moderator.firstName}`}</h5>
        }

        if( this.moderator.title ) {
            this.userSubtitle = <p className="mb-0" key="property_user__desc">{this.moderator.title}{this.moderator.company ? this.moderator.company + ' - ' : ''}</p>
        }else{
            this.userSubtitle = ''
        }

        if( this.moderator.company ) {
            this.userCompany = <h5 key="property_user__company" className="mt-4 mb-1">{this.props.property.title}</h5>
        }

        if( this.moderator.imageUrl ) {
            this.userPhoto = <figure className="flex">
                                <div className="sm:w-1/2-3">
                                    <div data-backgrounder style={{backgroundImage:`url(${this.moderator.imageUrl})`}}
                                            className="mb-4 mb-md-0 coin">
                                        <FeaturedImg
                                            key = {`baron--featured_img${this.id}`}
                                            src={this.moderator.imageUrl}/>
                                    </div>
                                </div>
                                <figcaption className="flex flex-col content-center">
                                    { this.userTitle }
                                    { this.userSubtitle }
                                    { this.userEmail &&
                                    <a href={`mailto:${this.userEmail}`}>{this.userEmail}</a>
                                    }
                                </figcaption>
                            </figure>
        }
    }*/

    }
  }

  componentDidMount(){
    jumpToTop()
    updateBodyStyle()
  }

  componentDidUpdate(){
    jumpToTop()
    updateBodyStyle()
    if( this.props ) this.parseProps()
  }

  renderCategories(categories) {
    // Pulled from Categories component
    return (
      <aside className="w-full md:w-1/3 col-lg-3">
        <nav data-categories className="flex mb-3">
          <div role="tablist" className="nav w-full flex-col nav-pills">
          { categories.map(c => {
            const isActive = c.href === this.state.activePanelHref;
            return (
              <span key={c.href}
                className={(isActive ? 'active ' : '') + 'nav-item w-full flex content-start nav-link text-lg w-full flex py-2'}
                style={{cursor: 'pointer'}}
                onClick={() => this.setState({activePanelHref: c.href})}>
                <span className="mb-0 text-left w-full">{c.label}</span>
              </span>
            )
          })}
          </div>
        </nav>
      </aside>
    )
  }

  render(){
    let categories =[{
      slug: `session__information`,
      href: `#tab--session__information`,
      label: `About`
    }]
    /*
    if( this.links ) {
      categories = [{
        slug: `session__information`,
        href: `#tab--session__information`,
        label: `About`
      },{
        slug: `session__links`,
        href: `#tab--session__links`,
        label: `Related Links`,
      }]
    }
    */  
    return (
      <div className="container py-4">
        { this.props.property &&
        <Stage key="session__stage"
         property={this.props.property.info}
         src={this.props.property.feature}
         poster={false}
         bg={this.props.property.feature}
         use_standard_video={true}
         employee={false} 
         contactSlug={this.contactSlug}
         checkVideo={this.props.checkVideo}
         clearSession={this.props.clearSession}
         vidStyle={this.vidStyle}/>
        }
    { this.props.property &&
      <Infobar key="session__infobar"
        headline={this.props.area.name} 
        artStyle={`flex items-center content-between bg-tertiary py-4 py-sm-0`}
        heaStyle={`mr-auto mr-sm-2 mr-auto sm:w-1/2-0 lg:mr-3 py-sm-4 pl-0 pr-0 pl-md-0 pr-md-3 lg:py-4 text-center`} 
        divStyle={"flex flex-col flex-sm-flex content-center mb-3 mb-sm-auto"}
        navStyle={"flex wrap content-center w-full pb-4 lg:pb-0"}
        cta={[{
        style: `flex flex-col items-center justify-center p-2 text-xs text-white bg-primary d-lg-none mr-auto md:mr-2`,
        label: <span>+&nbsp;Connect</span>,
        href: this.contactSlug
        },{
        style: ` flex flex-col items-center justify-center p-2 text-white text-xs bg-primary lg:hidden mr-2 md:mr-auto`,
        label: <span>+&nbsp;Meeting</span>,
        href: false
        }]}
        visiting={false}
        parseTitle={this.props.parseTitle}/>
    }
      <section className="flex content-between mt-4">
        {categories.length > 1 && this.renderCategories(categories)}
        <div className={`tab-content w-full${categories.length > 1 ? 
        ' md:w-2/3 lg:w-3/4 lg:pl-4 lg:pr-0' : ' px-3 lg:px-0'}`}>
            {((this.props.property || this.events) && this.state.activePanelHref === '#tab--session__information') &&
          <div data-panels className="tab-pane fade active show" role="tabpanel" id="tab--session__information">
            { this.props.property.description &&
            <article data-panel className={this.panStyle}>
              <div className="p-3 p-md-4 col flex-col">
                <h2 className={this.heaStyle}>About</h2>
                { this.props.property.description &&
                <ReactMarkdown source={this.props.property.description}
                 escapeHtml={false}
                 key={`property_p--${this.props.property.id}`}/>
                }
              </div>
            </article>
            }
            { this.props.area.contact &&
              <article data-panel className={this.panStyle}>
                <div className="p-3 md:p-4 col flex-col">
                  <h2 className="text-lg font-bold uppercase">Contact</h2>
                    { this.userPhoto }
                    { this.userCompany }
                  <ReactMarkdown source={this.props.area.contact}
                    escapeHtml={false}
                    key={`property_contact--${this.props.property.id}`}/>
                </div>
              </article>
              }
            </div>
            }
            { (this.links && this.state.activePanelHref === '#tab--session__links') && 
            <div data-links
             className="tab-pane fade active show"
             role="tabpanel" id="tab--session__links">
              <article data-panel className={this.panStyle}>
                <div className="p-3 md:p-4 col flex-col">
                  <h2 className={this.heaStyle}>Related Links</h2>
                  <nav className="flex flex-col">
                  { this.links.map( link => {
                    return (
                      <a href={link.url} target="_blank" rel="noopener noreferrer">
                        <svg className="icon primary mr-2">
                          <use xlinkHref="#icon__download"/>
                        </svg>
                        <span>{link.name}</span>
                      </a>
                    )
                  } )}
                  </nav>
                </div>
              </article>
            </div>
            }
            { this.roster &&
            <div data-my_barons
             className="tab-pane fade"
             role="tabpanel" id="tab--session__roster"
             aria-labelledby="session__roster">
              <Contacts key="session__roster" roster={this.roster}/>
            </div>
            }
          </div>
        </section>
      </div>
    )
  }
}

export default Area