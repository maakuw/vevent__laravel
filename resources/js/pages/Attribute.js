import React, {Component} from 'react'
import Categories from '../components/Categories'
//import Events from '../components/Events'
import Headline from '../components/Headline'
import Employee from '../components/Employee'
import Filters from '../partials/Filters'
import Search from '../partials/Search'
import ReactMarkdown from 'react-markdown'
import {jumpToTop, updateBodyStyle, randomID} from '../functions'

class Attribute extends Component {
  constructor(props) {
    super(props)

    this.state = {
      events: false
    }
    
    this.categories = false
    this.events = false
    this.username = ``
    this.userTitle = ``
    this.userSubtitle = ``
    this.tabStyle = ''

    this.sortList = this.sortList.bind(this)
  }
  
  sortList(event) {
    let property = this.property ? this.property.events : false
    if( event ) {
      let target = event.target
      let filter = target.parentNode.getAttribute('data-filter')
      let week = this.props.sortWeek(filter, property)

      this.setState({
        events: week
      })
    }else{
      this.setState({
        events: property
      })
    }
  }

  componentDidMount() {
    jumpToTop()
    updateBodyStyle()
  }

  render(){
    if( this.categories ) {
      if( this.categories[0] ) {
          if( this.categories[0].submenu.length > 1 ) this.tabStyle = 'w-full md:w-2/3 lg:w-3/4 lg:pl-4 lg:pr-0'
      }
    }

    if( this.props.attribute ) {
      if( this.props.attribute.last_name ) {
          this.userTitle = <h5 key="property_user__header">{`${this.props.attribute.first_name} ${this.props.attribute.last_name}`}</h5>
      }else{
          this.userTitle = <h5 key="property_user__header">{`${this.props.attribute.first_name}`}</h5>
      }

      if( this.props.attribute.name ) {
          this.userSubtitle = <p className="mb-0" key="property_user__desc">{this.props.attribute.name ? this.props.attribute.name + ' - ' : ''}{this.props.attribute.company}</p>
      }else{
          this.userSubtitle = ''
      }

      if( this.props.attribute.email ) {
          this.userEmail = this.props.attribute.email
      }else{
          this.userEmail = ''
      }

      if( this.props.attribute.company ) {
          this.userCompany = <h5 key="property_user__company" className="mt-4 mb-1">{this.props.property.title}</h5>
      }
    }

    return (
      <div className="container py-4">
        { this.props.headline &&
        <Headline key="property__headline" copy={this.props.headline}/>
        }
        { this.props.property &&
        <Employee key="property_employee"
          feature={this.props.attribute.feature}
          title={this.props.property.title} 
          employee={this.state.attribute} 
          color={this.props.property.color}
          use_contact={this.props.use_contact}
          use_rent={this.props.use_rent}
          employeeSrc={this.props.property.logo}/>
        }
        { ( this.props.property || this.state.events ) &&
        <section className="flex content-between">
          { this.categories &&
          <Categories 
            key="property__categories" 
            has_search={this.props.has_search}
            searchLabel="Search Information" 
            searchSubmit={this.props.searchSubmit}
            categories={this.categories}/>
          }
          <div className={`tab-content${this.tabStyle}`}>
            { ( !this.categories && this.props.has_search ) &&
            <Search key="property__search" 
              filterText={this.props.filterText}
              target={this}
              searchSubmit={this.props.searchSubmit}/>
            }
            { this.categories &&
            <Filters key="property__filters"
              sortList={this.sortList}
              filter={this.props.filters}
              activeFilters={this.props.activeFilters}/>
            }
          { this.props.attribute.description &&
          <div data-information
            className="tab-pane fade active show" 
            role="tabpanel" id="tab--property__information" 
            aria-labelledby="property__information">
            <article data-panel className="bg-tertiary p-4 mb-4">
              <div className="px-4">
                <h2 className="h4 uppercase">About</h2>
                { this.props.attribute.description &&
                <ReactMarkdown source={this.props.attribute.description}
                 escapeHtml={false} key={`property_p--${this.props.attribute.id}`}/>
                }
              </div>
            </article>
            <article data-panel className="contact bg-tertiary p-4 mb-4">
              <div className="px-4 col flex-col">
                <h2 className="h4 uppercase">Contact</h2>
                { this.userTitle }
                { this.userSubtitle }
                { this.userEmail &&
                <a href={`mailto:${this.userEmail}`}>{this.userEmail}</a>
                }
                { this.userCompany }
                <ReactMarkdown source={this.props.property.contact}
                  escapeHtml={false} key={`property_contact--${this.props.property.id}`}/>
              </div>
            </article>
          </div>
          }
          { this.state.events &&
          <div data-schedule
            className="tab-pane fade" 
            role="tabpanel" id="tab--property__schedule" 
            aria-labelledby="property__schedule">
            <div className="tab-content">
            {/* this.state.events.map( (day,d) => {
              return (
                <div data-schedule--day
                  className={`tab-pane fade${d === 0 && "active show"}`} 
                  role="tabpanel" id={`tab--property__schedule--${day.slug}`} 
                  aria-labelledby={`property__schedule--${day.slug}`}>
                  <Events 
                    key={`property__schedule--events--${day.slug}`}
                    has_tags={this.props.has_tags}
                    divStyle="w-full" 
                    list={day}
                    users={this.props.users}
                    use_contact={this.props.use_contact}
                    use_offsite_player={this.props.use_offsite_player}/>
                </div>
              )
            }) */}
            </div>
          </div>
          }
        </div>
      </section>
      }
    </div>
    )
  }
}

export default Attribute