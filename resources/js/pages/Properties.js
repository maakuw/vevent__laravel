import React, {Component} from 'react'
import Categories from '../components/Categories'
import Headline from '../components/Headline'
import Filters from '../partials/Filters'
import Search from '../partials/Search'
import {updateBodyStyle, parseCount} from '../functions'
import cms from '../cms.json'
import Property from '../partials/Property'
import Hero from '../components/Hero'

class Properties extends Component {
  constructor(props) {
    super(props)

    this.tabStyle = ''
    this.sorted = false

    this.state = {
      portfolios: false,
      houses: false
    }

    this.sortList = this.sortList.bind(this)
    this.parseProps = this.parseProps.bind(this)
    this.buildCategories = this.buildCategories.bind(this)
  }

  buildCategories(){
    return this.props.portfolios.map( (portfolio ) => {
      return {
        label: 'Houses',
        slug: 'houses',
        href: '#tab--houses',
        count: parseCount(portfolio.properties.length)
      }
    })
  }
  
  sortList(event) {
    if( event ) {
      this.sorted = false
      let target = event.target
      let filter = target.parentNode.getAttribute('data-filter')
      
      if( this.props.activeFilters ) {
        let index = this.props.activeFilters.indexOf(filter)

        if( index > -1 ) {
            this.props.activeFilters.splice(index,1)
        }else{
            this.props.activeFilters.push(filter)
        }
        
        let housesSorted =[]

        this.props.portfolios.forEach(portfolio => {
          this.activeFilters.forEach(act => {
            if( portfolio.filters ) {
              if( portfolio.filters.indexOf(act) !== -1 ) {
                if( housesSorted.indexOf(portfolio) === -1 ) {
                  housesSorted.push(portfolio)
                }
              }
            }
          })
        })

        this.setState({
          houses: housesSorted
        })
      }else{
        console.error('There are no active filters!')
      }
      this.sorted = true
    }else{
      this.setState({
        houses: this.props.portfolios.map(portfolio => portfolio.properties)
      })
      this.sorted = true
    }
  }

  parseProps(){
    if( this.props.portfolios ) {
      this.tabStyle = ' w-full md:w-2/3 lg:w-3/4 px-0 lg:pl-4 lg:pr-0'
      if( !this.sorted ) {
        this.categories = this.buildCategories()
        this.sortList()
      }
    }
  }

  componentDidMount() {
    updateBodyStyle()
    this.parseProps()
  }

  componentDidUpdate() {
    if( this.props.portfolios && this.props.portfolios != this.state.portfolios ) this.parseProps()
  }

  render(){
    return (
      <div className={cms.wrapStyle}>
        { this.props.headline &&
        <Headline key="properties__headline" copy={this.props.headline}/>
        }
        <Hero key="properties__hero" 
         headline={this.props.headline}
         subheadline={this.props.subheadline}
         capStyle={'p-4 flex wrap w-full h-33vh content-between items-center uppercase'}
         feature={this.props.feature}/>
        { ( this.props.has_search && !this.categories ) ?
        <div className="flex">
          <div className="pl-0 w-full md:w-1/4">
            <Search key="properties__search" 
             filterText={this.props.filterText}
             target={this}/>
          </div>
          { this.props.activeFilters &&
          <div className="w-full md:w-3/4 px-0 md:px-2">
            <Filters key="properties__filters"
             sortList={this.sortList}
             filter={this.props.filters}
             activeFilters={this.props.activeFilters}/>
          </div>
          }
        </div>
        :
        <Filters key="properties__filters"
         sortList={this.sortList}
         filter={this.props.filters}
         activeFilters={this.props.activeFilters}/>
        }
        <section className="flex content-between">
        { this.categories &&
          <Categories 
            key="properties__categories" 
            has_search={this.props.has_search}
            searchLabel="Search Propteries" 
            categories={this.categories}/>
        }
          <div className={`tab-content${this.tabStyle}`}>
            { this.props.filters &&
            <Filters key="properties__filters"
             sortList={this.sortList}
             filter={this.props.filters}
             has_search={this.props.has_search}
             activeFilters={this.props.activeFilters}/>
            }
            <div data-houses
             className="tab-pane fade active"
             role="tabpanel" id="tab--houses" 
             aria-labelledby="houses">
            { this.state.houses &&
              this.state.houses.map((house) => {
                house = house[0]
                return (
                <Property key={`property--${house.id}`}
                  property={house}
                  filters={this.props.filters}
                  activeFilters={this.props.activeFilters}/>
                )
              }) 
            }
            </div>
          </div>
        </section>
      </div>
    )
  }
}

Properties.defaultProps = {
  headline: '',
  summary: false, 
  filters: false,
  activeFilters: [], 
  has_search: false, 
  bgStyle: false, 
  ctaLabel: false, 
  tabStyle: ' w-full'
}

export default Properties