import React, {Component} from 'react'
import Headline from '../components/Headline'
import Hero from '../components/Hero'
import Floorplan from '../components/Floorplan'
import { jumpToTop, updateBodyStyle } from '../functions'

class WalkTheFloor extends Component {
  constructor(props) {
    super(props)
  }
    
  componentDidMount(){
    jumpToTop()
    updateBodyStyle()
  }

  render(){
    return (
      <div className="container py-4">
        { this.props.headline &&
        <Headline key="index__headline" 
         copy={this.props.headline}/>
        }
        { this.props.hero &&
        <Hero
         key="walk__hero"
         headline={this.props.hero.headline}
         subheadline={this.props.hero.subheadline}
         feature= '/img/hero__property.jpg'
         heaStyle="flex lg:pt-4"
         cta={{
          label: 'List View',
          to: '/properties'
         }}/>
        }
        <Floorplan key="walk_floorplan"
         floors={this.props.floors}
         changeFloor={this.props.changeFloor}/>
      </div>
    )
  }
}

export default WalkTheFloor