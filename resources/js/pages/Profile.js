import React, {Component} from 'react'
import Categories from '../components/Categories'
import Contacts from '../components/Tenants'
import Badges from '../partials/Badges'
import Cards from '../partials/Contacts'
import Properties from '../components/Properties'
import Exhibits from '../components/Exhibits'
import Tag from '../partials/Tag'
import Filters from '../partials/Filters'
import User from '../components/User'
import {randomID, updateBodyStyle} from '../functions'
import ReactMarkdown from 'react-markdown'
import Headline from '../components/Headline'

class Profile extends Component {
  constructor(props) {
    super(props)

    //this.sortList = this.sortList.bind(this)
  }
  //Part of this is global, and should be converted
  sortList(event) {
    //console.log('sortList', event)
    if( event ) {
      let propertySorted = []
      let rosterSorted = []
      let filter = event.target.parentNode.getAttribute('data-filter')
      let index = this.props.activeFilters.indexOf(filter)

      if( index > -1 ) {
        this.props.activeFilters.splice(index,1)
      }else{
        this.props.activeFilters.push(filter)
      }

      this.roster.forEach(ros => {
        this.props.activeFilters.forEach(act => {
          if( ros.filters.indexOf(act) !== -1 ) {
            if( rosterSorted.indexOf(ros) === -1 ) {
              rosterSorted.push(ros)
            }
          }
        })
      })
        
      this.properties.forEach(boo => {
        this.activeFilters.forEach(act => {
          if( boo.filters.indexOf(act) !== -1 ) {
            if( propertySorted.indexOf(boo) === -1 ) {
              propertySorted.push(boo)
            }
          }
        })
      })

      this.setState({
        propertyList: propertySorted ? propertySorted : false,
        rosterList: rosterSorted ? rosterSorted : false
      })
    }
  }

  parseProps(){
    if(this.props.user && !this.has_user){
      this.has_user = true
    }
        
    this.categories = []
    this.props.buildUserCategories(this.props.user,'profile')
    this.sortList()
  }

  componentDidMount() {
    updateBodyStyle()

    this.parseProps()
  }

  render() {
    let has_information = this.props.title > 0 || this.props.company || this.props.address || this.props.bio || this.props.interests > 0

    if( this.props.tabStyle ) {
      this.tabStyle = this.props.tabStyle
    }else if( this.categories && this.props.global_schedule && this.props.sessions ) {
      this.tabStyle = 'w-full md:w-2/3 lg:w-3/4 lg:pl-4 lg:pr-0'
    }else{
      this.tabStyle = 'w-full'
    }

    return (
      <div className="container h-screen">
        { this.props.headline &&
        <Headline key="profile_headline"
         copy={this.props.first_name+' '+this.props.last_name+' | '+this.props.headline}/>
        }
        <User key="profile_user"
         id={this.props.id}
         photo={this.props.photo} 
         feature={this.props.feature} 
         first_name={this.props.first_name} 
         last_name={this.props.last_name} 
         social={this.props.social}
         rent={this.rent}/>
        <section className="flex content-between">
        { ( this.props.sessions && ( this.categories || this.props.has_search ) ) &&
          <Categories key="information__categories" 
           searchLabel="Search Information" 
           sortWeek={this.props.sortWeek}
           target={this}
           use_events={false}
           use_barons={false} 
           use_information={true} 
           use_contactions={false} 
           categories={this.categories}/>
        }
          <div className={`tab-content ${( this.props.sessions && ( this.categories || this.props.has_search ) ) ? '' : ' px-0'} ${this.tabStyle}`}>
          { this.categories &&
            <Filters key="profile__filters"
             sortList={this.sortList}
             filter={this.props.filters}
             activeFilters={this.props.activeFilters}/>
          }
            <div data-information
             className="tab-pane fade active show" 
             role="tabpanel" id="tab--profile__information" 
             aria-labelledby="profile__information">
              { has_information &&
              <article id="about_me" data-panel className={this.panStyle}>
                <div className="px-4">
                { (this.props.title && this.props.company) &&
                  <h2 className="text-xl">{`${this.props.title}, ${this.props.company}`}</h2>
                }
                { (this.props.title && !this.props.company) &&
                  <h2 className="text-xl">{this.props.title}</h2>
                }
                { this.props.address &&
                  <address>
                    <ReactMarkdown source={this.props.address} escapeHtml={false}/>
                  </address>
                }
                { this.props.bio &&
                  <>
                  <h3 key="profile__header" className={this.heaStyle}>Bio</h3>
                  <ReactMarkdown key="profile__header--text" source={this.props.bio} />
                  </>
                }
                { this.props.interests &&
                  <h3 className={`mb-0 ${this.heaStyle}`}>Interests</h3>
                }
                { this.props.interests &&
                  <div className="flex wrap">
                  { this.props.interests.map(interest => {
                    return <Tag key={`interest_${interest}`} label={interest}/>
                  })}
                  </div>
                    }
                  </div>
                </article>
                }
                { this.props.badges &&
                <Badges panStyle={this.panStyle} badges={this.props.badges} makeBadge={this.props.makeBadge}/>
                }
                { ( this.props.contacts || this.props.badges || this.props.games ) &&
                <Cards panStyle={this.panStyle} contacts={this.props.contacts} badges={this.props.badges} games={this.props.games}/>
                }
            </div>
          { this.props.rosterList && 
            <div data-contacts
             className="tab-pane fade" 
             role="tabpanel" id="tab--profile__contacts" 
             aria-labelledby="profile__contacts">                
              <Contacts key="profile__contacts" 
                divStyle="w-full"
                roster={this.props.rosterList}
                barons={this.props.barons}/>
            </div>
          }
          { this.props.propertyList &&
            <div data-properties
             className="tab-pane fade" 
             role="tabpanel" id="tab--profile__properties" 
             aria-labelledby="profile__properties">
              <Exhibits key="profile__properties"
                divStyle="w-full"
                list={this.props.propertyList}/>
            </div>
          }
          </div>
        </section>
      </div>
    )
  }
}

Profile.defaultProps = {
  id: randomID(), 
  email: false,
  honorific: '',
  photo: false, 
  rent: 0, 
  badges: false, 
  level: false,     
  last_name: '',
  first_name: '',
  company: '',
  title: '', 
  bio: false,
  address: '',
  sessions: false, 
  contacts: false, 
  social: false, 
  rosterList: false,
  has_user: false, 
  buildUserCategories: () => {}, 
  tabStyle: 'w-full', 
  panStyle: "bg-tertiary p-4 mb-4", 
  bgStyle: false, 
  heaStyle: 'text-md font-bold text-primary uppercase'
}

export default Profile