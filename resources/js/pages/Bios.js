import React, {Component} from 'react'
import Tenants from '../components/Tenants'
import Headline from '../components/Headline'
import Summary from '../components/Summary'
import Filters from '../partials/Filters'
import Search from '../partials/Search'
import cms from '../cms.json'
import {updateBodyStyle} from '../functions'

class Bios extends Component {
  constructor(props){
    super(props)

    this.state = {
      barons: this.barons
    }

    this.barons = false

    this.sortList = this.sortList.bind(this)
    this.parseProps = this.parseProps.bind(this)
  }

  sortList(event) {
//    console.log('sortList', event)
    if( event ) {
      let target = event.target
      let filter = target.parentNode.getAttribute('data-filter')

      if( this.props.activeFilters ) {
        let index = this.props.activeFilters.indexOf(filter)

        if( index > -1 ) {
          this.props.activeFilters.splice(index,1)
        }else{
          this.props.activeFilters.push(filter)
        }
            
        let baronsSorted =[]

          this.barons.forEach(user_id => {
            this.props.activeFilters.forEach(act => {
              let user = this.props.getBio(user_id)
              if( user.filters ) {
                if( user.filters.indexOf(act) !== -1 ) {
                  if( baronsSorted.indexOf(user_id) === -1 ) baronsSorted.push(user_id)
                }
              }
            })
          })

          if( baronsSorted.length > 0 ){
            this.setState({barons: baronsSorted})
          }else{
            console.error(`There were no sorted results returned for the filter "${filter}"`)
          }
        }else{
          console.error('There are no active filters!')
        }
    }else{
      this.setState({
        barons: this.props.barons
      })
    }
  }

  parseProps(){
    if( !this.state.barons && !this.barons) {
      if( this.props.barons !== this.barons ) {
        this.barons = this.props.barons
        this.sortList()
      }
    }
  }

  componentDidMount() {
    updateBodyStyle()

    this.parseProps()
  }

  componentDidUpdate(){
    this.parseProps()
  }
    
  render(){
    return (
      <div className={cms.wrapStyle}>
        { this.props.headline &&
        <Headline key="barons__headline" copy={this.props.headline}/>
        }
        <Summary key="barons__summary" 
         headline={this.props.summary.headline}
         copy={this.props.summary.copy} 
         logo={this.props.summary.logo}
         artStyle={this.props.summary.artStyle}
         bgStyle={this.props.summary.background}
         figStyle={this.props.summary.figStyle}
         hStyle={this.props.summary.hStyle}
         imgStyle={this.props.summary.imgStyle}
         secStyle={this.props.summary.secStyle}/>
        { cms.has_search  ?
        <div className={cms.barons.subnav.divStyle}>
          <div className="pl-4 w-full md:w-1/4">
            <Search key="barons__search" 
              filterText={this.props.filterText}
              target={this}/>
          </div>
          { this.props.filters &&
          <div className="w-full md:w-3/4 px-0 md:px-2">
            <Filters key="barons__filters"
              sortList={this.sortList}
              filters={this.props.filters} 
              activeFilters={this.props.activeFilters}/>
          </div>
        }
        </div>
        :
        <Filters key="barons__filters"
          sortList={this.sortList}
          filters={this.props.filters} 
          activeFilters={this.props.activeFilters}/>
        }
        { this.state.barons &&
        <Tenants key="barons__barons" barons={this.state.barons}/>
        }
      </div>
    )
  }
}

Bios.defaultProps = {
  headline: false, 
  summary: {}, 
  filters: false, 
  filterText: 'Search Bios', 
  activeFilters: false, 
  barons: false
}

export default Bios