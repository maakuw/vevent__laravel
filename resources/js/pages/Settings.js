import React, {Component} from 'react'
import Headline from '../components/Headline'
import {updateBodyStyle} from '../functions'
import cms from '../cms.json'

class Settings extends Component {
  componentDidMount(){
    updateBodyStyle()
  }

  render() {
    return (
      <div className={cms.wrapStyle}>
        <Headline key="index__headline" copy={this.props.headline}/>
        <section className="flex content-between">
          <dl className={this.props.dlStyle}>
            <dt className={this.props.dtStyle}>Number of Tenants</dt>
            <dd className={this.props.ddStyle}>
              <div data-panel className="text-midnight bg-tertiary p-4 relative w-full flex flex-col">
                <span className={this.props.capStyle}>Tenants</span>
                <span className={this.props.numStyle}>{this.props.tenants ? this.props.tenants.length : 0}</span>
              </div>
            </dd>
            <dt className={this.props.dtStyle}>Number of Properties</dt>
            <dd className={this.props.ddStyle}>
              <div data-panel className="text-primary bg-tertiary p-4 relative w-full flex flex-col">
                <span className={this.props.capStyle}>Properties</span>
                <span className={this.props.numStyle}>{this.props.properties ? this.props.properties.length : 0}</span>
              </div>
            </dd>
          </dl>
          <dl className={this.props.dlStyle}>
            <dt className={this.props.dtStyle}>Rent Paid</dt>
            <dd className={this.props.ddStyle}>
              <div data-panel className="text-midnight bg-tertiary p-4 relative w-full flex flex-col">
                <span className={this.props.capStyle}>Rent Paid</span>
                <span className={this.props.numStyle}>{this.props.properties ? this.props.properties.length : 0}</span>
              </div>
            </dd>
            <dt className={this.props.dtStyle}>Bills Owed</dt>
            <dd className={this.props.ddStyle}>
              <div data-panel className="text-primary bg-tertiary p-4 relative w-full flex flex-col">
                <span className={this.props.capStyle}>Bills Owed</span>
                <span className={this.props.numStyle}>{this.props.bills ? this.props.bills.total : 0}</span>
              </div>
            </dd>
          </dl>
        </section>
      </div>
    )
  }
}

Settings.defaultProps = {
  has_filters: false, 
  activeFilters: [], 
  categories: [], 
  headline: 'Analytics', 
  bgStyle: false, 
  ddStyle: "w-full text-midnight bg-gray-100 p-4 relative flex flex-col rounded shadow-lg", 
  dtStyle: "hidden", 
  dlStyle: "flex content-between w-1/2 mb-3 mb-sm-4 lg:mb-3 space-x-8", 
  capStyle: "text-xl mb-0 uppercase text-center block", 
  numStyle: "text-3xl text-center font-bold"
}

export default Settings