import React, { Component } from 'react'
import Headline from '../components/Headline'
import Chat from '../components/Chat'
//import { GroupChat } from "../components/GroupChat"
import Categories from '../components/Categories'

class Talk extends Component {
    constructor(props) {
        super(props)

        this.defaults = {
            maiStyle: this.props.maiStyle,
            divStyle: `container pt-0 lg:pt-4 relative self-stretch`,
            bg: false,
            roomId: false,
            recent: [],
            contacts: []
        }

        this.is_wss = false
        this.maiStyle = this.defaults.maiStyle
        this.divStyle = this.defaults.divStyle
        this.headline = false
        this.recent = this.defaults.recent
        this.categories = false
        this.contacts = this.defaults.contacts
        this.bgStyle = {}
    }

    componentDidMount(){
        if( this.props ) {
            //Global Functions
            this.props.jumpToTop()
            this.props.
            //Page Vars
            if( this.props.maiStyle )  this.maiStyle = this.props.maiStyle
            if( this.props.divStyle ) this.divStyle = this.props.divStyle
            if( this.props.headline ) this.headline = this.props.headline
            if( this.props.categories ) this.categories = this.props.categories
            if( this.props.contacts ) this.contacts = this.props.contacts
            if( this.props.recent ) this.recent = this.props.recent
            if( this.props.bgStyle ) this.bgStyle = this.props.bgStyle
            if( this.props.roomId ) this.roomId = this.props.roomId
        }
    }

    render(){
        return  <main className={this.maiStyle} data-chat style={this.bgStyle}>
                    <div className={this.divStyle}>
                        { this.headline &&
                        <Headline
                            key="chat__headline" 
                            copy={this.headline}/>
                        }
                        { this.categories &&
                        <Categories
                            key="chat__categories"
                            categories={this.categories}/>
                        }
                        { this.is_wss ?
                        <Chat 
                         key="chat__window"
                         roomId={this.props.roomId}/>
                         :
                         "chat"
                        /*<GroupChat key="chat__window"/>*/
                        }
                    </div>
                </main>
    }
}

export default Talk