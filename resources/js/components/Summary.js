import React, {Component} from 'react';
import ReactMarkdown from 'react-markdown';
import FeaturedImg from '../partials/FeaturedImg';

class Summary extends Component {
  render() {
    let bgStyle = this.props.bgStyle ? { style: this.props.bgStyle } : {}
    return (
      <section data-summary className={this.props.secStyle} {...bgStyle}>
        { this.props.logo &&
        <figure className={this.props.figStyle}>
          <FeaturedImg 
           key = "hero--featured_img"
           divStyle = "block feature flex px-4"
           imgStyle = {this.props.imgStyle} 
           src = {this.props.logo.src}
           alt = {this.props.logo.alt}/>
        </figure>
        }
        <article className={this.props.artStyle}>
          <h2 className={this.props.hStyle}>{this.props.headline}</h2>
          { this.props.copy &&
            <ReactMarkdown source={this.props.copy} />
          }
        </article>
      </section>
    )
  }
}

Summary.defaultProps = {
  headline: '', 
  copy: false, 
  logo: false, 
  artStyle: "w-full md:w-7/12 px-4 pb-4 lg:pr-4 lg:ml-4", 
  secStyle: "flex content-start items-start pt-4 mb-4 border bg-white", 
  figStyle: "w-full md:w-5/12 lg:w-1/3 px-4", 
  hStyle: "text-4xl uppercase", 
  imgStyle: "display-4 uppercase font-bold mb-0 text-center mx-auto", 
  bgStyle: false
}

export default Summary;