import React, {Component} from 'react'

class Headline extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    if( this.props.copy ) {
      return (
        <header data-headline className="flex mb-2 w-full px-4 lg:px-0 pt-4">
          <h1 className="text-sm ml-0 mb-0 pl-0 uppercase font-bold">{this.props.copy}</h1>
        </header>
      )
    }else{
      return false
    }
  }
}

export default Headline