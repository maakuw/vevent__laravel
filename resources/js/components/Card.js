import React, {Component} from 'react'

class Card extends Component {
  render(){
    return (
      <dd data-card className={this.ddStyle}>
        { this.props.icon &&
        <figure className={`card-icon ${this.figStyle}`}>
          <svg className="icon absolute">
            <use xlinkHref={this.props.icon}/>
          </svg>
          <figcaption className={this.props.capStyle} style={{ transform: this.props.capOffset }}>
            {this.props.count}
          </figcaption>
        </figure>
        }
        { this.props.title || this.props.copy &&
        <div className={`card-body${this.props.divStyle ? ' ' + this.props.divStyle : ''}`}>
          { this.props.title &&
          <h3 className={`card-title ${this.props.h3Style ? ' ' + this.props.h3Style : ''}`}>
            {this.props.title}
          </h3>
          }
          { this.props.copy &&
          <p className="card-text">{this.props.copy}</p>
          }
        </div>
        }
      </dd>
    )
  }
}

Card.defaultProps = {
  title: '',
  copy: '',
  capStyle: 'h3 relative',
  ddStyle: 'card',
  divStyle: 'text-center',
  h3Style: 'h6 uppercase text-primary',
  pStyle: '',
  figStyle: "flex flex-col items-center mb-0",
  icon: false, 
  count: 0,
  capOffset: 'unset'
}

export default Card