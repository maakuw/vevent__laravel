import React, { Component } from 'react'
import Room from '../partials/Room'

class Chat extends Component {
    constructor(props) {
        super(props)

        this.title = ''
        this.logo = ''
        this.phone = ''
        this.is_aside = false
    }

    render(){

        if(this.props) {
            if( this.props.title ) this.title = this.props.title
            if( this.props.logo ) this.logo = this.props.logo
            if( this.props.is_aside ) this.is_aside = this.props.is_aside
            if( this.props.logo ) this.logo = this.props.logo
            if( this.props.phone ) this.phone = this.props.phone
        }

        if( this.is_aside ) {
            this.markup =   <aside data-chat-target>
                                <header className="p-3 flex
                                content-between bg-stoic">
                                    <h4 className="mb-0">Chat Window</h4>
                                </header>
                                <div className="px-3">
                                    <Room key="chat_room--aside" roomId={this.props.roomId}/>
                                </div>
                            </aside>
        }else{
            this.markup =   <Room key="chat_room--page" roomId={this.props.roomId} use_full={true}/>
        }

        return this.markup
    }
}

export default Chat