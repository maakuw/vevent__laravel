import React, {Component} from 'react'
import Property from '../partials/Property'

class Properties extends Component {
  render(){
    return (
      <div data-properties className={this.props.divStyle}>
        { this.props.properties.map(property => {
          return <Property key={"property" + property.id} property={property}/>
        }) }
      </div>
    )
  }
}

Properties.defaultProps = {
  properties: false, 
  divStyle: "w-full md:w-2/3 lg:w-3/4"
}

export default Properties