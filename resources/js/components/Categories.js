import React, {Component} from 'react'
import Search from '../partials/Search'
import { selectHouse } from '../functions'

class Categories extends Component {

  render(){
    return (
      <aside className="w-full md:w-1/3 col-lg-3">
          { this.props.has_search &&
          <Search
            key="categories_search" 
            filterText={this.props.filterText}
            target={this.props.target}
            use_properties={this.props.use_properties} 
            use_events={this.props.use_events} 
            use_information={this.props.use_information} 
            use_contactions={this.props.use_contactions} 
            use_barons={this.props.use_barons} />
          }
          <nav data-categories key="categories_nav" className="flex mb-3">
            <div role="tablist" 
              className="flex flex-wrap w-full flex-col nav-pills"
              aria-orientation="vertical">
              { this.props.categories.map( (item, i) => {
                let bStyle = `w-full flex content-start nav-link text-xl flex py-2 bg-transparent rounded-sm`
                let iStyle = i === 0 ? `active ${bStyle}` : bStyle
                let iCopy = <span className={`mb-0 text-left pointer-events-none ${item.count ? 'w-3/4' : 'w-full'}`}>{item.label}</span>
                let iCount = item.count ? <span data-count className="mb-0 pointer-events-none font-bold">&nbsp;{item.count}</span>:''
                let menu = false
                if(item.submenu){
                  menu = <details key={`${item.slug}--${i}`}
                          className={`relative ${iStyle}`}
                          data-bs-toggle="tab" 
                          role="tab" id={item.slug} 
                          aria-controls={`tab--${item.slug}`} 
                          aria-selected={i === 0 ? "true" : "false"}
                          href={item.href} open>
                          <summary className="flex w-full items-center">
                              {iCopy}
                              {iCount}
                          </summary>
                          <nav className="relative float-none mt-3 flex items-center dropdown-menu flex-col">
                            <div aria-orientation="vertical" role="tablist" className="w-full nav nav-pills">
                            { item.submenu.map( (subitem, s) => {
                              let has_count = subitem.count ? true : false
                              let iCopy = <span className={`mb-0 text-left ${has_count ? 'w-3/4' : 'w-full'}`}>{subitem.label}</span>
                              let iCount = has_count ? <span data-count className="mb-0 font-bold">&nbsp;{subitem.count}</span>:''
                              let is_tab = item.slug ? true : false
                              let anchor
                              if( is_tab ) {
                                anchor = <a key={item.slug+'__'+subitem.slug+'--'+s} 
                                            className={`dropdown-item ${s === 0 ? "active " : ""}${bStyle}`} href={subitem.href} 
                                            role="tab" id={subitem.slug} data-bs-toggle="tab" 
                                            aria-controls={`tab--${subitem.slug}`} 
                                            aria-selected={s === 0 ? "true" : "false"}
                                          >
                                            {iCopy}
                                            {iCount}
                                          </a>
                              }else{
                                anchor = <a key={item.slug+'__'+subitem.slug+'--'+s} 
                                            className={`dropdown-item ${s === 0 ? "active " : ""}${bStyle}`} href={subitem.href} 
                                            role="tab" id={subitem.slug} data-bs-toggle="tab" 
                                            aria-controls={`tab--${subitem.slug}`} 
                                            aria-selected={s === 0 ? "true" : "false"}
                                          >
                                            {iCopy}
                                            {iCount}
                                          </a>
                              }
                              return  anchor
                              })
                            }
                            </div>
                          </nav>
                        </details>
                }else if(item.slug) {
                  menu = <a key={`${item.slug}--${i}`} 
                            className={iStyle} 
                            role="tab" id={item.slug} 
                            aria-controls={`tab--${item.slug}`} 
                            aria-selected={i === 0 ? "true" : "false"}
                            onClick={selectHouse}
                            href={item.href}>
                              {iCopy}
                              {iCount}
                          </a>
                }
                return menu
              })
            }
          </div>
        </nav>
      </aside>
    )
  }
}

Categories.defaultProps = {
  categories: [], 
  has_search: true,
  searchLabel: 'Search',
  filterText: false, 
  target: false, 
  use_properties: false, 
  use_events: false, 
  use_information: false, 
  use_contactions: false, 
  use_barons: false
}


export default Categories