import React, {Component} from 'react';
import FeaturedImg from '../partials/FeaturedImg.js'
import { NavLink } from 'react-router-dom'
import { randomID, clickthrough } from '../functions.js'

class Hero extends Component {
  render() {
    let cta
    
    if(this.props) {
      if( this.props.cta ) {
        this.cta = this.props.cta
        if( this.props.cta.do_modal ) this.do_modal = this.props.cta.do_modal
      }
    }

    if( this.cta ) {
      if( this.cta.do_modal ) {
        cta = <a className={this.cta.style ? this.cta.style : this.aStyle}
                 href={this.cta.to} data-toggle="modal"
                 data-target={`#${this.cta.to}`}>
                {this.cta.label}
              </a>
      }else if( this.props.is_external ) {
        cta = <a target="_blank" 
                 className={this.cta.style ? this.cta.style : this.aStyle}
                 href={this.cta.to} rel="noopener noreferrer">
                {this.cta.label}
              </a>
      }else{
        cta = <NavLink className={this.cta.style ? this.cta.style : this.aStyle}
                    to={this.cta.to}>
                {this.cta.label}
              </NavLink>
      }
    }else{
      cta = false;
    }

    this.content = [
      <figure 
        key = "hero"
        data-hero 
        className={this.props.heaStyle ? 'flex ' + this.props.heaStyle : this.props.heaStyle}
        data-clickthrough 
        onClick={clickthrough}
        role="button">
          <div className={this.props.divStyle}>
            <div key="hero--caption"
                className={this.props.capStyle}
                style={{
                  backgroundImage: `url(${this.props.feature})`
                }}>
              { this.props.headline || this.props.subheadline &&
              <h2 className="text-4xl lg:px-4 lg:pt-4 sm:pb-4 mb-2 sm:mb-0 text-white">
                {this.props.headline}
                <span className="text-poppy ml-3 md:ml-0 block">{this.props.subheadline}</span>
              </h2>
              }
              { cta &&
              <nav className="lg:px-4 lg:pb-4 sm:pb-0 sm:w-1/2-0 md:ml-auto">
                {cta}
              </nav>
              }
              { this.props.feature &&
              <FeaturedImg 
                key = "hero--featured_img"
                src = {this.props.feature}/>
              }
            </div>
        </div>
      </figure>
    ]

    return this.content;
  }
}

Hero.defaultProps = {
  headline: '',
  subheadline: false, 
  feature: '/img/background-deco_house.jpg',
  cta: false, 
  style: 'py-4',
  is_external: false, 
  do_modal: false, 
  aStyle: 'block bg-poppy text-white', 
  capStyle: 'p-4 flex wrap w-full content-between items-center uppercase bg-center bg-cover', 
  divStyle: 'w-full', 
  heaStyle: 'flex pb-4 mb-0', 
  hero: false
}

export default Hero;