import React, {Component} from 'react'
//import Chat from './Chat'
//import { Player } from 'video-react'
//import { PropertyChannel } from './GroupChat';

class Stage extends Component {

  constructor(props) {
    super(props)

    this.defaults = {
        error: '',
        type: "application/x-mpegURL",
        src: false,
        bg: false,
        vidStyle: false,
        poster: '',
        style: false,
        fail_msg: `We're sorry, we know you wanted the truth. Sadly, your browser cannot handle the truth :(`
    }

    this.state = {
      error: null,
      fail_msg: false,
      showChat: false
    }

    this.aStyle = 'block bg-poppy text-white bg-small'
    this.navStyle = "flex content-end flex-col items-center text-center text-black"
    this.stage = false
    this.roomId = false
    this.use_standard_video = false
    this.error = null
  }

  componentDidUpdate(){
    if( this.props ){
      if( this.props.fail_msg ) this.fail_msg = this.props.fail_msg
      if( this.props.bg ) this.bg = this.props.bg
      if( this.props.roomId ) this.roomId = this.props.roomId
      if( this.props.src ) this.src = this.props.src
      if( this.props.poster ) this.poster = this.props.poster
      if( this.props.employee ) this.employee = this.props.employee
      if( this.props.use_standard_video ) this.use_standard_video = this.props.use_standard_video
      if( this.props.contactSlug ) this.contactSlug = this.props.contactSlug
      if( this.props.video ) {
        if( this.props.vidStyle ) this.vidStyle = this.props.vidStyle
      }
    }
  }

  toggleChat(event){
      event.preventDefault()
      let trigger = event.target.parentNode
      let target = document.querySelector('[data-chat-target]')
      
      trigger.classList.toggle('active')
      target.classList.toggle('show')

  }

  componentDidMount(){
    if( this.props ){
      if( this.props.fail_msg ) this.fail_msg = this.props.fail_msg
      if( this.props.bg ) this.bg = this.props.bg
      if( this.props.roomId ) this.roomId = this.props.roomId
      if( this.props.src ) this.src = this.props.src
      if( this.props.poster ) this.poster = this.props.poster
      if( this.props.employee ) this.employee = this.props.employee
      if( this.props.contactSlug ) this.contactSlug = this.props.contactSlug
      if( this.props.use_standard_video ) this.use_standard_video = this.props.use_standard_video
      if( this.props.video ) {
        if( this.props.vidStyle ) this.vidStyle = this.props.vidStyle
      }
      this.props.clearSession()
      if( this.props.duration ) this.props.checkVideo(this.props.duration)
    }
  }

  renderChatButton() {
    return (
      <div style={{position: 'absolute', top: 0, right: 0, zIndex: 20}}>
        <a href="#chat" className="block bg-poppy text-white rounded-0" onClick={() => this.setState(state => ({showChat: !state.showChat}))}>
          <svg className="icon h2 mt-2">
            <use xlinkHref="#icon__message"/>
          </svg>
        </a>
      </div>
    )
  }

  renderChatPanel() {
    return (
      <div style={{position: 'absolute', top: 0, right: 0, bottom: 0, width: '50%', zIndex: 10, display: 'flex'}}>
        <PropertyChannel propertyId={this.props.property.id} title={this.props.property.title + ' Chat'} />
      </div>
    )
  }

  renderChat() {
    return (
      <>
        {this.state.showChat && this.renderChatPanel()}
        {this.renderChatButton()}
      </>
    )
  }

  render() {
    if( this.state.error ) {
      this.error = <p>{this.state.error}</p>
    }
    
    let content
    let video = false
    let chat = false//<Chat key="stage--chat" is_aside={true} roomId={this.roomId} randomID={this.props.randomID}/>
    let stageNav
    let nav = false
    
    if( this.props.employee ) {
      stageNav = <nav key="stage_nav" data-employee_nav className="absolute hidden d-lg-flex items-stretch content-between w-full">
                    <a className={this.navStyle} href={`/${this.props.contactSlug}`}>
                      <span className="block text-center">Connect<br/>to Chat</span>
                      <svg className="icon mx-auto">
                        <use xlinkHref="#icon__plus"/>
                      </svg>
                    </a>
                    <a className={this.navStyle} 
                    href={`mailto:${this.props.employee.email}?subject=Kenworth%20Right%20Choice%20|%20Networking%20Appointment%20Request&body=Hello!%0D%0A%0D%0AI’m%20interested%20in%20setting%20up%20an%20appointment%20to%20talk%20with%20you%20further.%0D%0A%0D%0APlease contact%20me%20at%20your%20earliest%20convenience.`}>
                      <span className="block text-center">Schedule a Meeting</span>
                      <svg className="icon mx-auto">
                        <use xlinkHref="#icon__plus"/>
                      </svg>
                    </a>
                </nav>
/*
        nav = <nav
                key="stage_nav--chat"
                className="flex hidden 
                content-end items-center"
                style={this.nstyle}
                data-chat>
                <a
                  href="#chat"
                  className="block bg-poppy text-white rounded-0"
                  onClick={this.toggleChat}>
                  {this.props.session_id}
                  <svg className="icon h2 mt-2">
                      <use xlinkHref="#icon__message"/>
                  </svg>
                </a>
              </nav>*/
    }
//Dev note: not sending in the "type" because it didn't exist. Now that it does, we can ditch the "src" search
    if( this.props.src ) {
      if( this.props.src.search('vimeo') || this.props.src.search('youtube') ) {
        video = <iframe id="stage--video" title="Stage Video Player" src={this.props.src} width="640" height="360"
                  style={{
                    top: this.props.vidStyle.top,
                    left: this.props.vidStyle.left,
                    minWidth: this.props.vidStyle.minWidth,
                    width: this.props.vidStyle.width,
                    minHeight: this.props.vidStyle.minHeight,
                    height: this.props.vidStyle.height
                  }} frameBorder="0" allow="autoplay; fullscreen" allowFullScreen/>
      }else {
        video = <video id="stage--video" key="stage--video" poster={this.props.poster} controls
                  style={{
                    top: this.props.vidStyle.top,
                    left: this.props.vidStyle.left,
                    minWidth: this.props.vidStyle.minWidth,
                    width: this.props.vidStyle.width,
                    minHeight: this.props.vidStyle.minHeight,
                    height: this.props.vidStyle.height
                  }}>
                      <source src={this.props.src}/>
                      {this.fail_msg}
                </video>
      }
    }
    
    if ( this.props.bg ) {
      content = <section
                  key="stage--wrapper"
                  data-stage
                  className="flex"
                  data-backgrounder
                  style={this.props.bg ? {
                    backgroundImage: `url(${this.props.bg})`
                  } : {}}>
                      {video}
                      {chat}
                      {stageNav}
                      {nav}
                      {/*this.renderChat()*/}
                </section>
    }else{
      content = <section 
                  key="stage--wrapper" 
                  data-stage 
                  className="flex">
                    {video}
                    {chat}
                    {stageNav}
                    {nav}
                </section>
    }

    return content
  }
}

export default Stage