import React, {Component} from 'react'
import Properties from './Properties'
import Headline from './Headline'
import Summary from './Summary'
import Filters from '../partials/Filters'
import Search from '../partials/Search'

class Portfolio extends Component {
  constructor(props) {
    super(props)

    this.filters = []
    this.properties = []
    this.barons = false

    this.state = {
      properties: this.properties
    }

    this.sortList = this.sortList.bind(this)
    this.getToday = this.getToday.bind(this)
  }

  componentDidMount(){
    this.sortList()
  }
  
  sortList(event) {
    if( event ) {
    let target = event.target
    let filter = target.parentNode.getAttribute('data-filter')

    if( this.props.activeFilters ) {
      let index = this.props.activeFilters.indexOf(filter)

      if( index > -1 ) {
        this.props.activeFilters.splice(index,1)
      }else{
        this.props.activeFilters.push(filter)
      }
        
      let propertiesSorted = []

      this.properties.forEach(ev => {
        this.props.activeFilters.forEach(act => {
          if( ev.filters ) {
            if( ev.filters.indexOf(act) !== -1 ) {
              if( propertiesSorted.indexOf(ev) === -1 ) {
                propertiesSorted.push(ev)
              }
            }
          }
        })
      })

        if( propertiesSorted.length > 0 ){
          this.setState({
            properties: propertiesSorted
          })
        }else{
          console.error(`There were no sorted results returned for the filter "${filter}"`)
        }
      }else{
        console.error('There are no active filters!')
      }
    }else{
      this.setState({
        properties: this.properties
      })
    }
  }

  render(){
    return (
      <div className="container py-4">
        { this.props.headline &&
        <Headline key="schedule__headline" 
         headline={this.props.headline}
         copy={this.props.copy}/>
        }
        { this.props.summary &&
        <Summary key="schedule__summary" 
         headline={this.summary.headline}
         copy={this.props.summary.copy} 
         logo={this.props.summary.logo}
         artStyle={this.props.summary.artStyle}
         bgStyle={this.props.summary.background}
         figStyle={this.props.summary.figStyle}
         hStyle={this.props.summary.hStyle}
         imgStyle={this.summary.imgStyle}
         secStyle={this.props.props.summary.secStyle}/>
        }
        { this.props.has_search ?
        <div className="flex">
          <div className="pl-0 w-full md:w-1/4">
            <Search key="schedule__search" 
             filterText={this.props.filterText}
             target={this}
             use_barons={this.props.use_barons}/>
          </div>
          { this.props.filters &&
          <div className="w-full md:w-3/4 px-0 md:px-2">
            <Filters key="schedule__filters"
             sortList={this.sortList}
             filter={this.props.filters}
             activeFilters={this.props.activeFilters}/>
          </div>
          }
        </div>
        :
        <Filters key="schedule__filters"
         sortList={this.sortList}
         filter={this.props.filters}
         activeFilters={this.props.activeFilters}/>
        }
        <Properties key="schedule__events" 
         has_tags={this.props.has_tags}
         divStyle="flex"
         properties={this.state.properties}
         users={this.props.users}
         sessURL={this.props.sessURL}
         use_contact={this.props.use_contact}
         baseURL={this.props.baseURL}/>
      </div>
    )
  }
}

export default Portfolio