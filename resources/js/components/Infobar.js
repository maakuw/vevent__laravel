import React, {Component} from 'react'
import ReactMarkdown from 'react-markdown'

class Infobar extends Component {
  constructor(props) {
    super(props)

    this.divStyle = "flex flex-col flex-sm-flex content-start"
    this.cta = []
    this.visiting = []
  }

  render(){
    if(this.props) {
      if( this.props.divStyle ) this.divStyle = this.props.divStyle
      if( this.props.cta ) this.cta = this.props.cta
    }
    let copStyle = 'h2 my-sm-auto font-light uppercase text-left'
    this.divStyle = this.cta ? this.divStyle + ' w-full col-sm-5 lg:w-5/12' : ' w-full'
    
    if( this.props ) {
      return (
        <article data-infobar className="flex items-center content-between bg-tertiary">
          { ( this.headline || this.props.copy ) &&
          <div key="infobar__header" className={this.divStyle}>
          { this.props.headline ?
            <>
              <header key="infobar__header" className="ml-auto ms-md-0 mr-auto sm:w-1/2-0 lg:mr-3 pt-3 py-md-4 px-md-3 lg:py-4 text-center">
                <ReactMarkdown source={this.props.headline} escapeHtml={false} />
              </header>
              { this.props.copy ? 
              <p key="infobar__copy" className={copStyle}>{this.props.copy}</p>
              : null
              }
            </>
          :
            <p key="infobar__copy" className={`${copStyle} px-3 py-4`}>{this.props.copy}</p>
          }
          </div>
          }
          { this.props.cta &&
          <nav key="infobar__navigation" className="flex content-center w-full col-sm-3 md:w-1/3 col-lg-3 col-xl-2">
          { this.props.cta.map( ( anchor, a ) => {
            return (
              <a key={`anchor--${a}`} className={anchor.style ? anchor.style : this.cta.style} 
              href={anchor.href ? anchor.href : this.cta.href }>
                {anchor.label ? anchor.label : this.cta.label }
              </a>
            )
          })}
          </nav>
          }
          <div key="infobar__watching" className="w-full md:w-5/12 flex items-center content-end">
          { this.props.watching &&
          <> 
            <p className="mb-0 font-bold text-end">Currently<br/> Visiting</p>
              { this.props.watching.map( ( tenant, v ) => {                          
                let initials = tenant.first_name ? tenant.first_name.substr(0,1) : ''
                initials += tenant.last_name ? ' '+tenant.last_name.substr(0,1) : ''
                initials = initials === '' ? false : initials
                if(tenant.photo){
                  return (
                    <div key={`infobar__watching_initials--${v}`} className="coin bg-primary text-white m-2" data-backgrounder style={{backgroundImage:`url(${tenant.photo})`}}>
                      <span className="uppercase avatar invisible h-0 overflow-hidden">{initials}</span>
                    </div>
                  )
                }else{
                  return (
                    <div key={`infobar__watching_initials--${v}`} className="coin bg-primary text-white m-2">
                      <span className="uppercase avatar">{initials}</span>
                    </div>
                  )
                }
              }) }
              { this.props.watching.length > 5 &&
              <div className="flex items-center">
                <h6 className="text-4xl mb-0">+&nbsp;{this.props.watching.length - 5}</h6>
              </div>
              }
            </>
            }
          </div>
        </article>
      )
    }else{
      return null
    }
  }
}

export default Infobar