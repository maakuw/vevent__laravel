import React, {Component} from 'react'
import { NavLink } from 'react-router-dom'
import {randomID} from '../functions'

class Modal extends Component {

  constructor(props) {
    super(props);

    this.defaults = {
      headline: '',
      subheadline: '',
      copy: '',
      aStyle: 'block bg-poppy text-white bg-sm',
      nStyle: 'modal-footer content-center',
      mStyle: 'modal-dialog-centered',
      hStyle: 'h1 font-light uppercase text-secondary',
      feature: '//via.placeholder.com/800x550'
    }
    
    this.id = 'modal__'+randomID()
    this.headline = ''
    this.subheadline = ''
    this.copy = ''
    this.hide_directions = false
    this.feature = this.defaults.feature
    this.cta = false
    this.aStyle = this.defaults.aStyle
    this.nStyle = this.defaults.nStyle
    this.mStyle = this.defaults.mStyle
    this.hStyle = this.defaults.hStyle
  }

  render() {
    if(this.props) {
      if( this.props.id ) this.id = this.props.id
      if( this.props.headline ) this.headline = this.props.headline
      if( this.props.subheadline ) this.subheadline = this.props.subheadline
      if( this.props.copy ) this.copy = this.props.copy
      if( this.props.hide_directions ) this.hide_directions = this.props.hide_directions
      if( this.props.feature ) this.feature = this.props.feature
      if( this.props.cta ) this.cta = this.props.cta
      if( this.props.aStyle ) this.aStyle = this.props.aStyle
      if( this.props.nStyle ) this.nStyle = this.props.nStyle
      if( this.props.mStyle ) this.mStyle = this.props.mStyle
      if( this.props.hStyle ) this.hStyle = this.props.hStyle
    }

    return (
      <aside className="modal fade" id={`${this.id}__modal`} tabIndex="-1" role="dialog" aria-labelledby={`${this.id}__modal__label`} aria-hidden="true">
        <div className={`modal-dialog ${this.mStyle}`} role="document">
            <section className="modal-content p-4">
                <header className="modal-header">
                    <h5 className={`modal-title ${this.hStyle}`} id={`${this.id}__modal__label`}>{this.headline}</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </header>
                <div className="modal-body">
                    {this.copy}
                    { !this.hide_directions &&
                    <ol className="mt-3 lg:mt-4 text-battleship" style={{backgroundImage: `url(${this.feature})`}}>
                      <li className="h3 mb-2 mb-md-4 font-bold">
                        <div>
                          Hover over properties to learn more about areas &mdash; <span className="text-poppy">click to enter</span>
                        </div>
                      </li>
                      <li className="h3 mb-2 mb-md-4 font-bold">
                        <div>
                          Watch for the <span className="text-primary">crowds</span> to see what's hot!
                        </div>
                      </li>
                      <li className="h3 mb-2 mb-md-4 font-bold">
                        <div>
                          Keep your eyes peeled for hidden easter eggs!
                        </div>
                      </li>
                      <li className="h3 mb-0 font-bold">
                        <div>
                          Use the drop-down list to navigate between floors
                        </div>
                      </li>
                    </ol>
                    }
                </div>
                { this.cta &&
                <nav className={this.nStyle}>
                    <NavLink 
                    className={this.aStyle}
                    to={this.cta.href} 
                    onClick={this.props.closeModal}
                    >{this.cta.label}</NavLink>
                </nav>
                }
            </section>
        </div>
    </aside>
    )
  }
}

export default Modal