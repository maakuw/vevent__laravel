import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';

class Employee extends Component {

  constructor(props) {
    super(props);

    // TODO fixme -- mad dash to demo
    this.userId = localStorage.getItem("userID")

    this.style        = '';
    this.aStyle = 'flex items-center justify-center bg-poppy text-white text-xs text-white'
    this.navStyle = "flex content-center flex-col items-center text-center text-black"
    this.feature = false
    this.title = ''
    this.employee = false
    this.employeeSrc = false
    this.use_contact = false
    this.attributeship_id = 0

    this.state = {
      color: 'default'
    }

    this.palette = {
      'default': 'black',
      'Wc1F5PgsihOrPCR_ABmqi2rLrsBbojJO': 'black', 
      '8Y99Tb_5DvnkRecmqbfM_wVIJiDDiimP': 'blue', 
      'mB-6do231-Spm3_oZXXYoNOWSAeadncv': 'blue',
      'nmOrR4j-6NWaSdFvNyHusQ3Hex4kDDpt': 'blue', 
      'GROxOVSxvOIxAeDUhsX_Q0SpR5OcDqlF': 'blue',
      '8dO4xHHaJdVZJ5D4qi3GA3sNKvsZEnMp': 'danger', 
      'Zw4qo4CphHs-Or4oRPvrLspkR35kNdux': 'danger'
    }
  }

  componentDidMount(){
    if( this.props ) {
      this.setState({
        color: this.props.color
      })
    }
  }

  render() {
    console.log(this.props)
    if( this.props ) {
      if( this.props.title ) this.title = this.props.title
      if( this.props.aStyle ) this.aStyle = this.props.aStyle
      if( this.props.navStyle ) this.navStyle = this.props.navStyle
      if( this.props.feature ) this.feature = this.props.feature
      if( this.props.employeeSrc ) this.employeeSrc = this.props.employeeSrc
      if( this.props.employee ) this.employee = this.props.employee
      if( this.props.attributeship_id ) this.attributeship_id = this.props.attributeship_id
      if( this.props.use_contact ) this.use_contact = this.props.use_contact
    }

    this.content = [
      <header 
        key="employee" data-employee
        className={`flex content-stretch mb-4${this.attributeship_id ? ' ' + this.attributeship_id : ''}`}
        style={{
          backgroundSize: 'auto 100%, cover',
          backgroundAttachment: 'initial',
          backgroundPosition: 'center 80%, center center', 
          backgroundImage:`url(${this.feature}), url(/img/bg__attribute_property.jpg)`,
          backgroundRepeat:'no-repeat'
        }}>
            <figure key="employee--caption"
             className="flex w-full py-4 mb-0 mt-auto relative content-end items-center">
                <div key="employee--img" className="w-5/12 sm:w-1/3 md:w-1/4 feature">
                { this.employeeSrc ?
                    <img src={this.employeeSrc} width="100%" height="auto" alt="" className="avatar"/>
                :
                    <span className="uppercase avatar">{this.title ? this.title.substr(0,1) : ''}</span>
                }
                </div>
                <figcaption key="employee--figcaption" className="w-full sm:w-2/3 lg:w-3/4">
                  <div className="flex content-center sm:content-start items-center wrap">
                    <span className="w-full sm:w-2/37 md:w-5/12 lg:w-1/4 font-light mt-0 sm:mt-2 md:mt-auto mb-2 sm:mb-auto h2 text-white uppercase text-center sm:text-right md:text-left">{this.title}</span>
                    <nav key="employee--nav" className="w-5/12 md:w-1/4 xl:w-1/6 flex items-center content-center sm:content-start md:content-center mb-2 sm:mb-3 md:my-auto">
                      { this.use_contact &&
                        <NavLink className="flex items-center justify-between text-white bg-poppy text-white text-xs disabled text-center"
                        to='#contact'>Connect</NavLink>
                      }
                    </nav>
                  </div>
                </figcaption>
            </figure>
            { this.employee && 
            <nav data-employee_nav className="absolute flex items-stretch content-between w-full">
                <a className={this.navStyle} href={`/converse/${this.userId}`}>
                  <span className="block">Connect</span>
                  <svg className="icon">
                     <use xlinkHref="#icon__plus"/>
                  </svg>
                </a>
                <a className={this.navStyle} 
                 href={`mailto:${this.employee.email}?subject=Kenworth%20Right%20Choice%20|%20Networking%20Appointment%20Request&body=Hello!%0D%0A%0D%0AI’m%20interested%20in%20setting%20up%20an%20appointment%20to%20talk%20with%20you%20further.%0D%0A%0D%0APlease contact%20me%20at%20your%20earliest%20convenience.`}>
                   <span className="block">Schedule a Meeting</span>
                   <svg className="icon">
                     <use xlinkHref="#icon__plus"/>
                   </svg>
                </a>
            </nav>
            }
      </header>
    ];

    return this.content;
  }
}

export default Employee;