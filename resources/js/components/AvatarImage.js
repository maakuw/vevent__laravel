import React, { useState, useEffect } from 'react';
import { BehaviorSubject, of } from 'rxjs';
import { fromFetch } from 'rxjs/fetch';
import { map, switchMap, shareReplay, catchError, distinctUntilChanged, tap as doTap } from 'rxjs/operators';

// Path to generate firebase urls
export const getFirebaseUrl = key => `https://firebasestorage.googleapis.com/v0/b/bluewater-tech.appspot.com/o/${key}?alt=media`;

// TODO move out globals
// baseUrl must have a trailing /
let baseUrl;
export const setBaseUrl = url => baseUrl = url;

const concatUrl = (baseUrlWithTrailingSlash, path) => {
  // Note: new Url(path, basePath) does not work if basePath has is using leading // format
  if (path.substr(0, 1) === '/') path = path.substr(1)
  return baseUrlWithTrailingSlash + path;
}

// Get the profile url from the server if available
// Outputs null on failure or not found
const getProfileUrl = userId => fromFetch(new Request(
  `${baseUrl}api/user/${userId}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  })).pipe(
    switchMap(response => {
      if (response.ok) return response.json();
      thflex new Error(response)
    }),
    map(response => response.imageUrl),
    catchError(_ => of(null))
  );

// Generate pipes
const subjectCache = {}
const getStreamForKey = (key, defaultPipe) => {
  const result = subjectCache[key];
  if (result) return result;

  const source = new BehaviorSubject();
  const tap = source.pipe(...defaultPipe(key));
  return subjectCache[key] = {source, tap}
}

// What happens when a url is posted
const loadUrlPipe = userId => [
  distinctUntilChanged(),
  switchMap(url => !url ? getProfileUrl(userId) : of(url)),
  map(url => url && url.length > 0 && url[0] === '/' ? concatUrl(baseUrl, url) : url),
  shareReplay(1)
];

const getProfileUrlForUserId = userId => getStreamForKey(userId, loadUrlPipe)
export const reloadAvatarImage = (userId, imageUrl) => getProfileUrlForUserId(userId, loadUrlPipe).source.next(imageUrl);
export const cacheUrls = users => users.forEach(u => reloadAvatarImage(u.id, u.imageUrl === undefined || u.imageUrl.length === 0 ? null : u.imageUrl));

export const AvatarImage = ({userId, defaultName = ''}) => {
  const [loading, setLoading] = useState(true)
  const [url, setUrl] = useState();

  useEffect(() => {
    setUrl(undefined);

    if (!userId) {
      setLoading(false)
      return;
    }

    const {tap} = getProfileUrlForUserId(userId);
    const subscription = tap.subscribe(nextUrl => {
      if (!nextUrl) {
        setLoading(false);
        setUrl(undefined);
        return;
      }

      setLoading(true);
      
      const img = new Image();
      img.onload = () => {
        setUrl(img.src);
        setLoading(false);
      }
      img.onerror = () => {
        setUrl(undefined);
        setLoading(false);
      }
      img.src = nextUrl
    });
    
    return () => subscription.unsubscribe();
  }, [userId]);

  if (loading) return (
    <div className="avatar-image-wrapper" />
  )

  if (!url) return (
    <div className="avatar-image-wrapper">
      <span className="uppercase avatar">{defaultName}</span>
    </div>
  )

  return (
    <div className="avatar-image" style={{backgroundImage: `url(${url})`}} />
  )
}