import React, {Component} from 'react'
import Baron from '../partials/Baron'

class Barons extends Component {
  render() {
    return (
      <section data-barons className="flex content-between">
        <dl key="barons--dl" className="flex wrap mb-0 md:px-0 w-full">
          { this.props.barons.map( (baron, u) => {
            return (
              <Baron
                key={`baron--${u}`}
                id={baron.id} 
                first_name={baron.first_name}
                last_name={baron.last_name}
                bio={baron.bio} 
                url={baron.url} 
                photo={baron.photo}/>
            )
          }) }
        </dl>
      </section>
    )
  }
}

Barons.defaultProps = {
}

export default Barons