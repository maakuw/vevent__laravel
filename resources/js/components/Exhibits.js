import React, {Component} from 'react'
import Area from '../partials/Area'
import Attribute from '../partials/Attribute'

class Exhibits extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div data-properties className="py-4">
      { this.props.attributes &&
      this.props.attributes.map(attribute => {
        return (
         <Attribute key={`property_attribute--${attribute.id}`}
            id={attribute.id}
            parent_id={attribute.responsibility_id}
            filters={this.props.filters}
            activeFilters={this.props.activeFilters}
            name={attribute.label}
            ctaLabel={this.props.ctaLabel}/>
        )
      })
      }
      { this.props.areas &&
      this.props.areas.map(area => {
        return (
         <Area key={`property_area--${area.id}`}
            id={area.id}
            parent_id={area.property_id}
            filters={this.props.filters}
            activeFilters={this.props.activeFilters}
            name={area.label}
            ctaLabel={this.props.ctaLabel}/>
        )
      })
      }
    </div>
    )
  }
}

export default Exhibits