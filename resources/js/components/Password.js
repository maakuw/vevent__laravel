import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import ReactMarkdown from 'react-markdown'

class Password extends Component {

  constructor(props) {
    super(props)

    this.event = this.props.event ? this.props.event : ''

    this.forgot_headline = `Forgot your password?`
    this.forgot_copy = `We can help you recover your password, all we need is your email`
    this.confirm_headline = `Email sent!`
    this.confirm_copy = `We've sent you an email with a path to reset your password`

    this.state = {
      errors: this.props.errors,
      is_submitted: this.props.is_submitted,
      style: 'alert-warning'
    }

    if( this.props ) {
      if( this.props.errors ) this.errors = this.props.errors
      if( this.props.is_submitted ) this.is_submitted = this.props.is_submitted
      if( this.props.confirm_headline ) this.confirm_headline = this.props.confirm_headline
      if( this.props.confirm_copy ) this.confirm_copy = this.props.confirm_copy
      if( this.props.forgot_headline ) this.forgot_headline = this.props.forgot_headline
      if( this.props.forgot_copy ) this.forgot_copy = this.props.forgot_copy
    }

    this.formSubmit = this.formSubmit.bind(this)
    this.showSubmitted = this.showSubmitted.bind(this)
  }

  showSubmitted(){
    let ef = document.getElementById('emailForm')
    let sf = document.getElementById('submittedForm')

    if( sf && ef ) {
      sf.classList.remove('hidden')
      ef.classList.add('hidden')
    }
  }

  formSubmit(event) {
    event.preventDefault()

    const target = this
    const email = event.target.querySelector('input[name=email]')

    const xhr = new XMLHttpRequest()

    xhr.open('POST', `${this.props.baseURL}api/forgot-password`, true)
    xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest')
    xhr.setRequestHeader('content-type', 'application/json')

    let usr = {
      email: email.value
    }

    usr = JSON.stringify(usr)

    xhr.onload = function () {
      let allowed = this.status >= 200 && this.status < 400
    
      if( allowed ) {
          target.setState({
            is_submitted: true, 
            errors: `You have updated your password!`
          })
      } else {
          let msg = ''
          let err = ''
          switch(this.status) {
            case 400:
              msg = `We can't seem to find your email. Please use the Sign-Up tab to get started!`
              err = 'alert-poppy'
            break;
            case 403:
              msg = `We're sorry, but password doesn't seem to match your email. Please try again.`
              err = 'alert-warning'
            break;
            default:
              msg = <pre>{this.responseText}</pre>
              err = 'alert-poppy'
            break;
          }

          target.setState({
            style: err,
            is_submitted: false, 
            errors: msg
          })
      }
    }

    xhr.send(usr)
  }

  render() {
    let tab_style = "tab-content p-4 relative bg-white"
    let nav_style = "flex content-between items-center mt-4"
    let tab__h_style = "mt-4 font-bold text-center"

    if( this.state.is_submitted ) {
      this.response =   <legend className="small alert alert-success" role="alert">
                          {this.state.errors}
                        </legend>
    }else if( this.state.errors ) {
      this.response =  <legend className={`small alert ${this.state.style}`} role="alert">
                          {this.state.errors}
                        </legend>
    }
            
    return  <section data-component="signup" className="m-auto">
              <div className="container">
                <article className={tab_style}>
                  {this.props.has_server &&
                  <div key="emailForm" id="emailForm" className="tab-pane fade active show">
                    <h2 className={tab__h_style}>{this.forgot_headline}</h2>
                    { this.forgot_copy &&
                    <ReactMarkdown key="copy" source={this.forgot_copy} escapeHtml={false}/>
                    }
                    <form onSubmit={this.formSubmit}>
                      {this.response}
                      <div className="flex">
                        <div className="flex-col">
                          <label htmlFor="email">Email</label>
                          <input 
                            type="email" 
                            placeholder="Placeholder Text"
                            className="form-control" 
                            name="email" 
                            required 
                          />
                        </div>
                      </div>
                      <nav className={nav_style}>
                        <div className="w-full sm:w-2/3">
                          <Link
                            to="/login"
                            className="small text-battleship" 
                          ><u>Back to login</u></Link>                            
                        </div>
                        <div className="flex w-full sm:w-1/3 content-end">
                          <button 
                            type="submit" 
                            className="block bg-poppy text-white bg-sm sm:w-1/2"
                          >Submit</button>
                        </div>
                      </nav>
                    </form>
                  </div>
                  }
                  {this.props.has_server &&
                  <div key="submittedForm" id="submittedForm" className="tab-pane fade">
                    <h2 className={tab__h_style}>{this.confirm_headline}</h2>
                    { this.confirm_copy &&
                    <ReactMarkdown key="copy" source={this.confirm_copy} escapeHtml={false}/>
                    }
                      <nav className={nav_style}>
                        <div className="w-full sm:w-2/3 form-group">
                          <Link
                            to="/login"
                            className="small text-battleship" 
                          ><u>Back to login</u></Link>                            
                        </div>
                        <div className="flex w-full sm:w-1/3 form-group content-end">
                          <Link 
                            to="/forgot" 
                            className="block bg-poppy text-white bg-sm sm:w-1/2-4"
                          >Send Another Link</Link>
                        </div>
                      </nav>
                  </div>
                  }
                </article>
              </div>
            </section>
  }
}

export default Password