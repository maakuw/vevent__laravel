import React, {Component} from 'react'
import Contact from '../partials/Contact'

class Tenants extends Component {
  render() {
    return (
      <div data-contacts className="flex flex-col">
      { this.props.barons ?
        <dl className="flex flex-col">
        { this.props.barons.map( (baron,s) => {
          return (
          <Contact key={`contact--${baron.id}`}
           is_odd={s%2} 
           id={baron.id}
           photo={baron.photo} 
           feature={baron.feature} 
           first_name={baron.first_name} 
           last_name={baron.last_name} 
           rent={baron.rent} 
           use_rent={true}/>
        )
        }) }
        </dl>
      :
      <p>No Contacts</p>
      }
      </div>
    )
  }
}

Tenants.defaultProps = {
  barons: false
}

export default Tenants