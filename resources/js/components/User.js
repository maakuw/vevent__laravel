import React, {Component} from 'react'
import { NavLink } from 'react-router-dom'
import cms from '../cms.json'
import { getInitials, getRent, getSocial } from '../functions';

class User extends Component {

  constructor(props) {
    super(props);
    
    this.wStyle = "relative p-1/3 rounded-full inline-block"
    this.avStyle = "uppercase letter-spacing-none font-xl absolute white-space-none left-0 top-0 h-full w-full flex justify-center items-center"
  }

  render() {
    return (
      <header 
        key="user" data-user
        className={`flex content-stretch mb-4 bg-cover bg-center`}
        style={{
          backgroundAttachment: 'initial',
          backgroundImage:`url(${this.props.feature})`
        }}>
        <figure key="user--caption"
          className="flex w-full py-4 mb-0 mt-auto content-end items-center">
          <div key="user--img" className="w-5/12 sm:w-1/3 xl:w-1/4 feature">
          { this.props.photo ?
            <div className={this.wStyle} data-backgrounder style={{backgroundImage:`url(${this.props.photo})`}}>
              <span className={this.avStyle}>{ getInitials({
                first_name: this.props.first_name, 
                last_name: this.props.last_name
              }) }</span>
            </div>
          :
            <div className={this.wStyle}>
              <span className={this.avStyle}>{ getInitials({
                first_name: this.props.first_name, 
                last_name: this.props.last_name
              }) }</span>
            </div>
          }
          </div>
          <figcaption key="user--figcaption" className="w-full sm:w-2/3 xl:w-3/4">
            <div className="flex content-center content-sm-start items-center wrap">
              <span className={`name ${this.props.social ? 'w-full sm:w-7/12 md:w-5/12 lg:w-1/4 ' : ''}font-light mt-0 mb-2 mr-3 sm:mr-4 sm:my-auto text-xl text-white uppercase text-center sm:text-right md:text-left`}>{`${this.props.first_name} ${this.props.last_name}`}</span>
              <nav key="user--nav" className={`${this.props.social ? 'mb-2 sm:mb-3 w-full md:w-1/4 xl:w-1/6' : 'mb-0'} flex items-center content-center sm:content-start md:content-center md:my-auto`}>
                <NavLink className="block rounded-md text-white p-2 bg-poppy text-sm text-center" to={cms.tenant.slug}>Connect</NavLink>
              </nav>
              { this.props.social &&
              <nav key="user--social" data-social className="mb-2 sm:my-auto w-full md:w-1/3 col-lg-2 xl:w-1/4">
                <div className="flex wrap items-center content-center content-md-start">
                { this.props.social.map( network => getSocial(network) ) }
                </div>
              </nav>
              }
              { cms.use_rent &&
              <p className="w-full sm:w-1/2 md:w-5/12 lg:w-1/3 text-xl my-auto text-primary font-light text-center sm:text-left uppercase">
                Total Rent: <span className="ml-2 font-bold">{ getRent(this.props.rent)}</span>
              </p>
              }
            </div>
          </figcaption>
        </figure>
      </header>
    )
  }
}

User.defaultProps = {
  first_name: '', 
  feature: false, 
  photo: false, 
  social: false,
  rent: 0
}

export default User