import api from './api'
export const getPaths = () => {
  return window.location.pathname.split('/').map(path => path)
}

export const getRent = (rent) => {
    if(rent > 0) rent = String(rent)
    if(rent.length > 3 ){
      rent = rent.substr(0, rent.length - 3) + ',' + rent.substr(rent.length - 3)
    }
}


export const getSocial = (network) => {
  if(network.href) {
    return (
      <a href={`${network.prefix}/${network.href}`} 
        target="_blank" rel="noopener noreferrer" 
        className="text-xl w-full md:w-1/4 lg:w-1/2 xl:w-1/4 my-auto text-center text-white px-1"
        key={`social_link--${network.slug}`}>
        <svg className="icon">
          <use xlinkHref={`#icon__${network.slug}`}/>
        </svg>
      </a>
    )
  }else{
    return null
  }
}

export const getInitials = (contact) => {
  let initials = contact.first_name ? contact.first_name.substr(0,1) : ''
  initials += contact.last_name ? ' ' + contact.last_name.substr(0,1) : ''
  initials = initials === '' ? false : initials
  contact.initials = initials
  return initials
}

export const getNow = () => {
  let today = new Date()
  let hrs = today.getHours()
  let min = today.getMinutes()

  return hrs + ':' + min
}

export const getToday = () => {
  let today = new Date()
  let dd = today.getDate()
  let mm = today.getMonth()+1
  let yyyy = today.getFullYear()

  if( dd < 10 ) {
      dd = '0' + dd
  } 

  if( mm < 10 ) {
      mm = '0' + mm
  } 

  return yyyy + '-' + mm + '-' + dd
}

//Spit out the correctly styled numbers for the Categories component
export const parseCount = (num) => {
  return num ? ( num < 10 ? `0${num}` : num ) : false
}

//Change the class on the Body tag so that it matches the current page(s),
//also added classes for "animating" (when in motion) and "loaded" on first paint
//@style: <String> any css class you'd like to ensure gets added
export const updateBodyStyle = (style='') => {
  let body = document.querySelector('body')
  let location = style ? style : ''
  let has_loaded = body.classList.contains('loaded') ? false : true

  //let paths = window.location.pathname.split('/')
  getPaths().forEach(path => {
    if( path !== '' ) {
      if( location.search(path) < 0 ) location += ' ' + path
    }
  })

  //If we've already loaded, don't stagger animate things in
  if(has_loaded) {
    setTimeout(()=>{
      if( location.search('loaded') < 0 ) location = `${location ? `${location} loaded` : 'loaded'}`
      body.className = location
    },500)
  }else{
    setTimeout(()=>{
      if( location.search('animating') >= 0 ) body.classList.remove('animating')
    },5)
    //First, set loaded and location
    if( location.search('loaded') < 0 ) location = `${location ? `${location} loaded animating` : 'loaded animating'}`
  }
  body.className = location
}

export const showSidebar = (event) => {
  event.preventDefault()

  const target = document.querySelector('[data-sidebar]')
  if( !target ) {
    console.error('Oh no!! Theres no sidebar in the Sidebar.show()', document.querySelector('[data-sidebar]'))
    return
  }
  target.classList.add('active')
  target.classList.add('show')
}

export const hideSidebar = (event) => {
  const target = document.querySelector('[data-sidebar]')
  if( !target ) {
    console.error('Oh no!! Theres no sidebar in the Sidebar.hide()', document.querySelector('[data-sidebar]'))
    return
  }
  target.classList.remove('active')
  target.classList.remove('show')
}

//Trigger any DOM element using the counter dataset, and a child element containing the ring dataset
export const initCounters = () => {  
  let counters = document.querySelectorAll('[data-counter]')

  if( counters ) {
    counters.forEach(counter => {
      let rings = counter.querySelectorAll('[data-ring]')

      if( rings ) {
        rings.forEach( (ring, r) => {
          let is_first = ( rings[0] === ring )
          let span = ring.querySelector('span')
          let num = counter.dataset.counter
          let pcnt
          let a = 0
          
          if( is_first ) {
            pcnt = Math.round(360*(num*0.01))
            pcnt = pcnt > 180 ? 180 : pcnt

            let animateIt = setInterval( function(){
              if( a <= pcnt) {
                span.style.transform = `rotate(${a}deg)`
                a += 10
              }else{
                a = 0
                clearInterval(animateIt)
              }
            }, 1 )
          }else{
            if( num > 50 ) {
              pcnt = Math.round(360*((num - 50)*0.01))

              let continueIt = setInterval( function(){
                if( a <= pcnt) {
                  span.style.transform = `rotate(${a}deg)`
                  a += 10
                }else{
                  a = 0
                  clearInterval(continueIt)
                }
              }, 1 )
            }
          }
        })
      }
    })
  }
}

//Create a numerical suffix that is (mostly) random, once combinded with other string data
export const randomID = (length=8) => {
  // Declare all characters
  let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  // Pick characers randomly
  let str = '';
  for (let i = 0; i < length; i++) {
      str += chars.charAt(Math.floor(Math.random() * chars.length));
  }

  return str
}

//Take a standard string of numbers and format it to look like a phone number
export const formatPhone = (phone) => {
  return `(${phone.substr(0,3)}) ${phone.substr(3,3)}-${phone.substr(6)}`
}

//Force the window contents to hard jump tot he top
export const jumpToTop = () => {
  setTimeout(window.scrollTo(0, 0), 300)
}

//Read the location hash (if there is one) and trigger that tab
export const getHash = () => {
  let hash = window.location.hash
  if(hash) {
    let anchor = document.querySelector(`a[href="${hash}"]`)
    if( anchor ) {
      anchor.click()
    }
  }else{
    return false
  }
}

export const setHash = (event) => {
  let hash = event.target.dataset.href
  window.location = hash
}

//Convert any DOM object containing an anchor into a clickable element
export const clickthrough = (event) => {
  if(!event) return false
  event.preventDefault()
    
  const target = event.target.querySelector('a:first-of-type')
  if(target) {
    if(!target.classList.contains('disabled')) target.click()
  }
}

//Trigger the closing of a Bootstrap modal
export const closeModal = (callback) => {
  let closer = document.querySelector('[data-dismiss="modal"]')
  if(closer) closer.click()
  if(callback) callback()
}

export const getCookie = (cname) => {
  let name = cname + "="
  let decodedCookie = decodeURIComponent(document.cookie)
  let ca = decodedCookie.split(';')
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) == ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ""
}

export const getLogo = (callback) => {
  api.get('/assets/logos/1')
  .then( response => {
    callback && callback(response.data)
  })
  .finally( () => {
    return true
  })
  .catch( error => {
    return `Settings error occurred: ${error}`
  })
}

export const toggleSidenav = (event) => {
  if(event) event.preventDefault()

  let sidebar = document.querySelector('[data-sidebar]')
  sidebar.classList.toggle('-ml-14')
}

export const toggleSubnav = (event) => {
  if(event) event.preventDefault()

  let subnav = document.querySelector('[data-subnav]')
  subnav.classList.toggle('h-auto','p-4', 'h-0')
}

export const getSettings = (callback) => {
  axios.get('/resource/settings')
  .then( response => {
    callback && callback(response.data)
  })
  .finally( () => {
    return true
  })
  .catch(error => {
    return error.response ? error.response.statusText : error
  })
}

export const toggleRegistration = (event) => {
  if(event){
    let is_hash = event.target.href.search('#')
    //console.log(is_hash)

    if(is_hash < 0){
      event.preventDefault()
      history.pushState( "", document.title, window.location.pathname + window.location.search)
    }
  }

  let tabs = document.querySelectorAll('[data-slidetab]')
  tabs.forEach(tab => {
    tab.classList.toggle('hidden')
  })
}

export const selectHouse = (event) => {
//  console.log(event)
  if(event){
    let is_hash = event.target.href.search('#')
//    console.log(is_hash)

    if(is_hash < 0){
//      event.preventDefault()
      history.pushState( "", document.title, window.location.pathname + window.location.search)
    }
  }

  let tabs = document.querySelectorAll('[data-houses]')
  tabs.forEach(tab => {
    tab.classList.remove('active')
  })

  let target = event.target.id
  if(target){
    //console.log(target)
    let tab = document.getElementById('tab--'+target)
    if(tab) tab.classList.add('active')
  }
}

export const getUsers = (callback) => {
  axios.get('/resource/users')
  .then( response => {
    callback && callback(response.data)
  })
  .finally( () => {
    return true
  })
  .catch(error => {
    return error.data
  })
}

export const parseTitle = (title) => {
  if( title ) {
    let pt1  = title.substr(0, title.search('<sup>'))
    if( pt1 ){
        let pt2  = title.substr(title.search('</sup>') + 6)
        return <span>{pt1}<sup>&reg;</sup>{pt2}</span>
    }else{
      return title
    }
  }else{ 
    return false
  }
}

export const searchSubmit = (target, str='', user_barons=true, use_information=false) => {
  if(str) {
    console.log(str, 'changes to')
    let state = {}
    let query = str
    if(user_barons) {
      query = str.trim().toLowerCase()
      state.barons = target.state.barons.filter(r => 
        `${r.first_name.toLowerCase()} ${r.last_name.toLowerCase()}`.indexOf(query) >= 0
      )
    }
    if(use_information) {
      let words
      if(target.state.title){
        query = str.toLowerCase()
        words = target.state.title.replace(/<mark>/g,'').replace(/<\/mark>/g,'').split(' ')
        let title = words.map(w => {
          if(w.toLowerCase().indexOf(query) !== -1) {
            return `<mark>${w}</mark>`
          }else{
            return w
          }
        } )
        state.title = title.join(' ')
      }
/*
      if(target.state.bio){
        query = str.toLowerCase()
        words = target.state.bio.toLowerCase().split(' ')
        state.bio = words.filter(w => w.indexOf(query) >= 0 ? w = <mark>{w}</mark> : w )
        state.bio = state.bio.join(' ')
      }*/
    }
    target.setState(state)
  }
  console.log('searchSubmit', str)
}

//After a specified time, close the warning/error alert aside
export const timedAlert = (callback = false, time = 3600) => {
  var alerts = document.querySelector('[data-error]')

  function animateOut(callback) { 
    callback && callback()
    alerts.classList.remove('show')
  }

  if(alerts){
    //Reset the animator and the alert aside
    alerts.classList.add('show')

    if(callback){
      setTimeout(() => animateOut(callback), time)
    }else{
      setTimeout(() => animateOut(), time)
    }
  }else{
    return {
      feedback: {
        msg: 'timedAlert called, but no alerts are avialable to close', 
        style: 'danger'
      }
    }
  }
}

//Order an array of objects by a specific key
export const dynamicSort = (property) => {
  let sortOrder = 1

  if(property[0] === "-") {
    sortOrder = -1
    property = property.substr(1)
  }

  return function (a,b) {
    if(sortOrder === -1){
        return b[property].localeCompare(a[property])
    }else{
        return a[property].localeCompare(b[property])
    }        
  }
}

//Convert date formats to something readable
export const standardizeDate = (date, use_time=false) => {
  let months = [`Jan`,`Feb`,`Mar`,`Apr`,`May`,`Jun`,`Jul`,`Aug`,`Sep`,`Oct`,`Nov`,`Dec`]
  let formatted = new Date(date)
  let month = formatted.getMonth()
  let day = formatted.getDate()
  let suffix = formatted.getHours() > 12 ? 'PM' : 'AM'
  let hour = formatted.getHours() > 12 ? formatted.getHours() - 12 : formatted.getHours()
  let year = formatted.getFullYear()
  let time = use_time ? ` ${hour}:${formatted.getMinutes()}${suffix}` : ''
  let fulldate = use_time ? `${month}/${day}/${year}${time}` : `${months[month]}. ${day}, ${year}`
  return fulldate
}

//Convert date formats to something readable
export const standardizeTime = (time) => {
  let hour = parseFloat(time.substr(0,2))
  let suffix = hour > 12 ? 'PM' : 'AM'
  hour = hour > 12 ? hour - 12 : hour
  hour = hour === 0 ? 12 : hour
  return ` ${hour}:${time.substr(3,2)}${suffix}`
}

//Sort a set of alphanumeric strings
export const sortAlpha = (a, b) => {
  if (a === undefined || a === null) {
      return -1
  }
  if (b === undefined || b === null) {
      return 1
  }
  return a.localeCompare(b)
}

//Sort a set of numbers
export const sortNums = (a, b) => {
  if (a === null || b === null) {
      return 1
  }
  if (a > b ) { 
      return 1
  } else if (b > a ) {
      return -1
  } else {
      return 0
  }
}

  //Sort a set of dates
export const sortDate = (a, b) => {
  return a - b
}

//Sort a set of boolean vales
export const sortBool = (a, b) => {
  if (a === b) {
      return 0
  } else if (a === true) {
      return -1
  } else {
      return 1
  }
}

export const checkEqual = (a, b) => {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length !== b.length) return false;
  
    // If you don't care about the order of the elements inside
    // the array, you should sort both arrays here.
    // Please note that calling sort on an array will modify that array.
    // you might want to clone your array first.
  
    for (var i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) return false;
    }
    return true;
}