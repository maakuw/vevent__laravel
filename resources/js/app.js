/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point.
*/

import React from 'react';
import ReactDOM from 'react-dom';

/**
* After that, we pull in everything and cast it as a serviceWorker
* then include the JS component itself
*/
import * as serviceWorker from './serviceWorker';

/**
 * Then we load up our pages so we can use them based on where
 * we are in.
*/

import Main from './global/Main'
import Login from './global/Login'
import Register from './global/Register'

/**
 * Lastly, check to ensure all this dated code Bootstrap uses is
 * intact
*/
try {
   window.Popper = require('popper.js').default;
   window.$ = window.jQuery = require('jquery');
 
   require('bootstrap');
} catch (e) {
   console.error(e)
}
 
 let main = document.querySelector('[data-main]')
 let login = document.querySelector('[data-login]')
 let register = document.querySelector('[data-register]')
 
 if( login ) {
  ReactDOM.render(<Login/>, login)
  ReactDOM.render(<Register/>, register)
 } else if( main ) {
  ReactDOM.render(<Main />, main)
 }
 
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister(); 