import React from 'react'
import useChat from "../hooks/useChat"

const Room = (props) => {
  const roomId = props ? props.roomId : false // Gets roomId from URL
  const logo = props ? props.logo : false
  const phone = props ? props.phone : false
  const use_full = props ? props.use_full : false
  const { messages, sendMessage } = useChat(roomId) // Creates a websocket and manages messaging
  const [newMessage, setNewMessage] = React.useState("") // Message to be sent

  const handleNewMessageChange = (event) => {
    setNewMessage(event.target.value);
  }

  const handleKeyPress = (event) => {
    if(event.key === 'Enter' && newMessage.length){
        handleSendMessage()
    }
  }

  const handleSendMessage = () => {
    if(newMessage.length){
        sendMessage(newMessage)
        setNewMessage("")
    }
  }
  
  var content
  if(  use_full ) {
    content = <section data-chat-target className="p-4 bg-tertiary chat-room-container">
                <header className="flex px-4 content-between">
                    <div className="py-2 px-4 w-2/3 md:w-5/6 flex items-center">
                        <div className="flex">
                            { logo &&
                            <img className="avatar" alt={logo.alt} src={logo.src} />
                            }
                            <h2 className="mb-0 room-name">{roomId}</h2>
                        </div>
                    </div>
                    <div className="py-2 px-4 w-1/3 md:w-1/6">
                        <nav className="flex content-end">
                            <a href={`tel:+1${phone}`} className="flex justify-center items-center text-xs bg-white rounded-circle p-2 mr-3 text-battleship">
                                <svg className="icon">
                                    <use xlinkHref="#icon__phone"/>
                                </svg>
                            </a>
                            <a href="#contact" className="flex justify-center items-center text-xs bg-white rounded-circle p-2 text-battleship">
                                <svg className="icon">
                                    <use xlinkHref="#icon__contacts"/>
                                </svg>
                            </a>
                        </nav>
                    </div>
                </header>
                <footer className="flex p-3 p-lg-4 relative">
                    <div className="messages-container w-full h-full">
                        <dl className="messages-list relative flex flex-col mb-0">
                        {messages.map((message, i) => (
                            [ <dt 
                                key={`message-title${i}`}
                                className="avatar-wrapper"></dt>,
                            <dd
                            key={`message-body${i}`}
                            className={`px-4 py-2 mb-4 text-sm text-light text-white message-item inline-flex w-auto ${
                                message.ownedByCurrentUser ? "my-message mr-auto ms-0" : "received-message ms-auto mr-0"
                            }`}
                            > 
                            {message.body}
                            </dd> ]
                        ))}
                        </dl>
                    </div>
                    <input
                        value={newMessage}
                        onChange={handleNewMessageChange}
                        onKeyPress={handleKeyPress}
                        placeholder="Write message..."
                        className="border-none text-white form-control"
                    />
                    <button onClick={handleSendMessage} className="send-message-button flex justify-center items-center text-xs">
                       <svg className="icon"><use xlinkHref="#icon__paper_airplane"/></svg>
                    </button>
                </footer>
            </section>
  }else{
    content = <div className="flex">
                <div className="messages-container w-full">
                    <div className="messages-list">
                    {messages.map((message, i) => (
                        <p
                        key={`message-body${i}`}
                        className={`mb-2 text-sm message-item ${
                            message.ownedByCurrentUser ? "my-message" : "received-message"
                        }`}
                        >From <span className="text-primary">{message.ownedByCurrentUser ? `me` : 'Dudes name'}</span> to <span className="text-primary">Everyone</span>: {message.body}</p>
                    ))}
                    </div>
                </div>
                <input
                    value={newMessage}
                    onChange={handleNewMessageChange}
                    onKeyPress={handleKeyPress}
                    placeholder="Type your message here..."
                    className="border-none form-control"
                />
            </div>
  }
  return content
}

export default Room