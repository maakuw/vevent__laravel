import React, {Component} from 'react'

class Tag extends Component {
  render() {
    return <span className={this.props.sClass}>{this.props.label}</span>
  }
}

Tag.defaultProps = {
  label: 'Tag',
  sClass: "tag badge mt-2"
}

export default Tag