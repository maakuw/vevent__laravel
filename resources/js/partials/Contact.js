import React, {Component} from 'react'
import {NavLink} from 'react-router-dom'
import ReactMarkdown from 'react-markdown'
import {clickthrough, getInitials, getPaths, getRent, randomID} from '../functions'

class Contact extends Component {
  render() {
    let initials = getInitials({
      first_name: this.props.first_name, 
      last_name: this.props.last_name
    })
    return (
      <>
        <dt className="invisible h-0 overflow-hidden contact-name" key={`contact__name--${this.props.id}`}>
          {this.props.first_name+' '+this.props.last_name}
        </dt>
        <dd data-contact className="contact w-full" key={`contact--${this.props.id}`}>
          <div className="w-full flex wrap" data-clickthrough onClick={clickthrough}>
            <figure className="flex content-start items-center pb-0 pt-3 pr-3 pl-3 lg:pt-4 lg:pr-4 md:pb-4 m-0 w-full md:w-1/2">
              { this.props.photo ?
                <div className="mr-3" data-backgrounder style={{backgroundImage:`url(${this.props.photo})`}}>
                  <span className="invisible h-0 overflow-hidden">{initials}</span>
                </div>
              :
                <div className="mr-3">
                  <span className="uppercase avatar">{initials}</span>
                </div>
              }
              <figcaption className="flex flex-col items-start content-center lg:content-between mr-auto ml-4 lg:mx-4">
                <h3 className="flex flex-col text-2xl font-bold">{`${this.props.first_name}${this.props.last_name ? ' '+this.props.last_name.substr(0,1):''}`}</h3>
                <p className="text-md mb-2 mb-md-4 lg:mb-auto lg:pb-4 font-light">{this.props.title}</p>
                <NavLink className="bg-poppy flex justify-center items-center p-2 rounded-md text-xs text-white" to={`/${getPaths()[1]}/${this.props.id}`}>View Profile</NavLink>
              </figcaption>
            </figure>
            { ( this.props.bio || this.props.use_rent ) &&
            <div className="description w-full md:w-1/2 px-4 lg:px-3 pt-2 pt-sm-auto pb-4 lg:pb-3 lg:py-4 lg:px-2 mx-0 mb-0 mt-3">
              { this.props.bio &&
              <ReactMarkdown source={this.props.bio} escapeHtml={true} />
              }
              <div className={`${this.props.is_odd ? 'text-white' : 'text-tertiary'} flex items-end flex-col`}>
                <div className="flex text-2xl mb-0">
                  <svg className="icon" style={{
                    width: '2rem',
                    height: '2rem',
                    transform: 'scale(1.25)'
                  }}>
                    <use xlinkHref="#icon__users"/>
                  </svg>
                  <span className="text-3xl text-primary flex flex-col self-end mr-2">{ getRent(this.props.rent) }</span>
                </div>
                <div className="block text-md text-primary">Total Points</div>
              </div>
            </div>
            }
          </div>
        </dd>
      </>
    )
  }
}

Contact.defaultProps = {
  first_name: '', 
  last_name: '', 
  title: '',  
  feature: false, 
  photo: false, 
  rent: 0,
  use_rent: true,
  id: randomID()
}

export default Contact;