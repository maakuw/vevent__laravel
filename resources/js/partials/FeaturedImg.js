import React, {Component} from 'react'

class FeaturedImg extends Component {
  render() {
    return (
      <picture className={this.props.divStyle}>
        <img alt={this.props.alt}
         src={this.props.src} 
         srcSet={this.props.srcSet}    
         className={this.props.imgStyle}
         data-src-medium={this.props.srcMd}
         data-src-large={this.props.srcLg}
         data-src-xlarge={this.props.srcXl}/>
      </picture>
    )
  }
}

FeaturedImg.defaultProps = {
    src: '',
    alt: '',
    srcSet: '',
    srcMd: '',
    srcLg: '',
    srcXl: '',
    divStyle: "invisible w-0 h-0",
    imgStyle: ''
}

export default FeaturedImg