import React, {Component} from 'react'

class Feedback extends Component {
  render(){
    return (
      <aside data-error className="fixed top-0 left-0 flex content-between w-full m-0 p-0 sticky-top">
      {this.props.feedback.msg &&
        <p className="my-auto p-2">
          <small>
            {this.props.feedback.msg}
          </small>
        </p>
        }
      </aside>
    )
  }
}

Feedback.defaultProps = {
  feedback: {
    msg: false,
    style: 'danger'
  }
}

export default Feedback