import React, {Component} from 'react'
import {NavLink} from 'react-router-dom'
import {clickthrough} from '../functions'

class Attribute extends Component {
  render(){   
    return (
      <article data-property data-clickthrough onClick={clickthrough}
        className={this.props.filters ? this.props.filters.join(' ') : ''}>
        <span key="property__colorbar" data-colorbar className="flex flex-col content-stretch items-stretch">
        { this.props.filters &&
          this.props.filters.map(filter=>{
            return <span key={`property__colorbar--${filter}`} className={`flex w-full ${filter}`}/>
          })
        }
        </span>
        <div key="property__wrapper" className="flex px-3 py-4 mb-4">
          <h2 className="text-3xl font-bold mr-4 my-auto">{ this.props.name }</h2>
          <NavLink className="block bg-poppy text-white text-xs rounded-sm p-1" to={`/properties/attribute/${this.props.parent_id}`}>View</NavLink>
        </div>
      </article>
    )
  }
}

Attribute.defaultProps = {
  name: '',
  parent_id: 0,
  filters: false,
  activeFilters: []
}

export default Attribute