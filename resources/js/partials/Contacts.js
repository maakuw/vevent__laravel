import React, {Component} from 'react'
import Card from '../components/Card'

class Contacts extends Component {
  render() {
    return (
      <article id="anchor--profile__information--score_board" data-panel className={this.props.panStyle}>
        <div className="px-4">
          <h2 className="h6 font-bold text-primary uppercase">Conference Scoreboard</h2>
          <dl className="pt-3 mb-0 flex content-start">
          { this.props.contacts &&
          <>
            <dt className="invisible h-0 overflow-hidden">Contacts Made</dt>
            <Card key="card__connnections_made"
            icon="#icon__contacts_made" 
            ddStyle="card w-1/3 mx-2" 
            count={this.props.contacts.length}
            title="Contacts"
            capOffset="translate(5px,3px)"
            copy="Keep track of the contacts you've made "/>
          </>
          }
          { this.props.badges &&
          <>
            <dt className="invisible h-0 overflow-hidden">Badges Earned</dt>
            <Card key="card__badges_earned"
            icon="#icon__badges_earned"
            ddStyle="card w-1/3 mx-2" 
            count={this.props.badges.length}
            capOffset="translateY(8px)"
            title="Badges"
            copy="Unlock badges with your event participation. Work towards specific challenges, and earn hidden bonuses along the way."/>
          </>
          }
          { this.props.games &&
          <>
            <dt className="invisible h-0 overflow-hidden">Games Played</dt>
            <Card key="card__games_played"
            icon="#icon__games_played"
            ddStyle="card w-1/3 mx-2" 
            count={this.props.games.length}
            title="Games Played"
            copy="Game sessions count towards your event score, while serving as virtual team-building + networking opportunities."/>
          </>
          }
          </dl>
        </div>
      </article>
    )
  }
}

Contacts.defaultProps = {
  panStyle: '', 
  badges: false,
  contacts: false, 
  games: false,
}

export default Contacts