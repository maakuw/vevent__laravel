import React, {Component} from 'react'
import {NavLink} from 'react-router-dom'
import FeaturedImg from './FeaturedImg'
import {clickthrough, getInitials, randomID} from '../functions'

class Baron extends Component {
  render() {
    return (
      <>
        <dt key={`baron_title--${this.props.id}`} className={`baron-name ${this.props.dtStyle}`}>{this.props.contact.first_name} {this.props.contact.last_name}</dt>,
        <dd key={`baron_desc--${this.props.id}`} data-baron className={`baron ${this.props.ddStyle}`}>
          <div className="w-full flex">
            <figure className={this.props.figStyle} onClick={clickthrough} data-clickthrough>
            { this.props.photo ?
              <div data-backgrounder style={{backgroundImage:`url(${this.props.photo})`}}>
                <FeaturedImg key={`baron--featured_img${this.props.id}`} src={this.props.photo}/>
              </div>
            :
              <div className="avatar-wrapper">
                <span className="uppercase avatar">{getInitials(this.props.contact)}</span>
              </div>
            }
              <figcaption className={`content-between flex ${this.props.capStyle}`}>
                <h3 className={this.props.h3Style}>
                  <span className={`${this.props.namStyle} mb-0`}>{this.props.contact.first_name} {this.props.contact.last_name}</span>
                </h3>
                <NavLink className={`bg-primary ${this.props.btnStyle}`} to={`/contact/${this.props.id}`}>+&nbsp;Add</NavLink>
              </figcaption>
            </figure>
          </div>
        </dd>
      </>
    )
  }
}

Baron.defaultProps = {
  contact: {
    first_name : '',
    last_name : ''
  }, 
  bio : [],
  url : '', 
  btnStyle : "flex items-center justify-between p-2 text-white rounded-md", 
  capStyle : "flex-col items-start ml-4 mr-auto", 
  dtStyle : "visible",
  ddStyle : "w-full md:w-1/2 mb-3 md:mb-0",
  ddStyleF : "featured w-full mb-3",
  figStyle : "px-2 py-3 lg:p-4 m-0 flex content-start items-center",
  h3Style : "flex flex-col mb-auto", 
  namStyle : "font-bold",
  img : false,
  id: randomID()
}

export default Baron