import React, {Component} from 'react'
import {searchSubmit} from '../functions'

class Search extends Component {
  constructor(props) {
    super(props)

    this.state = {
      error: []
    }
    
    //Methods
    this.fieldChange = this.fieldChange.bind(this)
  }

  fieldChange(event) {
    event.preventDefault()
    searchSubmit( this.props.target, event.target.value,  this.props.use_barons, this.props.use_information )
  }
    
  render() {
    return (
      <form data-search onSubmit={(e) => e.preventDefault()}>
        { this.state.errors ? this.state.errors : '' }
        <label className="flex items-center relative border rounded-md border-gray text-slate-400 overflow-hidden">
          { this.props.icon &&
          <svg className="pointer-events-none absolute left-3 top-3 icon">
            <use xlinkHref={this.props.icon}/>
          </svg>
          }
          <input type="text" 
            className="pl-8 p-2 text-sm mb-0 font-light w-full"
            placeholder={this.props.searchLabel} 
            onChange={this.fieldChange} />
        </label>
      </form>
    )
  }
}

Search.defaultProps = {
  searchLabel: 'Search', 
  icon: '#icon__search'
}

export default Search