import React, {Component} from 'react'

class Counter extends Component {
  render() {
    return (
      <div data-counter={this.props.number} className="text-4xl w-full md:w-1/2 mb-0 text-center content-center flex flex-col">
        <span data-ring>
          <span/>
        </span>
        <span data-ring>
          <span/>
        </span>
        <span data-count>
          { this.props.number }
          { !this.props.hide_percent && 
            <span className="suffix">%</span>
          }
        </span>
      </div>
    )
  }
}

Counter.defaultProps = {
  number: false,
  hide_percent: false
}

export default Counter