import React, {Component} from 'react'
import { randomID } from '../functions'

class Filters extends Component {
  constructor(props) {
    super(props)

    this.sortList = this.sortList.bind(this)
  }

  sortList(e) {
    e.persist()
    let target = this
    setTimeout( function(){
      target.props.sortList(e)
    }, 300 )
  }
    
  render() {
    if(this.props.filters){
      return (
        <form data-filters className="flex px-3 px-md-0">
          <div className="wrap flex content-start items-center wrap">
          { this.props.filters.map((filter,f) => {
          return (
            <div data-filter className="py-4 lg:py-2 px-0" key={`switches-wrapper--${f}`}>
              <label className={`switch mb-0 ${filter.slug}`} data-filter={filter.slug}>
                <input type="checkbox" defaultChecked={ 
                  this.props.activeFilters ? ( this.props.activeFilters.indexOf(filter.slug) > -1 ? true : false ) : false
                }
                  className="rounded-pill" onChange={this.sortList}/>
                <span className="text-md uppercase label pl-2 pr-6">{filter.label}</span>
              </label>
            </div>
          )
          }) }
          </div>
        </form>
      )
    }else{
      return null
    }
  }
}

Filters.defaultProps = {
  filters: false,
  activeFilters: false
}
export default Filters