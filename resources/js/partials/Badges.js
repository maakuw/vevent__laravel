import React, {Component} from 'react'
import { NavLink } from 'react-router-dom'

class Badges extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount(){
    if(this.props.panStyle) this.panStyle = this.props.panStyle
  }

  render() {
    return (
      <article data-panel className={this.props.panStyle}>
        <div className="px-4">
          <h2 className="text-xs font-bold text-primary uppercase">Earned Badges</h2>
          <div className="flex flex-row">
            <div className="w-full md:w-3/4 md:pl-0">
              <picture className={`flex content-center ${this.props.badges.length < 5 ? 'lg:content-start' : 'lg:content-between'}`}>
              { this.props.badges.map((badge,b) => {
                if(b < 5){
                  return this.props.makeBadge(<text transform="translate(34.432 39.262)"
                  className={badge.color} fontSize="7" fontFamily="Poppins-Medium, Poppins" fontWeight="500" letterSpacing="0.042em">
                      <tspan className="uppercase" x={badge.coor_a1} y={badge.coor_a2}>{badge.title}</tspan>
                      <tspan className="uppercase" fontSize="12" x={badge.coor_b1} y={badge.coor_b2}>{badge.subtitle}</tspan>
                  </text>,
                  <path d="M9.757,2A7.765,7.765,0,1,0,17.53,9.765,7.761,7.761,0,0,0,9.757,2Zm3.292,12.424-3.285-1.98L6.48,14.424l.87-3.735-2.9-2.508,3.82-.326L9.765,4.329l1.491,3.518,3.82.326-2.9,2.508Z"
                  className={badge.color} transform="translate(25.436 8.353)"></path>, badge.color)
                }else{
                  return null
                }
              }) }
              </picture>
            </div>
          { this.props.badges &&
          <nav className="flex w-full md:w-1/4 lg:mr-0
          content-center items-center">
            { this.props.badges.length > 5 &&
            <NavLink className="flex flex-col items-center justify-center p-2 text-xs text-white bg-battleship lg:ml-4" to="/profile/badges">
              More&nbsp;+
            </NavLink>
            }
          </nav>
          }
        </div>
      </div>
      </article>
    )
  }
}

Badges.defaultProps = {
  panStyle: '',
  badges: false
}

export default Badges