import React, {Component} from 'react'
import ReactMarkdown from 'react-markdown'
import { NavLink } from 'react-router-dom'
import { clickthrough } from '../functions'

class Property extends Component {
  render(){
    if(this.props.property){
      return (
        <article data-property data-clickthrough onClick={clickthrough}
          className={this.props.filters ? this.props.filters.join(' ') : ''}>
          <span data-colorbar className="absolute z-10 left-0 top-0 w-4 h-full bg-transparent flex flex-col content-stretch items-stretch">
          { this.props.filters &&
            this.props.filters.map((filter, f)=>{
              return <span key={`property__colorbar--${f}`} className={`flex w-full ${filter}`}/>
            })
          }
          </span>
          <div className="flex px-3 py-4 mb-4">
            <h2 className="text-3xl font-bold mr-4 my-auto">{this.props.property.name}</h2>
            <NavLink className="bg-amber-600 text-white p-2 rounded-sm text-xs" to={`/properties/${this.props.property.id}`}>View</NavLink>
            {this.props.property.address &&
            <address>{ this.props.property.address.replace('<br>',<br/>)}</address>
            }
          </div>
        </article>
      )
    }else{
      return false
    }
  }
}

Property.defaultProps = {
  property: false,
  filters: false,
  activeFilters: []
}

export default Property