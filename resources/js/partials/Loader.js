import React, {Component} from 'react'

class Loader extends Component {
  render() {
    return (
      <div className="progress">
        <div className="progress-bar" role="progressbar" style={{width: String(this.props.width) + '%', ariaValuenow:this.props.width, ariaValuemin:'0', ariaValuemax:100}}/>
      </div>
    )
  }
}

Loader.defaultProps = {
  width: 25
}

export default Loader