let markers = [], infowindows = [], 
map, mapCenter = {lat: 42.4409298, lng:-83.2502813},
mapZoom = 17.06, 
standardOptions = {
  zoom: mapZoom,
  heading: 269,
  tilt: 0,  
  zoomControl: false,
  rotateControl: true,
  tiltControl: false,
  center: mapCenter, 
  disableDefaultUI: false
}

export function initMap(options = standardOptions) {
  map = new google.maps.Map(document.getElementById("map"), {
    ...options
  })

  const activations = [
    { 
      title: 'Comunior', 
      address: 'Pierson',
      position: {
        lat: 42.44293413439971, lng: -83.24812250180321
      }
    },{ 
      title: 'Clan Wolf-Williamson', 
      address: 'Stout St',
      description: 'Consisting of <em>Mark</em> and <em>Tina Williamson</em>, Clan Wolf-Williamson occupies the eastern-most tip of <em>the Trapp</em>', 
      position: {
        lat: 42.44183, lng: -83.24449
      }
    },{ 
      title: 'the Braile House', 
      address: 'Braile',
      position: {
        lat: 42.44182497020209, lng: -83.24635456085619
      }
    },{
      title: 'Dia & Dom',
      address: 'Trinity',
      position: {
        lat: 42.43867959843874, lng: -83.24985412712603
      }
    },{ 
      title: 'Umique', 
      address: 'Trinity',
      position: {
        lat: 42.43883993531232, lng: -83.24995873327066
      }
    },{ 
      title: 'William & Deanna', 
      address: 'Blackstone Ct',
      position: {
        lat: 42.442337897684446, lng:-83.25114972928012
      }
    },{
      title: 'the Trinity House',
      address: 'Trinity',
      position: {
        lat: 42.44229258194737, lng: -83.25061391121
      }
    }/*,{
      title: 'the Save A Lot',
      type: 'business',
      address: '20811 Eight Mile Rd, M-102, Detroit, MI 48219',
      description: 'Some of these mufuckaz got attitude, but the Greek chick cool, once you get on her good side. Go midday, avoid Sunday and 1st of the month through the 3rd! Shit be HOT',
      position: {
        lat: 42.44326352263565, lng: -83.24859845618748
      }
    }*/
  ]

  function makeDot(step){
    if(!step) return false
    return {
      url: "./assets/images/dot.svg",
      anchor: step.anchor ? step.anchor : new google.maps.Point(10,13)
    }
  }

  activations.forEach(step => {
    const marker = new google.maps.Marker({
      title: step.title, 
      position: step.position,
      map: map,
      icon: makeDot(step)
    })
    let infowindow

    markers.push(marker)

    infowindow = new google.maps.InfoWindow({
      content: '<aside id="content" class="px-8">' +
      '<div id="siteNotice"></div>' + 
      '<button class="pointer-events-none absolute z-50 -right-3 -top-3 rounded-full w-8 h-8 text-gray-50 bg-bongo_pink" style="box-shadow: 0px 6px 7px rgba(102, 51, 255, 0.25);">' +
      '<svg class="w-1/3 h-1/3 mx-auto">' +
      '<use xlink:href="#icon__math--multiply"/></svg>' + 
      '</button>' +
      '<h1 class="text-base font-semibold">'+step.title+'</h1>' +
      (step.description ? '<p class="text-xs font-base">'+step.description+'</p>' : '' ) +
      "</aside>",
      maxWidth: 280
    })
    
    infowindows.push(infowindow)

    marker.addListener("click", () => {
      infowindows.forEach(win => win.close())
      infowindow.open({
        anchor: marker,
        map,
        shouldFocus: false
      })
      map.setCenter(step.position)
    })
  })

  let trapPoints = activations.map((act) => { return { lat: act.position.lat, lng: act.position.lng } })
  trapPoints.push({ lat: activations[0].position.lat, lng: activations[0].position.lng })
  const flightPath = new google.maps.Polygon({
    path: trapPoints,
    geodesic: true,
    strokeColor: "#FF00CC",
    strokeOpacity: 1.0,
    strokeWeight: 2,
    fillColor: "#FF00CC",
    fillOpacity: 0.35
  })

  flightPath.setMap(map)
}
/*
document.addEventListener("DOMContentLoaded", () => {
  initMap(standardOptions)
})*/