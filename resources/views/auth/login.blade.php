@extends('layouts.app')

@section('content')
  <div data-slidetab="login" class="transition duration-300 w-full">
    <form method="POST" action="{{ route('login') }}">
      @csrf
      <div data-login></div>
    </form>
  </div>
  <div data-slidetab="register" class="transition duration-300 w-full hidden">
    <form method="POST" action="{{ route('register') }}">
      @csrf
      <div data-register></div>
    </form>
  </div>
@endsection