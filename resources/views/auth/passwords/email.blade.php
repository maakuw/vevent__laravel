@extends('layouts.app')

@section('content')
<div class="container flex flex-col h-full items-center content-center">
  <section class="flex content-center">
    <div class="w-full sm:w-2/3 md:w-5/6">
      @if (session('status'))
      <aside class="alert alert-success" role="alert">
        {{ session('status') }}
      </aside>
      @endif
      <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="card mb-0">
          <legend class="card-header">{{ __('Reset Password') }}</legend>
          <div class="card-body mb-0">  
            <p>Please enter your email and we'll send you a password reset link.</p>
            <fieldset class="form-group">
              <label for="email">{{ __('E-Mail Address') }}</label>
              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
              @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
              @enderror
            </fielset>
          </div>
          <nav class="card-footer flex content-end">
            <button type="submit" class="flex items-center justify-center bg-primary">
                {{ __('Send') }}
            </button>
          </nav>
        </div>
      </form>
    </div>
  </section>
</div>
@endsection