@extends('layouts.app')

@section('content')
<div class="container flex flex-col h-full items-center content-center">
  <section class="flex content-center">
    <div class="w-full sm:w-2/3 md:w-5/6">
      <form method="POST" action="{{ route('password.update') }}"">
        @csrf
        <div class="card mb-0">
          <legend class="card-header">{{ __('Reset Password') }}</legend>
          <div class="card-body mb-0">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <fieldset class="form-group">
              <label for="email">{{ __('E-Mail Address') }}</label>
              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
              @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </fieldset>
            <fieldset class="form-group">
              <label for="password">{{ __('Password') }}</label>
              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
              @error('password')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </fieldset>
            <fielset class="form-group">
              <label for="password-confirm">{{ __('Confirm Password') }}</label>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </fielset>
          </div>
          <nav class="card-footer">
            <button type="submit" class="flex items-center justify-center text-white bg-primary">
              {{ __('Reset Password') }}
            </button>
          </nav>
        </div>
      </form>
    </div>
  </section>
</div>
@endsection