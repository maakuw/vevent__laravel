@php
  $user = Auth::user();
@endphp
<header data-header class="absolute top-0 flex content-center content-start bg-gray-800 w-screen z-20 px-4 py-2">
  <div class="container w-full h-full flex content-between relative">
    @if($user)
    <button data-toggler class="pl-2 md:pl-3 lg:hidden" type="button" aria-label="Toggle primary navigation">
      <svg class="icon">
        <use xlink:href="#icon__house"/>
      </svg>
    </button>
    @endif
    <figure class="flex w-auto pl-0 py-0">
      <figcaption class="uppercase font-light text-gray-100">Land Baron</figcaption>
    </figure>
  </div>
</header>