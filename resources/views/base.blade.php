@php
  $route_name = Request::path();
  $route_name = str_replace('/', ' ', $route_name);
  $user = Auth::user();
@endphp
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if ($user)
    <meta name="user" content="{{$user}}">
    @endif

    <title>{{ config('app.name', 'land_baron') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    @if ($user)
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="{{ asset('js/map_functions.js') }}"></script>
    @endif
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>.gmnoprint{background:black;color:white;font-weight:normal;text-transform:uppercase}.gm-style-cc{height: unset !important;line-height: unset !important;}.gm-style-cc>div:first-child{width:unset;height:unset;position:static;display:none}.gm-style-cc>div:last-child{padding:0.25rem 0.5rem;font-size:0.5875rem;color:#ff00cc;}.gm-style-cc button,.gm-style-cc a,.gm-style-cc span{color: #ff00cc !important;}</style>
  </head>
  <body class="{{$route_name}}"{{$user ? 'data-logged' : ''}}>
    <x-sprite/>
    <main>
      <div data-main class="relative z-0 flex content-between items-start"></div>
      @if ($user)
      <!--
      <article data-map class="absolute z-10 right-0 top-0 flex m-0 px-0 items-stretch justify-stretch w-4/5 flex-col h-screen">
        <div id="map" class="w-full h-full">
          <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOA6hcZ5GobjXA1Ve56MGqxGcxV3qNURo&amp;v=beta&amp;channel=2"></script>
        </div>
      </article>-->
      @endif
    </main>
  </body>
</html>