@php
    $route_name = Request::path();
    $route_name = str_replace('/', ' ', $route_name);
@endphp
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'land_baron') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body class="{{$route_name}}">
    <x-sprite/>
    <main class="flex nowrap content-stretch items-stretch w-screen h-screen overflow-hidden">
      <aside class="absolute z-10 flex flex-col relative w-full sm:w-3/5 md:w-1/3 lg:w-1/4 p-4 lg:p-12 bg-gray-50 shadow-lg">
        <figure class="flex flex-col w-1/3 mx-auto my-12">
          <svg class="w-24 h-24">
            <use xlink:href="#logo__land_baron"/>
          </svg>
        </figure>
        @yield('content')
      </aside>
      <article class="relative z-0 hidden sm:flex nowrap relative w-full md:w-2/3 lg:w-3/4 bg-gray-600 bg-cover bg-center" style="background-image: url(./img/background-deco_house.jpg)">
        <img alt="" src="./img/background-deco_house.jpg" class="hidden"/>
      </article>
    </main>
  </body>
</html>