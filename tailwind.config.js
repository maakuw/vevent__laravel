const colors = require('tailwindcss/colors')

module.exports = {
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
    },
    colors: {
      amber: colors.amber,
      gray: colors.gray,
      blue: colors.blue,
      sky: colors.sky, 
      slate: colors.slate, 
      orange: colors.orange, 
      poppy: 'rgb(var(--poppy))',
      danger: 'rgb(var(--poppy))', 
      pink: colors.fuchsia,
      success: {
        DEFAULT: 'rgb(var(--success))',
        600: 'rgb(var(--success))'
      }, 
      white: 'rgb(var(--white))',
      primary: {
        DEFAULT: 'rgb(var(--primary))',
        600: 'rgb(var(--primary))',
        100: colors.amber[100]
      },
      secondary: {
        DEFAULT: 'rgb(var(--secondary))',
        600: 'rgb(var(--secondary)/60%)'
      },
      tertiary: colors.gray['100'],
      current: 'currentColor'
    },
    fontFamily: {
      sans: ['Graphik', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
    },
    extend: {
      spacing: {
        '128': '32rem',
        '144': '36rem'
      },
      padding: { 
        '1/3': '33%'
      },
      borderRadius: {
        '4xl': '2rem',
      },
      colors: {
        gray: {
          100: 'rgb(var(--gray-100))'
        }
      },
      height: {
        '33vh': '33vh'
      }
    }
  }
}