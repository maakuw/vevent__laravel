<?php

namespace App\Http\Resources;
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/
//Open CMS Routes
Route::get('/cms/areas', [AreasController::class, 'index']);
Route::get('/cms/attributes', [AttributesController::class, 'index']);
Route::get('/cms/barons', [UsersController::class, 'barons']);
Route::get('/cms/responsibilities', [ResponsibilitiesController::class, 'index']);
Route::get('/cms/properties', [PropertiesController::class, 'index']);
Route::get('/cms/portfolios', [PortfoliosController::class, 'active']);
Route::get('/cms/filters', [FiltersController::class, 'index']);
Route::get('/cms/floors', [FloorsController::class, 'index']);
Route::get('/cms/settings', [SettingsController::class, 'index']);
Route::get('/cms/tenants', [UsersController::class, 'tenants']);
Route::get('/cms/subattributes', [SubattributesController::class, 'index']);
Route::get('/cms/watching', [UsersController::class, 'watching']);

//Global Authorization Routes
Auth::routes();

//Frontend

Route::view('/', 'base')->middleware('auth');
Route::view('/portfolio', 'base')->middleware('auth');
Route::view('/properties', 'base')->middleware('auth');
Route::view('/properties/{id}', 'base')->middleware('auth');
Route::view('/properties/attribute/{id}', 'base')->middleware('auth');
Route::view('/properties/area/{id}', 'base')->middleware('auth');
Route::view('/properties/walk-the-floor', 'base')->middleware('auth');
Route::view('/profile', 'base')->middleware('auth');
Route::view('/barons', 'base')->middleware('auth');
Route::view('/session', 'base')->middleware('auth');
Route::view('/barons/{id}', 'base')->middleware('auth');
Route::view('/tenants', 'base')->middleware('auth');
Route::view('/tenants/{id}', 'base')->middleware('auth');
Route::view('/settings', 'base')->middleware('auth');

//Admin Resources
Route::resources([
    '/resource/properties' => PropertiesController::class,
    '/resource/portfolios' => PortfoliosController::class,
    '/resource/filters' => FiltersController::class,
    '/resource/floors' => FloorsController::class,
    '/resource/users' => UsersController::class,
    '/resource/settings' => SettingsController::class,
    '/resource/areas' => AreasController::class,
    '/resource/tenants' => TenantsController::class,
    '/resource/barons' => BaronsController::class,
    '/resource/attributes' => AttributesController::class,
    '/resource/subattributes' => SubattributesController::class
]);

Route::get('/user', [SettingsController::class, 'user']);
Route::get('/logout', [SettingsController::class, 'logout']);
Route::put('/setting', [SettingsController::class, 'setting']);