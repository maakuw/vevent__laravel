const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
const use_maps = process.env.APP_ENV === 'local' ? true : false;
mix.js('resources/js/app.js', 'public/js')
.js('resources/js/map_functions.js', 'public/js/map_functions.js')
.sass('resources/scss/app.scss', 'public/css')
.options({
    postCss: [ tailwindcss('./tailwind.config.js') ],
})
.copyDirectory('resources/img', 'public/img')
.sourceMaps(use_maps, 'source-map')
.version();
   