/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	// The require scope
/******/ 	var __webpack_require__ = {};
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
/*!***************************************!*\
  !*** ./resources/js/map_functions.js ***!
  \***************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "initMap": () => (/* binding */ initMap)
/* harmony export */ });
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var markers = [],
    infowindows = [],
    map,
    mapCenter = {
  lat: 42.4409298,
  lng: -83.2502813
},
    mapZoom = 17.06,
    standardOptions = {
  zoom: mapZoom,
  heading: 269,
  tilt: 0,
  zoomControl: false,
  rotateControl: true,
  tiltControl: false,
  center: mapCenter,
  disableDefaultUI: false
};
function initMap() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : standardOptions;
  map = new google.maps.Map(document.getElementById("map"), _objectSpread({}, options));
  var activations = [{
    title: 'Comunior',
    address: 'Pierson',
    position: {
      lat: 42.44293413439971,
      lng: -83.24812250180321
    }
  }, {
    title: 'Clan Wolf-Williamson',
    address: 'Stout St',
    description: 'Consisting of <em>Mark</em> and <em>Tina Williamson</em>, Clan Wolf-Williamson occupies the eastern-most tip of <em>the Trapp</em>',
    position: {
      lat: 42.44183,
      lng: -83.24449
    }
  }, {
    title: 'the Braile House',
    address: 'Braile',
    position: {
      lat: 42.44182497020209,
      lng: -83.24635456085619
    }
  }, {
    title: 'Dia & Dom',
    address: 'Trinity',
    position: {
      lat: 42.43867959843874,
      lng: -83.24985412712603
    }
  }, {
    title: 'Umique',
    address: 'Trinity',
    position: {
      lat: 42.43883993531232,
      lng: -83.24995873327066
    }
  }, {
    title: 'William & Deanna',
    address: 'Blackstone Ct',
    position: {
      lat: 42.442337897684446,
      lng: -83.25114972928012
    }
  }, {
    title: 'the Trinity House',
    address: 'Trinity',
    position: {
      lat: 42.44229258194737,
      lng: -83.25061391121
    }
  }
  /*,{
   title: 'the Save A Lot',
   type: 'business',
   address: '20811 Eight Mile Rd, M-102, Detroit, MI 48219',
   description: 'Some of these mufuckaz got attitude, but the Greek chick cool, once you get on her good side. Go midday, avoid Sunday and 1st of the month through the 3rd! Shit be HOT',
   position: {
     lat: 42.44326352263565, lng: -83.24859845618748
   }
  }*/
  ];

  function makeDot(step) {
    if (!step) return false;
    return {
      url: "./assets/images/dot.svg",
      anchor: step.anchor ? step.anchor : new google.maps.Point(10, 13)
    };
  }

  activations.forEach(function (step) {
    var marker = new google.maps.Marker({
      title: step.title,
      position: step.position,
      map: map,
      icon: makeDot(step)
    });
    var infowindow;
    markers.push(marker);
    infowindow = new google.maps.InfoWindow({
      content: '<aside id="content" class="px-8">' + '<div id="siteNotice"></div>' + '<button class="pointer-events-none absolute z-50 -right-3 -top-3 rounded-full w-8 h-8 text-gray-50 bg-bongo_pink" style="box-shadow: 0px 6px 7px rgba(102, 51, 255, 0.25);">' + '<svg class="w-1/3 h-1/3 mx-auto">' + '<use xlink:href="#icon__math--multiply"/></svg>' + '</button>' + '<h1 class="text-base font-semibold">' + step.title + '</h1>' + (step.description ? '<p class="text-xs font-base">' + step.description + '</p>' : '') + "</aside>",
      maxWidth: 280
    });
    infowindows.push(infowindow);
    marker.addListener("click", function () {
      infowindows.forEach(function (win) {
        return win.close();
      });
      infowindow.open({
        anchor: marker,
        map: map,
        shouldFocus: false
      });
      map.setCenter(step.position);
    });
  });
  var trapPoints = activations.map(function (act) {
    return {
      lat: act.position.lat,
      lng: act.position.lng
    };
  });
  trapPoints.push({
    lat: activations[0].position.lat,
    lng: activations[0].position.lng
  });
  var flightPath = new google.maps.Polygon({
    path: trapPoints,
    geodesic: true,
    strokeColor: "#FF00CC",
    strokeOpacity: 1.0,
    strokeWeight: 2,
    fillColor: "#FF00CC",
    fillOpacity: 0.35
  });
  flightPath.setMap(map);
}
/*
document.addEventListener("DOMContentLoaded", () => {
  initMap(standardOptions)
})*/
/******/ })()
;
//# sourceMappingURL=map_functions.js.map