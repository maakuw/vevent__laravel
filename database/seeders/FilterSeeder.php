<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FilterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('filters')->insert([
            "slug" => "house",
            "label" => "House"
        ]);
        DB::table('filters')->insert([
            "slug" => "apartment",
            "label" => "Apartment"
        ]);
        DB::table('filters')->insert([
            "slug" => "commercial",
            "label" => "Commercial"
        ]);
        DB::table('filters')->insert([
            "slug" => "plot",
            "label" => "Plot"
        ]);
    }
}
