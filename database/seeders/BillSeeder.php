<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bills')->insert([
            "slug" => "mortgage",
            "label" => "Mortgage",
            "user_id" => 1
        ]);
        DB::table('bills')->insert([
            "slug" => "utilities",
            "label" => "Utilities",
            "user_id" => 4
        ]);
        DB::table('bills')->insert([
            "slug" => "taxes",
            "label" => "Taxes",
            "user_id" => 1
        ]);
        DB::table('bills')->insert([
            "slug" => "rent",
            "label" => "Rent",
            "user_id" => 4
        ]);
    }
}
