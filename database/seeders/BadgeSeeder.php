<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BadgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('badges')->insert([
            "slug" => "session-soldier",
            "title" => "Session", 
            "coor_a1" => -15.25, 
            "coor_a2" => 0, 
            "subtitle" => "Soldier", 
            "coor_b1" => -25.278, 
            "coor_b2" => 10, 
            "color" => "primary"
        ]);
        DB::table('badges')->insert([
            "slug" => "contact-champ",
            "title" => "Contact", 
            "coor_a1" => -24.524, 
            "coor_a2" => 0, 
            "subtitle" => "Champ", 
            "coor_b1" => -22.92, 
            "coor_b2" => 10, 
            "color" => "primary"
        ]);
        DB::table('badges')->insert([
            "slug" => "breakout-baron",
            "title" => "Breakout",
            "coor_a1" => -19.068, 
            "coor_a2" => 0, 
            "subtitle" => "Baron",
            "coor_b1" => -21.792, 
            "coor_b2" => 10, 
            "color" => "primary"
        ]);
        DB::table('badges')->insert([
            "slug" => "straight-up-gamer",
            "title" => "Straight Up",
            "coor_a1" => -24.826, 
            "coor_a2" => 0, 
            "subtitle" => "Gamer",
            "coor_b1" => -22.056, 
            "coor_b2" => 10, 
            "color" => "warning"
        ]);
        DB::table('badges')->insert([
            "slug" => "merchandise-master",
            "title" => "Merchandise",
            "coor_a1" => -26.092, 
            "coor_a2" => 0, 
            "subtitle" => "Master",
            "coor_b1" => -24.678, 
            "coor_b2" => 10, 
            "color" => "green"
        ]);
    }
}
