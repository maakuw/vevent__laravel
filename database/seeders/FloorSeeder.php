<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FloorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('floors')->insert([
            'slug' => 'ground', 
            'name' => 'Ground', 
            'color' => '#fd7e14', 
            'area_id' => 1, 
            'shadowColor' => '#0099AE', 
            'coors' => json_encode([[47,-116],[125,-70],[62,-33],[-2,4],[-65,41],[14,87],[91,132],[155,96],[219,59],[282,21]]),
            'background' => '//via.placeholder.com/1280x1024'
        ]);
        DB::table('floors')->insert([
            'slug' => 'aerial', 
            'name' => 'Aerial', 
            'color' => '#fd7e14',
            'area_id' => 1, 
            'shadowColor' => '#0099AE', 
            'coors' => json_encode([[47,-116],[125,-70],[62,-33],[-2,4],[-65,41],[14,87],[91,132],[155,96],[219,59],[282,21]]),
            'background' => '//via.placeholder.com/1280x1024'
        ]);
        DB::table('floors')->insert([
            'slug' => 'upstairs', 
            'name' => 'Upstairs', 
            'color' => '#fd7e14',
            'area_id' => 2, 
            'shadowColor' => '#0099AE', 
            'coors' => json_encode([[47,-116],[125,-70],[62,-33],[-2,4],[-65,41],[14,87],[91,132],[155,96],[219,59],[282,21]]),
            'background' => '//via.placeholder.com/1280x1024'
        ]);
        DB::table('floors')->insert([
            'slug' => 'downstairs', 
            'name' => 'Downstairs', 
            'color' => '#fd7e14',
            'area_id' => 2, 
            'shadowColor' => '#0099AE', 
            'coors' => json_encode([[47,-116],[125,-70],[62,-33],[-2,4],[-65,41],[14,87],[91,132],[155,96],[219,59],[282,21]]),
            'background' => '//via.placeholder.com/1280x1024'
        ]);
    }
}
