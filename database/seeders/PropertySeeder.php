<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->insert([
            "name" => "the Stout House",
            "slug" => "stout",
            "address" => "20311 Stout St<br>Detroit, MI 48219-1484<br>United States",
            "contact_id" => 4,
            "portfolio_id" => 1,
            "rent" => 900
        ]);   
    }
}
