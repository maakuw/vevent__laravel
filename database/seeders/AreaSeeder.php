<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areas')->insert([
            "slug" => "interior",
            "label" => "Interior",
            "property_id" => 1
        ]);

        DB::table('areas')->insert([
            "slug" => "exterior",
            "label" => "Exterior",
            "property_id" => 1
        ]);
    }
}
