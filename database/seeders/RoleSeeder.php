<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Admin',
            'slug' => 'admin',
            'level' => 1
        ]);
        DB::table('roles')->insert([
            'name' => 'Land Baron',
            'slug' => 'baron',
            'level' => 2
        ]);
        DB::table('roles')->insert([
            'name' => 'Tenant',
            'slug' => 'tenant',
            'level' => 3
        ]);
    }
}
