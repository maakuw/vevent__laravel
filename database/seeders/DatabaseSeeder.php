<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class, 
            RoleSeeder::class,
            PortfolioSeeder::class,
            FloorSeeder::class,
            AreaSeeder::class, 
            BadgeSeeder::class, 
            BadgeUserSeeder::class, 
            ResponsibilitySeeder::class, 
            BillSeeder::class, 
            ContactsSeeder::class, 
            ContactUserSeeder::class, 
            SocialSeeder::class,  
            SocialUserSeeder::class, 
            PropertySeeder::class, 
            AttributeSeeder::class, 
            SubattributeSeeder::class, 
            FilterSeeder::class
        ]);
    }
}
