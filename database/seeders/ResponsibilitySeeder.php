<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResponsibilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('responsibilities')->insert([
            "slug" => "gardening",
            "label" => "Gardening",
            "floor_id" => 1
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "plumbing",
            "label" => "Plumbing",
            "floor_id" => 1
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "entry",
            "label" => "Entry",
            "floor_id" => 1
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "fences",
            "label" => "Fences",
            "floor_id" => 1
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "gates",
            "label" => "Gates",
            "floor_id" => 1
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "electrical",
            "label" => "Electrical",
            "floor_id" => 1
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "refuse",
            "label" => "Refuse",
            "floor_id" => 1
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "electrical",
            "label" => "Electrical",
            "floor_id" => 2
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "roof",
            "label" => "Roof",
            "floor_id" => 2
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "gutters",
            "label" => "Gutters",
            "floor_id" => 2
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "appliances",
            "label" => "Appliances",
            "floor_id" => 3
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "walls_and_ceilings",
            "label" => "Walls & Ceilings",
            "floor_id" => 3
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "plumbing",
            "label" => "Plumbing",
            "floor_id" => 3
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "floors",
            "label" => "Floors",
            "floor_id" => 3
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "doors",
            "label" => "Doors",
            "floor_id" => 3
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "entry",
            "label" => "Entry",
            "floor_id" => 3
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "appliances",
            "label" => "Appliances",
            "floor_id" => 4
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "electrical",
            "label" => "Electrical",
            "floor_id" => 4
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "plumbing",
            "label" => "Plumbing",
            "floor_id" => 4
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "floor",
            "label" => "Floor",
            "floor_id" => 4
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "water_heater",
            "label" => "Water Heater",
            "floor_id" => 4
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "foundation",
            "label" => "Foundation",
            "floor_id" => 4
        ]);
        DB::table('responsibilities')->insert([
            "slug" => "housekeeping",
            "label" => "Housekeeping",
            "floor_id" => 4
        ]);
    }
}
