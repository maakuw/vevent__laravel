<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            "username" => "abateast", 
            "email" => "abateast@gmail.com", 
            "first_name" => "Angelo",
            "last_name" => "Bateast",
            'user_level_id' => 2, 
            "title" => "Owner/Operator",
            'password' => bcrypt('password'),
            "bio" =>"High touch client streamline, nor on your plate, but put a record on and see who dances net net nor UX, so prioritize these line items. High touch client streamline, nor on your plate, but put a record on and see who dances net net nor UX, so prioritize these line items."
        ]);
        DB::table('users')->insert([
            'username' => 'btesteroni',
            'first_name' => 'Bobby',
            'last_name' => 'Testeroni',
            'user_level_id' => 1, 
            'email' => 'maakuwemail@gmail.com',
            'photo' => '//via.placeholder.com/100x100',
            'password' => bcrypt('password'),
            'honorific' => 'Mr.',
            'title' => 'Sr Software Engineer',
            'company' => 'Bluewater Technologies',
            'bio' => 'Lorem ipsum sin dolor',
            'address' => '123 4th Street<br>Detroit, MI 48208'
        ]);
        DB::table('users')->insert([
            'username' => 'sitemanager',
            'first_name' => 'Manager',
            'last_name' => 'Username',
            'user_level_id' => 2, 
            'email' => 'mwilliamson@bluewatertech.com',
            'photo' => '//via.placeholder.com/100x100',
            'password' => bcrypt('password'),
        ]);
        DB::table('users')->insert([
            'username' => 'dgrey',
            'first_name' => 'Dorian',
            'last_name' => 'Grey',
            'user_level_id' => 3, 
            'email' => 'dorianmgrey@gmail.com',
            'photo' => '//via.placeholder.com/100x100',
            'password' => bcrypt('password'),
        ]);
    }
}
