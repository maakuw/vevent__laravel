<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attributes')->insert([
            "slug" => "doors",
            "label" => "Doors",
            "responsibility_id" => 3
        ]);
        DB::table('attributes')->insert([
            "slug" => "lights",
            "label" => "Lights",
            "responsibility_id" => 6
        ]);
        DB::table('attributes')->insert([
            "slug" => "cameras",
            "label" => "Cameras",
            "responsibility_id" => 6
        ]);
        DB::table('attributes')->insert([
            "slug" => "trash",
            "label" => "Trash",
            "responsibility_id" => 7
        ]);
        DB::table('attributes')->insert([
            "slug" => "recycling",
            "label" => "Recycling",
            "responsibility_id" => 7
        ]);
        DB::table('attributes')->insert([
            "slug" => "construction",
            "label" => "Construction",
            "responsibility_id" => 7
        ]);
        DB::table('attributes')->insert([
            "slug" => "bulk",
            "label" => "Bulk",
            "responsibility_id" => 7
        ]);
        DB::table('attributes')->insert([
            "slug" => "yard_waste",
            "label" => "Yard Waste",
            "responsibility_id" => 7
        ]);
        DB::table('attributes')->insert([
            "slug" => "lights",
            "label" => "Lights",
            "responsibility_id" => 8
        ]);
        DB::table('attributes')->insert([
            "slug" => "cameras",
            "label" => "Cameras",
            "responsibility_id" => 8
        ]);
        DB::table('attributes')->insert([
            "slug" => "stove",
            "label" => "Stove",
            "responsibility_id" => 9
        ]);
        DB::table('attributes')->insert([
            "slug" => "refrigerator",
            "label" => "Refrigerator",
            "responsibility_id" => 9
        ]);
    }
}
