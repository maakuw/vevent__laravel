<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BadgeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('badge_user')->insert([
            'user_id' => 10, 
            'badge_id' => 1
        ]);
        DB::table('badge_user')->insert([
            'user_id' => 10, 
            'badge_id' => 2
        ]);
        DB::table('badge_user')->insert([
            'user_id' => 10, 
            'badge_id' => 3
        ]);
    }
}
