<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubattributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subattributes')->insert([
            "slug" => "front",
            "label" => "Front",
            "attribute_id" => 1
        ]);
        DB::table('subattributes')->insert([
            "slug" => "rear",
            "label" => "Rear",
            "attribute_id" => 1
        ]);
        DB::table('subattributes')->insert([
            "slug" => "side",
            "label" => "Side",
            "attribute_id" => 1
        ]);
        DB::table('subattributes')->insert([
            "slug" => "front",
            "label" => "Front",
            "attribute_id" => 2
        ]);
        DB::table('subattributes')->insert([
            "slug" => "rear",
            "label" => "Rear",
            "attribute_id" => 2
        ]);
        DB::table('subattributes')->insert([
            "slug" => "side",
            "label" => "Side",
            "attribute_id" => 2
        ]);
        DB::table('subattributes')->insert([
            "slug" => "bathroom",
            "label" => "Bathroom",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "kitchen",
            "label" => "Kitchen",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room0",
            "label" => "Room 1",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room0",
            "label" => "Room 1",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room1",
            "label" => "Room 2",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room1",
            "label" => "Room 2",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room2",
            "label" => "Room 3",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room2",
            "label" => "Room 3",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room3",
            "label" => "Room 4",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room3",
            "label" => "Room 4",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room4",
            "label" => "Room 5",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room4",
            "label" => "Room 5",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room5",
            "label" => "Room 6",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room5",
            "label" => "Room 6",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room6",
            "label" => "Room 7",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room6",
            "label" => "Room 7",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room7",
            "label" => "Room 8",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room7",
            "label" => "Room 8",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room8",
            "label" => "Room 9",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room8",
            "label" => "Room 9",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room9",
            "label" => "Room 10",
            "attribute_id" => 3
        ]);
        DB::table('subattributes')->insert([
            "slug" => "room9",
            "label" => "Room 10",
            "attribute_id" => 3
        ]);
    }
}
