<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('thumb')->nullable();
            $table->string('bg')->nullable();
            $table->string('name');
            $table->double('rent')->default(0);
            $table->string('slug');
            $table->text('address')->nullable();
            $table->integer('contact_id')->default(0);
            $table->integer('portfolio_id')->default(0);
            $table->double('structure_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
