1) Run curl -s https://laravel.build/land_baron | bash to download a fresh install of Laravel to your current folder
2) Type "cd land_baron" to navigate to the directory
3) Type "npm install" to build grab your modules and build out the core folders
4) Run the following script to create a shortcut so you don't have to keep typing all this crap: alias sail='bash ./vendor/bin/sail'*
5) Start Docker, if it's not already running
6) We want to add our login stuff using "sail composer require laravel/ui"
7) Type "sail up" to start a fresh Docker instance and spool up a database with the existing data and our new CRUD setup!
8) Snags the styles modules altogether with "npm install -D tailwindcss@latest postcss@latest autoprefixer@latest"
9) npx tailwindcss init spits out a config file for tailwind

*If you'd like to have Laravel Sail setup with the shortcut globally (that's my vibe, personally) edit your bashrc! It differs on Windows and Mac, so do a search for your personal terminal shortcut

If, at some point, you've been manually screwing around with the Seeders, Factories, or Migrations, it's a good bet you've screwed up the migration by now. If you run into a wall, try sail composer dump-autoload -o to re-align the autoload to your changes. Doesn't always work, but it's the first step. Next, if that doesn't work, ensure that Laravel didn't put all your "seeders" in a "seeds" directory. Not sure why this misnaming happens, I believe it to be a legacy effect.