<?php
 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Portfolio;

use Illuminate\Http\Request;

class PortfoliosController extends Controller
{
    /**
     * An Interface to pull all the Portfolios for each State
     *
     * @return App\Models\Portfolios
     */
    public function parse($portfolios)
    {
        foreach ($portfolios as $portfolio) {
            $portfolio->properties;
        }
        return $portfolios;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::all();
        return response()->json($this->parse($portfolios));
    }

    /**
     * Display a listing of ONLY the enabled Portfolios.
     *
     * @return \Illuminate\Http\Response
     */
    public function active()
    {
        $portfolios = Portfolio::all();
        return response()->json($this->parse($portfolios));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $portfolio = new Portfolio([
            'title' => $request->get('title'),
            'slug' => $request->get('slug'),
            'address' => $request->get('address'),
            'description' => $request->get('description')
        ]);

        $portfolio->save();

        return response()->json('Portfolio successfully created!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $portfolio = Portfolio::find($id);
        $portfolio->title = $request->get('title');
        $portfolio->slug = $request->get('slug');
        $portfolio->description = $request->get('description');
        $portfolio->address = $request->get('address');

        $portfolio->save();
        return response()->json('Portfolio successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = Portfolio::find($id);
        $portfolio->delete();
        
        return response()->json('Portfolio successfully deleted!');
    }
}
