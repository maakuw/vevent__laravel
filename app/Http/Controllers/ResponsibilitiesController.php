<?php

namespace App\Http\Controllers;

use App\Models\Responsibility;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResponsibilitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responsibilities = Responsibility::all();
        
        foreach ($responsibilities as $responsibility) {
            $responsibility->attributes;
            $responsibility->floor;
        }

        return response()->json($responsibilities);
    }
}
