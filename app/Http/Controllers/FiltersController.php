<?php

namespace App\Http\Controllers;

use App\Models\Filter;

class FiltersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = Filter::all();
        return response()->json($filters);
    }
}
