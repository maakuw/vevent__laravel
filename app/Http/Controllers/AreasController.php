<?php

namespace App\Http\Controllers;

use App\Models\Area;
use Illuminate\Http\Request;

class AreasController extends Controller
{
    /**
     * An Interface to pull all the events for each State
     *
     * @return App\Models\Area
     */
    public function parse($areas)
    {
        foreach ($areas as $area) {
            $area->property;
        }
        return $areas;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = Area::all();
        return response()->json($this->parse($areas));
    }
}
