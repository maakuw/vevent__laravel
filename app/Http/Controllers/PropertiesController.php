<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Http\Controllers\Controller;

class PropertiesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = Property::all();
        
        foreach ($properties as $property) {
            $property->portfolio;
            $property->contacts;
        }
        
        return response()->json($properties);
    }
}
