<?php

namespace App\Http\Controllers;

use App\Models\Program;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProgramsController extends Controller
{

    /**
     * An Interface to pull all the programs for each State
     *
     * @return App\Models\Program
     */
    public function parse($programs)
    {
        foreach ($programs as $program) {
            $program->events;
        }
        return $programs;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programs = Program::all();
        
        return response()->json($this->parse($programs));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function active()
    {
        $programs = Program::where('suspended', '=', 'off')->get();
        
        return response()->json($this->parse($programs));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $program = new Program([
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'end_time' => $request->get('end_time'),
            'start_time' => $request->get('start_time'),
            'suspended' => $request->get('suspended')
        ]);

        $program->save();

        return response()->json($program->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $program = Program::find($id);
        $program->name = $request->get('name');
        $program->slug = $request->get('slug');
        $program->start_time = $request->get('start_time');
        $program->end_time = $request->get('end_time');
        $program->suspended = $request->get('suspended');

        $program->save();
        return response()->json($program);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program = Program::find($id);
        $program->delete();
        
        return response()->json('Program successfully deleted!');
    }
}
