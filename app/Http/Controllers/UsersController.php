<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        foreach ($users as $user) {
            $user->badges;
            $user->speeches;
            $user->social;
            foreach($user->contacts as $contact){
                $contact->user;
            }
        }
        
        return response()->json($users);
    }

    /**
     * Display a listing of only Users that are "tenant" user_level.
     *
     * @return \Illuminate\Http\Response
     */
    public function tenants()
    {
        $tenants = User::where('user_level_id','=',3)->get();
        foreach ($tenants as $tenant) {
            $tenant->badges;
            $tenant->social;
            foreach($tenant->contacts as $tenant){
                $tenant->user;
            }
        }
        
        return response()->json($tenants);
    }

    /**
     * Display a listing of only Users that are "tenant" user_level.
     *
     * @return \Illuminate\Http\Response
     */
    public function watching()
    {
        $tenants = User::all();
        $users = [];
        foreach ($tenants as $tenant) {
          if( Cache::has('user-is-watching-' . $tenant->id) ) $users[] = $tenant;
        }
        
        return response()->json($users);
    }

    /**
     * Display a listing of only Users that are "baron" user_level.
     *
     * @return \Illuminate\Http\Response
     */
    public function barons()
    {
        $barons = User::where('user_level_id','=',2)->get();
        foreach ($barons as $baron) {
            $baron->speeches;
            $baron->social;
            $baron->badges;
            foreach($baron->contacts as $contact){
                $contact->user;
            }
        }
        
        return response()->json($barons);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique'
        ]);

        $user = new User([
            'slug' => $request->get('slug'),
            'username' => $request->get('username'), 
            'first_name' => $request->get('first_name'), 
            'last_name' => $request->get('last_name'), 
            'user_level_id' => $request->get('user_level_id'), 
            'email' => $request->get('email'), 
            'password' => $request->get('password'), 
            'photo' => $request->get('photo'), 
            'honorific' => $request->get('honorific'), 
            'title' => $request->get('title'), 
            'company' => $request->get('company'), 
            'bio' => $request->get('bio'), 
            'address' => $request->get('address'),
            'rent' => $request->get('rent'),
        ]);

        $user->save();

        return response()->json($user->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique'
        ]);

        $user = User::find($id);
        $user->slug = $request->get('slug');
        $user->username = $request->get('username');
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->user_level_id = $request->get('user_level_id');
        $user->email = $request->get('email');
        $user->password = $request->get('password');
        $user->photo = $request->get('photo');
        $user->honorific = $request->get('honorific');
        $user->title = $request->get('title');
        $user->company = $request->get('company');
        $user->bio = $request->get('bio');
        $user->address = $request->get('address');
        $user->rent = $request->get('rent');

        $user->save();
        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        
        return response()->json('User successfully deleted!');
    }
}
