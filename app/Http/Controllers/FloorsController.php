<?php

namespace App\Http\Controllers;

use App\Models\Floor;
use Illuminate\Http\Request;

class FloorsController extends Controller
{
    /**
     * An Interface to pull all the events for each State
     *
     * @return App\Models\Floor
     */
    public function parse($floors)
    {
        foreach ($floors as $floor) {
            $floor->area;
            $floor->responsibilities;
        }
        return $floors;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $floors = Floor::all();
        return response()->json($this->parse($floors));
    }
}
