<?php

namespace App\Models;

use App\Models\Responsibility;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $fillable = [
        'label', 
        'slug',
        'responsibility_id'
    ];

    public function responsibility(){
        return $this->belongsTo(Responsibility::class);
    }
}
