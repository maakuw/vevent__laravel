<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $fillable = ([
      'label', 
      'slug',
      'user_id'
    ]);

    public function user(){
      return $this->belongsTo(User::class);
    }
}
