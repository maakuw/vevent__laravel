<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUser extends Model
{

    protected $table = 'contact_user';

    protected $fillable = ([
        'user_id',
        'contact_id'
    ]);
}
