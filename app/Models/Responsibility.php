<?php

namespace App\Models;

use App\Models\Floor;
use App\Models\Attribute;
use Illuminate\Database\Eloquent\Model;

class Responsibility extends Model
{
    protected $fillable = ([
      'slug', 
      'label', 
      'floor_id'
    ]);

    protected $table = 'responsibilities';

    public function floor(){
      return $this->belongsTo(Floor::class);
    }
    public function attributes(){
      return $this->hasMany(Attribute::class);
    }
}
