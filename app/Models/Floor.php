<?php

namespace App\Models;

use App\Models\Area;
use App\Models\Responsibility;
use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $fillable = ([
      'slug', 
      'name', 
      'area_id',
      'color', 
      'shadowColor' , 
      'coors', 
      'background'
    ]);

    public function area(){
      return $this->belongsTo(Area::class);
    }

    public function responsibilities(){
      return $this->hasMany(Responsibility::class);
    }
}
