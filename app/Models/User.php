<?php

namespace App\Models;

use App\Models\Badge;
use App\Models\Speech;
use App\Models\Social;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'username', 
        'first_name', 
        'last_name', 
        'role_id',
        'email', 
        'password',
        'photo',
        'honorific',
        'title',
        'company',
        'bio',
        'address',
        'rent',
        'last_seen'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function badges(){
        return $this->belongsToMany(Badge::class);
    }

    public function portfolio(){
        return $this->hasMany(Portfolio::class);
    }

    public function house(){
        return $this->belongsTo(Property::class);
    }

    public function contacts(){
        return $this->belongsToMany(Contact::class);
    }

    public function social(){
        return $this->belongsToMany(Social::class);
    }

    public function online(){
        return Cache::has('user-is-watching-' . $this->id);
    }
}
