<?php

namespace App\Models;

use App\Models\Property;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $fillable = ([
        'title', 
        'slug', 
        'address',
        'description'
    ]);

    public function properties() {
        return $this->hasMany(Property::class);
    }
}
