<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    protected $fillable = ([
        "slug",
        "title", 
        "coor_a1",
        "coor_b1", 
        "subtitle", 
        "coor_b1",  
        "coor_b2", 
        "color"
    ]);
}
