<?php

namespace App\Models;

use App\Models\Attribute;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use HasFactory;

    protected $fillable = ([
      'name', 
      'slug',
      'attribute_id'
    ]);
  
    public function attribute() {
      return $this->belongsTo(Attribute::class);
    }
}
