<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
  protected $fillable = ([
      'name', 
      'slug', 
      'address',
      'user_id', 
      'contact_id', 
      'portfolio_id'
  ]);

  protected $table = "properties";

  public function portfolio(){
    return $this->belongsTo(Portfolio::class);
  }

  public function contacts(){
    return $this->belongsTo(Contact::class);
  }
}
